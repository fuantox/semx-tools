var cants = Number(document.getElementById('res').value); //cantidad de filas de la consulta
//alert(cants);

var fechas = [];
var cont = 0;
for(i=0; i<cants; i++){ //Ciclo que guardará las fechas en orden que serán mostradas en la tabla
	if(i==0) { //Si es el primer registro, tomamos la primera fecha encontrada
		fechas[cont] = document.getElementById("fecha"+i).value; //
		cont++;
	}
	if(i>=1){ //Si es el segundo registro o uno mayor, guaradaremos solo la fechas nuevas, sin repetir alguna
		var fech1 = document.getElementById("fecha"+i).value;
		var fech2 = fechas[cont-1];
		if((Date.parse(fech1)) > (Date.parse(fech2))){
			fechas[cont] = document.getElementById("fecha"+i).value;
			cont++;
		}
	}
}
//alert("fechas " + fechas.length);

var ops = Number(document.getElementById("nops").value); //cantidad de operaciones que apareceran
//alert(ops);
var operaciones = [];
for(i=0; i<ops;i++){ // Guarda un vector con las operaciones que son para la parte seleccionada y que serán mostradas en la gráfica
	operaciones[i] = document.getElementById("operaciones"+i).value;
}
operaciones.sort(); //vector ordenado alfabéticamente
var colores = [];//Array donde se guardará un color para cada operacion
for(i=0; i<ops;i++){
	switch(operaciones[i]){
		case 'OP10': colores[i] = 'rgba(123, 158, 253, 1)'; break;
		case 'OP20': colores[i] = 'rgba(255, 159, 64, 1)'; break;
		case 'OP30': colores[i] = 'rgba(247, 231, 88, 1)'; break;
		case 'OP40': colores[i] = 'rgba(178, 178, 178, 1)'; break;
		case 'OP50': colores[i] = 'rgba(238, 221, 36, 1)'; break;		
		case 'OP60': colores[i] = 'rgba(25, 202, 81, 1)'; break;
		case 'OP60A': colores[i] = 'rgba(250, 62, 62, 1)'; break;
		case 'OP60B': colores[i] = 'rgba(83, 156, 122, 1)'; break;
		case 'OP70': colores[i] = 'rgba(13, 103, 151, 1)'; break;
		case 'OP80': colores[i] = 'rgba(171, 80, 0, 1)'; break;
		case 'OP90': colores[i] = 'rgba(107, 91, 71, 1)'; break;
		
		case 'OP15': colores[i] = 'rgba(32, 88, 243, 1)'; break;
		case 'OP25': colores[i] = 'rgba(250, 185, 120, 1)'; break;
		case 'OP35': colores[i] = 'rgba(216, 195, 7, 1)'; break;
		case 'OP45': colores[i] = 'rgba(142, 142, 142, 1)'; break;
		case 'OP55': colores[i] = 'rgba(247, 240, 159, 1)'; break;
		case 'OP65': colores[i] = 'rgba(71, 225, 120, 1)'; break;
		case 'OP75': colores[i] = 'rgba(107, 158, 185, 1)'; break;
		case 'OP85': colores[i] = 'rgba(216, 101, 0, 1)'; break;
		case 'OP95': colores[i] = 'rgba(164, 140, 109, 1)'; break;
		
		default: colores[i] = 'rgba(0, 0, 0, 1)'; break;
	}
}
//alert(operaciones);

var totalM = []; //Total de la multiplicación de cantidad * precio
var cantPiezasT = []; //Cantidad de piezas
var band = 0;
var band2 = 0;
var band3 = 0;
var x = 0;
var y = 0;
var aux = 0;
for(i=0; i<ops; i++){
	totalM[i] = [];
	cantPiezasT[i] = [];
	for(j=0; j<cants;j++){
		var fech1 = document.getElementById("fecha"+band).value;
		var fech2 = document.getElementById("fecha"+j).value;
		if(fech1 == fech2){ //Solo guardará los costos del mismo día, sino, pasará a un nuevo día, y guardará en una nueva posición del vector
			if(document.getElementById("descop"+j).value == operaciones[i]){
				if(Number(document.getElementById("piezas"+j).value) != 0){
					x += ((Number(document.getElementById("cant"+j).value)*Number(document.getElementById("precio"+j).value)));
					totalM[i][band2] = Number(x) + 0.0;
					
					var p = document.getElementById("piezas"+j).value;
					if(p != aux) {
						y += Number(p);
						cantPiezasT[i][band3] = Number(y) + 0.0;
						aux = p;
					}
				}
				else if(Number(document.getElementById("piezas"+j).value) == 0){
					x += 0.0;
					totalM[i][band2] = Number(x) + 0.0;
				}
			}
			else{
				x=0;
				y=0;
			}
		}
		else{
			x = 0;
			band = j;
			band2++;
			band3++;
			j--;
		}
	}
	band = 0;
	band2 = 0;
	band3 = 0
}

for(i=0; i<totalM.length; i++){
	for(j=0; j<totalM[j].length; j++){
		if(totalM[i][j] == null || totalM[i][j] == "") totalM[i][j] = 0;
	}
}
for(i=0; i<cantPiezasT.length; i++){
	for(j=0; j<cantPiezasT[j].length; j++){
		if(cantPiezasT[i][j] == null || cantPiezasT[i][j] == "") cantPiezasT[i][j] = 0;
	}
}

//alert(totalM.length);
//alert(cantPiezasT.length);

var total = [];
for(i=0; i<totalM.length; i++){ //Guardará un vector con la suma de los costos de herramienta por pieza, para cada operación en X día
	total[i] = [];
	for(j=0; j<fechas.length;j++){
		if(cantPiezasT[i][j]>0 && cantPiezasT[i][j] != null && cantPiezasT[i][j] != "")
			total[i][j] = (parseFloat(totalM[i][j])/parseFloat(cantPiezasT[i][j])).toFixed(2);
		else 
			total[i][j] = 0;
		
		total[i][j] = roundToTwo(total[i][j]);
	}
}
//alert(total);
var datasetValue = [];
for (j=0; j<ops; j++) { //Creamos un dataset, donde asignaremos, color, data, etc para cada barra de la gráfica
	var newDataSet =
	{
		label: operaciones[j],
		data: total[j], //usar un vector para los distintos operaciones
		backgroundColor: colores[j],
		borderColor: colores[j],
		borderWidth: 1,
		stack: 'AAAAAAAAAAAA'
	}
	datasetValue[j] = newDataSet;
}

var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, { //Creamos las gráfica
    type: 'bar',
    data: {
        labels: fechas,
		datasets: datasetValue //Cada dataset debe ser una operación diferente
    },
    options: {
		animation: {
            duration: 500,
            easing: "easeOutQuart",
            onComplete: function () {
                var ctx = this.chart.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';
				var valoresFinales = new Array();
				var len = this.data.datasets.length;
				var a = 0;
				this.data.datasets.forEach(function (dataset) {
					for (var i = 0; i < dataset.data.length; i++) {
						var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                            scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
                        ctx.fillStyle = '#444';
                        var y_pos = model.y - 5;
                        // Make sure data value does not get overflown and hidden
                        // when the bar's value is too close to max value of scale
                        // Note: The y value is reverse, it counts from top down
                        if ((scale_max - model.y) / scale_max >= 0.93)
                            y_pos = model.y + 20; 
						if(valoresFinales[i] == null) valoresFinales[i]=0.0;
						valoresFinales[i] = Number(valoresFinales[i]) + Number(dataset.data[i]); //Guardamos la suma de cada valor de cada día
						if(a==(len-1)){ //Si ya estamos en los últimos valores, entonces mostramos el resultado final
							ctx.fillText(Number(valoresFinales[i]).toFixed(2), model.x, y_pos);
						}
						//if(i==(dataset.data.length-1))
						//ctx.fillText(Number(valoresFinales[i]).toFixed(2), model.x, y_pos);
                    }
					a++;
                });
            }
        },
        scales: {
            yAxes: [{
				stacked: true,
                ticks: {
                    beginAtZero:true
                },
				scaleLabel: {
					display: true,
					labelString: 'USD',
					fontSize: 30
				}
            }],
            xAxes: [{
				stacked: true,
                ticks: {
                    beginAtZero:true
                },
				scaleLabel: {
					display: true,
					labelString: 'Date',
					fontSize: 30
				}
            }]
			
        }
    }
});
document.getElementById('datos').style.display = 'none';

function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}