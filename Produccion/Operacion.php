<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['part'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
	else if (!isset($_GET['num']) || !isset($_GET['name'])) { //validar que exista algún valor en 'part' y 'name'
		print '<script language="JavaScript">'; 
		//print "alert('Select a Part to show Operations.');"; 
		print "window.location='Parte.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">'; 
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<?php 
		$numpart = $_GET['num']; //Obtenemos el numero de parte
		$namepart = $_GET['name']; //Obtenemos el nombre de parte
		?>
		
		<div class="container main-content">
			<div class="row">
				<h1> <?php echo $namepart." - Operations"; ?> </h1>
			</div>
			
			<!--FORM-->
			<div class="col-md-10"><input type="hidden" class="form-control"></div>
			<div class="col-md-2"><button type="button" onclick="mostrar()" id="balta" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Operation</button></div>		
			<div class="row" id="alta" style="display:none;">
				<table class="table">
					<tbody><tr class="thead-inverse" style="text-align:center; border-style:ridge; border-width: 1px; border-color:#fff;"><td>
						<div class="row" style="padding-left: 20%;">
						  <div class="col-md-6" style="padding-left: 3%;"><input type="text" id="descs" class="form-control" placeholder="Description"></div>
						  <div class="col-md-2" style="padding-left: 10%;"><button type="button" onclick="add(<?php echo $numpart; ?>)" class="btn btn-success btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Add</button></div>
						  <div class="col-md-2" style="padding-left: 0%;"><button type="button" onclick="ocultar()" id="bocultar" style="display:none;" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Close</button></div>
						</div>
					</td></tr></tbody>
				</table>
			</div>
			
			
			<!--TABLA-->
			<br><br><input type="hidden" id="parteid" value="<?=$numpart;?>" />
			<div id="operaciones"> 
			
			</div>
		</div>
		
        
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<script type="text/javascript">
		function mostrar(){ //Mostrar el contenido para dar de alta
			document.getElementById('alta').style.display = 'block';
			document.getElementById('balta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'block';
		}
		function ocultar(){ //Ocultar el contenido para dar de alta
			document.getElementById('alta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'none';
			document.getElementById('balta').style.display = 'block';
		}
	</script>
	<script type="text/javascript" src="js/operation.js"></script>

</html>