<?php
require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['part'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
	else if (!isset($_GET['numop']) || !isset($_GET['desc'])) { //validar que exista algún valor en 'numop' y 'desc'
		print '<script language="JavaScript">'; 
		//print "alert('Select a Operation to show Tools.');"; 
		print "window.location='Operacion.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">'; 
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

require("API/connection.php");
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
			$conn = connect();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<?php
		$numop = $_GET['numop']; //Obtenemos el numero de operacion
		$desc = $_GET['desc']; //Obtenemos el nombre de operacion
		$numpart = $_GET['nump']; //Obtenemos el numero de parte
		$namepart = $_GET['namep']; //Obtenemos el nombre de parte
		?>
		
		<div class="container main-content">
			<div class="row">
				<h1> <?php echo $namepart.": ".$desc." - Tools"; ?> </h1>
			</div>
			
			<input type="hidden" id="opid" value="<?=$numop;?>" />
			<input type="hidden" id="parteid" value="<?=$numpart;?>" />
			<!--FORM-->
			<div class="col-md-10"><input type="hidden" class="form-control"></div>
			<div class="col-md-2"><button type="button" onclick="mostrar()" id="balta" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Tool</button></div>
			<div class="row" id="alta" style="display:none;">
				<table class="table">
					<tbody><tr class="thead-inverse" style="text-align:center; border-style:ridge; border-width: 1px; border-color:#FFF;"><td>
						<div class="row">
							<div class="col-md-5">
							<select id="seleccion">
							  <option>Select an option...</option>
							  <?php 
								$query = "select * from herramienta;";
								$resultado = $conn->query($query);
								for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) { //Ciclo para mostrar todos los datos de la base de datos de Herramientas
									echo "<option value='".$fila[0]."'>".$fila[1]." - ".$fila[6]."</option>";
								}
							?>
							</select>
							</div>
						  <div class="col-md-1"><button type="button" onclick="asignar(<?php echo $numop; ?>)" form="falta" class="btn btn-success btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Assign </button></div>
						  <div class="col-md-1" style="right: 0;"><button type="button" onclick="ocultar()" id="bocultar" style="display:none;" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Close</button></div>
						</div>
					</td></tr></tbody>
				</table>
			</div>
			
			<!--TABLA-->
				<br><br>
			<div id="herramientas"> 
				
			</div>
		</div>
		
		
        
        <?php
			disconnect($conn);
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<script type="text/javascript">
		function mostrar(){ //Mostrar el contenido para dar de alta
			document.getElementById('alta').style.display = 'block';
			document.getElementById('balta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'block';
		}
		function ocultar(){//Ocultar el contenido para dar de alta
			document.getElementById('alta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'none';
			document.getElementById('balta').style.display = 'block';
		}
	</script>
	<script type="text/javascript" src="js/herr.js"></script>

</html>