var cants = Number(document.getElementById('ress').value); //cantidad de filas de la consulta
//alert(cants);

var opsherr = Number(document.getElementById("nopss").value); //cantidad de herramientas que apareceran en cada operacion
var opherr = [];
var colores = [];//Array donde se guardará un color para cada operacion
for(i=0; i<opsherr;i++){ //Método que guardará todas las herramientas por operación para la gráfica
	opherr[i] = document.getElementById("opherrs"+i).value;
}
//alert(opherr);

var total = [];
var band = 0;
var x = 0;
for(i=0; i<cants;){ // Guardamos el costo total validando que cada operación sea la correcta
	var nombre = document.getElementById("descops"+i).value + " " + document.getElementById("descs"+i).value;
	if(nombre == opherr[band]){
		x += (Number(document.getElementById("cants"+i).value)*Number(document.getElementById("precios"+i).value));
		total[band] = Number(x).toFixed(2) + 0.0;
		i++;
	}
	else{
		x = 0;
		band++;
	}
	nombre = "";
}

for(i=0; i<total.length; i++){
	if(total[i] == null) total[i] = 0;
}
//alert(total);

var swapped;
do { //Método Bubble Sort para ordenar de mayor a menor los costos generados
	swapped = false;
	for (i=0; i < total.length-1; i++) {
		if (Number(total[i]) < Number(total[i+1])) {
			var temp = Number(total[i]);
			total[i] = Number(total[i+1]);
			total[i+1] = Number(temp);
			
			var temp2 = opherr[i];
			opherr[i] = opherr[i+1];
			opherr[i+1] = temp2;
			swapped = true;
		}
	}
} while (swapped);

var suma = 0;
for(i=0; i<total.length; i++){
	suma += Number(total[i]);
}
document.getElementById('pareto').value = suma.toFixed(2);

var datasetValue = [];
var newDataSet =
{
	label: 'Cost Pareto',
	data: total,
	backgroundColor: 'rgba(68, 160, 209, 1)',
	borderColor: 'rgba(68, 160, 209, 1)',
	borderWidth: 1
}
datasetValue[0] = newDataSet;

var ctx = document.getElementById("myChart2");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: opherr,
		datasets: datasetValue //Cada dataset debe ser una operación diferente
    },
    options: {
		animation: {
            duration: 500,
            easing: "easeOutQuart",
            onComplete: function () {
                var ctx = this.chart.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function (dataset) {
                    for (var i = 0; i < dataset.data.length; i++) {
                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                            scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
                        ctx.fillStyle = '#444';
                        var y_pos = model.y - 5;
                        // Make sure data value does not get overflown and hidden
                        // when the bar's value is too close to max value of scale
                        // Note: The y value is reverse, it counts from top down
                        if ((scale_max - model.y) / scale_max >= 0.93)
                            y_pos = model.y + 20; 
                        ctx.fillText(dataset.data[i], model.x, y_pos);
                    }
                });               
            }
        },
        scales: {
            yAxes: [{
				stacked: true,
                ticks: {
                    beginAtZero:true
                },
				scaleLabel: {
					display: true,
					labelString: 'USD',
					fontSize: 30
				}
            }],
            xAxes: [{
				stacked: true,
                ticks: {
                    beginAtZero:true
                },
				scaleLabel: {
					display: true,
					labelString: 'Tools',
					fontSize: 30
				}
            }]
        }
    }
});

document.getElementById('datos2').style.display = 'none';
