<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['tool'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers and SHMM employes.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">';  
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
	<div class="container main-content">
		<div class="row">
			<h1> Tools <!--<small> Ver. 0.41 Beta </small>--></h1>
		</div>
		
		<!--FORM-->
		<div class="col-md-10"><input type="hidden" class="form-control"></div>
		<div class="col-md-2"><button type="button" onclick="mostrar()" id="balta" class="btn btn-primary btn-block"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;  Add Tool</button></div>
		<div class="row" id="alta" style="display:none;">
			<table class="table">
				<tbody><tr class="thead-inverse" style="text-align:center; border-style:ridge; border-width: 1px; border-color:#FFF;"><td>
					<div class="row">
					 <div class="col-md-2"><input type="text" id="descs" name="descs" class="form-control" placeholder="Description" /></div>
					  <div class="col-md-2"><input type="text" id="times" name="times" class="form-control" placeholder="Time of Life" /></div>
					  <div class="col-md-1"><input type="text" id="prices" name="prices" class="form-control" placeholder="Price" /></div>
					  <div class="col-md-2"><input type="text" id="binlocs" name="binlocs" class="form-control" placeholder="Binloc" /></div>
					  <div class="col-md-2"><input type="text" id="semxs" name="semxs" class="form-control" placeholder="SEMX code" /></div>
					  <div class="col-md-3"><input type="text" id="shmms" name="shmms" class="form-control" placeholder="SHMM code" /></div>
					  <div class="col-md-4" style="padding-left:90%;"><button type="button" onclick="add()" form="falta" class="btn btn-success btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Add</button></div>
					  <div class="col-md-4" style="padding-left:90%;"><button type="button" onclick="ocultar()" id="bocultar" style="display:none;" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Close</button></div>
					</div>
				</td></tr></tbody>
			</table>
		</div>
		
		<!--TABLA-->
		<br><br>
		<div id="herramientas"> 
		
		</div>
	</div>
	
		
		
        
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>

	<!-- Dependencies -->
	<script src="jquery/jquery.js" type="text/javascript"></script>
	<script src="jquery/jquery.ui.draggable.js" type="text/javascript"></script>

	<!-- Core files -->
	<script src="jquery/jquery.alerts.js" type="text/javascript"></script>
	<link href="jquery/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript">
		function mostrar(){ //Mostrar el contenido para dar de alta
			document.getElementById('alta').style.display = 'block';
			document.getElementById('balta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'block';
		}
		function ocultar(){ //Ocultar el contenido para dar de alta
			document.getElementById('alta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'none';
			document.getElementById('balta').style.display = 'block';
		}
	</script>

	<script type="text/javascript" src="js/herrabc.js"></script>
</html>