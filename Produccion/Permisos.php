<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['user'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">'; 
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<div class="container main-content">
			
			<div class="row searchDiv">
				<div class="col-md-3">
					<h1>Permissions</h1>
				</div>
				<div class="col-md-6">
					<div class="input-group searchbar" id="searchBar">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
						<input type="text" id="searchBarInput" class="form-control" placeholder="Search an user..." aria-describedby="basic-addon1">
					</div>
				</div>              
			</div>
			
			<!--FORM-->
			<div class="row" id="alta">
				<table class="table">
					<tbody><tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;"></tr>
					
					<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">				
						<td class="col-md-6"><div class="row">
							
								<!-- Mostrar todos los Usuarios existentes -->
								<div id="permisos">
								
								</div>
							
							<br>
						</div></td>
					</tr>
					
					</tbody>
				</table>
			</div>	
				
		</div>
        
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<script type="text/javascript" src="js/usuarioperm.js"></script>
	<!--<script type="text/javascript" src="js/permiso.js"></script> -->
	<script type="text/javascript">
		document.getElementById("searchBarInput").focus(); //Mantener el cursor en el buscador
		function ocultar(no){
			document.getElementById('perm1'+no).style.display = 'none';
			document.getElementById('perm2'+no).style.display = 'none';
			document.getElementById('perm3'+no).style.display = 'none';
			document.getElementById('b_send'+no).style.display = 'none';
			document.getElementById('b_show'+no).style.display = 'block';
			document.getElementById('b_del'+no).style.display = 'none';
		}
		
		function show(no){
			document.getElementById('perm1'+no).style.display = 'block';
			document.getElementById('perm2'+no).style.display = 'block';
			document.getElementById('perm3'+no).style.display = 'block';
			document.getElementById('b_send'+no).style.display = 'block';
			document.getElementById('b_show'+no).style.display = 'none';
			document.getElementById('b_del'+no).style.display = 'block';
		}
	</script>

</html>