<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['change'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">'; 
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

require("API/connection.php");
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>
	<link type="text/css" rel="stylesheet" href="css/bootstrap-datepicker.css"  media="screen,projection"/>

    <body>
        <?php 
            navbar();
			$conn = connect();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<div class="container main-content">
			<div class="row">
				<h1> Tool Change Register </h1>
			</div>
			
			<ul class="nav nav-tabs">
				<li id="register" class="active"><a data-toggle="tab" href="#alta">Register</a></li>
				<li id="edit"><a data-toggle="tab" href="#baja">Edit</a></li>
			</ul>
			
		<div class="tab-content" id="todo">
			<div class="tab-pane fade in active" id="alta">
				<table class="table">
					<tbody><tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;"></tr>
					
					<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
						<td class="col-md-3"><div class="row">
							<?php $fecha = getdate();
							$dateactual = $fecha['weekday'].", ".$fecha['month']." ".$fecha['mday'].", ".$fecha['year'].".";
							?>
						 <div class="col-md-2"> <label>Date: </label></div> <?php //print_r($dateactual); ?> <!-- Mostrando la fecha actual-->
							<div class="col-md-2">
								<input type="text" class="form-control" id="startd" name="startd" placeholder="Start Date" onkeypress="return false;"/>
							</div>
						</div></td>
					</tr>
					
					<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">				
						<td class="col-md-6"><div class="row">
							<!-- Mostrar todas las partes existentes -->
							<div class="col-md-2"><label>Parts: </label></div>
							<select id="selPart" class="selectpicker" onchange="getLines(<?php echo $_SESSION['loggedin'];?>)"> <!-- Combobox donde se muestran todas las partes.-->
							  <option>Select an option...</option>
							  <?php 
								$query = "select * from parte;";
								$resultado = $conn->query($query);
								for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) { //Ciclo para mostrar todos los datos de la base de datos de PARTES
									echo "<option value='".$fila[0]."'> ".$fila[1]."</option>";
								}
							?>
							</select>
						</div></td>
					</tr>
						
					<tr class="thead-inverse" id="trop" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
						<td class="col-md-6"><div id="alllines" class="row">					
							
						</div></td>
					</tr>
					
					<tr class="thead-inverse" id="trop" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
						<td class="col-md-6"><div id="allop" class="row">					
							
						</div></td>
					</tr>
					
					<tr class="thead-inverse" id="trtool" style="border-style:ridge; border-width: 1px; border-color:#FFF; ">
						<td class="col-md-3"><div id="alltool" class="row">		
							
						</div></td>
					</tr>
					
					<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
						<td class="col-md-3"><div class="row">
							<div class="col-md-1"><button type="button" id="buttonSend" onclick="changeRegister()" form="" class="btn btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Send </button></div>
						</div></td>
					</tr>
							
					</td></tr></tbody>
				</table>
			</div>
			
			
			
			<div class="tab-pane fade" id="baja">
				<table class="table">
					<tbody><tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;"></tr>
					
					<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
						<td class="col-md-3"><div class="row">
							<?php $fecha = getdate();
							$dateactual = $fecha['weekday'].", ".$fecha['month']." ".$fecha['mday'].", ".$fecha['year'].".";
							?>
						 <div class="col-md-2"> <label>Date: </label></div> <?php //print_r($dateactual); ?> <!-- Mostrando la fecha actual-->
							<div class="col-md-2">
								<input type="text" class="form-control" id="startd1" name="startd1" placeholder="Start Date" onkeypress="return false;"/>
							</div>
						</div></td>
					</tr>
					
					<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">				
						<td class="col-md-6"><div class="row">
							<!-- Mostrar todas las partes existentes -->
							<div class="col-md-2"><label>Parts: </label></div>
							<select id="selPart1" class="selectpicker" onchange="getLines1()"> <!-- Combobox donde se muestran todas las partes.-->
							  <option>Select an option...</option>
							  <?php 
								$query = "select * from parte;";
								$resultado = $conn->query($query);
								for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) { //Ciclo para mostrar todos los datos de la base de datos de PARTES
									echo "<option value='".$fila[0]."'> ".$fila[1]."</option>";
								}
							?>
							</select>
						</div></td>
					</tr>
						
					<tr class="thead-inverse" id="trline1" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
						<td class="col-md-6"><div id="alllines1" class="row">					
							
						</div></td>
					</tr>
					
					<tr class="thead-inverse" id="trop1" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
						<td class="col-md-6"><div id="allop1" class="row">					
							
						</div></td>
					</tr>
					
					<tr class="thead-inverse" id="trtool1" style="border-style:ridge; border-width: 1px; border-color:#FFF; ">
						<td class="col-md-3"><div id="alltool1" class="row">		
							
						</div></td>
					</tr>
							
					</td></tr></tbody>
				</table>
			</div>
		</div>
		
		</div>
		<?php disconnect($conn); ?>
		
        
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
	<script>
		$(document).ready(function(){
  
			$("#startd").datepicker({
				todayBtn:  1,
				autoclose: true,
				format: 'yyyy-mm-dd',
			});
			
			$("#startd1").datepicker({
				todayBtn:  1,
				autoclose: true,
				format: 'yyyy-mm-dd',
			});
		});
	</script>

	<script type="text/javascript" src="js/cambioLine.js"></script>
	<script type="text/javascript" src="js/cambio.js"></script>
	<script type="text/javascript" src="js/cambioHerr.js"></script>
	<script type="text/javascript" src="js/viewLine.js"></script>
	<script type="text/javascript" src="js/view.js"></script>
	<script type="text/javascript" src="js/viewHerr.js"></script>
</html>