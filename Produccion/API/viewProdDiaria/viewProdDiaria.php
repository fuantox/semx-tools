<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

		case "DELETE":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
			$idp = explode(",", $params[4]);
            
			for($i=0; $i<count($idp); $i++){
				$query="delete from produccion_diaria where idProd = $idp[$i] ;";
				$conn->query($query); // ejecuta la consulta para traer al cliente 			
			}
			echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "ids" => $params[4]]);
			
        break;
		
		case "PUT":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
			
			if($params[5]==null && $params[6]==null && $params[7]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                $cants = explode(",", $params[4]);
				$idl = explode(",", $params[5]);
				$idp = explode(",", $params[6]);
				$secciones = explode(",", $params[7]);
				
				$a=0;
				for($i=0; $i<count($idl);){
					$seccion = $secciones[$i];
					$linea = $idl[$i];
					while($secciones[$i]==$seccion && $idl[$i]==$linea){
						$query="update produccion_diaria set cantPiezas = $cants[$a] where idProd = $idp[$i] ;";
						$conn->query($query); // ejecuta la consulta para traer al cliente 
						$i++;
						if($i==count($idl)) break;
					}
					$a++;
				}
				
               echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "ids" => $params[6]]);
            }
            echo json_encode(["result" => "put", "uri" => $_SERVER["REQUEST_URI"]]);
        break;
        
        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $piezas = $_POST['cants'];
            $fechas = mysql_escape_mimic($_POST['fecha']);
            $idop = $_POST['ops'];
			$idpart = $_POST['parts'];
            $query = "INSERT INTO produccion_diaria
                (cantPiezas, fecha, idOperacion)
                VALUES ('$piezas', '$fechas', '$idop');";        
            if($conn->query($query) != TRUE){
                echo json_encode(["result" => "error", "msg" => $conn->error, "q" => $query, $request->{'id'}]);
                disconnect($conn);
            }
            echo json_encode(["result" => "success"]);
        break;

        case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				
				$fecha = $_GET['fecha'];
				$query = "select o.descripcion, l.idLinea, l.nombre, pd.cantPiezas, pd.idProd, lh.seccion
				from operacion o, linea l, linea_has_operacion lh, parte p, produccion_diaria pd
				where p.numParte = $term
				and l.Parte_numParte = p.numParte 
				and lh.Linea_idLinea = l.idLinea
				and o.numOperacion = lh.Operacion_numOperacion 
				and pd.idOperacion = o.numOperacion
				and pd.idLinea = l.idLinea
				and pd.fecha = '$fecha'
				order by pd.idProd
				";
                $result = $conn->query($query);
                $response = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
            else{
                $query = "select * FROM operacion  ; ";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 