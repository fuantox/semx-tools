<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

        
        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $fecha = $_POST['fecha'];
			$li = $_POST['lineas'];
			$linea = explode(",", $li);
			$cants1 = $_POST['cants1'];
			$cants2 = $_POST['cants2'];
			$cants3 = $_POST['cants3'];
			$cants1 = explode(",", $cants1);
			$cants2 = explode(",", $cants2);
			$cants3 = explode(",", $cants3);
			
			for($i=0; $i<count($linea); $i++){
				if($cants1[$i]!="-1"){
					$consulta = "select o.numoperacion, o.descripcion, l.idLinea
					from operacion o, linea l, linea_has_operacion lo
					where l.idLinea = $linea[$i] 
					and l.idLinea = lo.Linea_idLinea
					and lo.Operacion_numOperacion = o.numOperacion
					and lo.seccion = 1;";
					$resultado = $conn->query($consulta);
					
					for ($x=0; $row = mysqli_fetch_array($resultado); $x++) { 
						$query="insert into produccion_diaria (cantPiezas, fecha, idOperacion, idLinea) 
								values($cants1[$i], '$fecha', $row[0], $linea[$i]);";
						$conn->query($query); // ejecuta la consulta para traer al cliente
					}
				}

				if($cants2[$i]!="-1"){
					$consulta = "select o.numoperacion, o.descripcion, l.idLinea
					from operacion o, linea l, linea_has_operacion lo
					where l.idLinea = $linea[$i] 
					and l.idLinea = lo.Linea_idLinea
					and lo.Operacion_numOperacion = o.numOperacion
					and lo.seccion = 2;";
					$resultado = $conn->query($consulta);
					
					for ($x=0; $row = mysqli_fetch_array($resultado); $x++) { 
						$query="insert into produccion_diaria (cantPiezas, fecha, idOperacion, idLinea) 
								values($cants2[$i], '$fecha', $row[0], $linea[$i]);";
						$conn->query($query); // ejecuta la consulta para traer al cliente
					}
				}
				
				if($cants3[$i]!="-1"){
					$consulta = "select o.numoperacion, o.descripcion, l.idLinea
					from operacion o, linea l, linea_has_operacion lo
					where l.idLinea = $linea[$i] 
					and l.idLinea = lo.Linea_idLinea
					and lo.Operacion_numOperacion = o.numOperacion
					and lo.seccion = 3;";
					$resultado = $conn->query($consulta);
					
					for ($x=0; $row = mysqli_fetch_array($resultado); $x++) { 
						$query="insert into produccion_diaria (cantPiezas, fecha, idOperacion, idLinea) 
						values($cants3[$i], '$fecha', $row[0], $linea[$i]);";
						$conn->query($query); // ejecuta la consulta para traer al cliente
					}
				}
			}
			
			echo json_encode(["result" => "success"]);
        break;

        case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				$fecha = $_GET['fecha'];
				
				$query = "select o.descripcion, l.idLinea, l.nombre, pd.cantPiezas, pd.idProd, lh.seccion
				from operacion o, linea l, linea_has_operacion lh, parte p, produccion_diaria pd
				where p.numParte = $term
				and l.Parte_numParte = p.numParte 
				and lh.Linea_idLinea = l.idLinea
				and o.numOperacion = lh.Operacion_numOperacion 
				and pd.idOperacion = o.numOperacion
				and pd.idLinea = l.idLinea
				and pd.fecha = '$fecha'
				order by pd.idProd
				";
                $result = $conn->query($query);
				$cants = mysqli_num_rows($result);
				
				if($cants == 0){ //Si no hay registros, mostramos la tabla para ingresar los nuevos registros
					$query = "SELECT l.idLinea, l.nombre, 
					GROUP_CONCAT(case when lo.seccion=1 then o.descripcion else null end order by o.descripcion SEPARATOR ' - ') as ops1, 
					GROUP_CONCAT(case when lo.seccion=2 then o.descripcion else null end order by o.descripcion SEPARATOR ' - ') as ops2, 
					GROUP_CONCAT(case when lo.seccion=3 then o.descripcion else null end order by o.descripcion SEPARATOR ' - ') as ops3 
					FROM Linea as l, Operacion as o, Linea_has_Operacion as lo, Parte as p 
					WHERE p.numParte = $term 
					and o.Parte_numParte = p.numParte 
					and l.Parte_numParte = p.numParte 
					and o.Parte_numParte = l.Parte_numParte 
					and o.numOperacion = lo.Operacion_numOperacion 
					and l.idLinea = lo.Linea_idLinea 
					group by l.idLinea 
					order by l.idLinea ; ";
					$result = $conn->query($query);
					$response = [];
					while ($row = mysqli_fetch_assoc($result)) {
						$response[] = $row;
					}
					echo json_encode($response);
				}
				else if($cants > 0){
					echo json_encode(["result" => "error"]);
				}
            }
            else{
                $query = "select * FROM operacion  ; ";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 