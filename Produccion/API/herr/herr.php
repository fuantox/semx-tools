<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

        
        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $idh = $_POST['idh'];
			$idop = $_POST['idop'];
			
			$query="insert into operacion_has_herramienta values (".$idop.",".$idh.");";
			if($conn->query($query) != TRUE){
                echo json_encode(["result" => "error", "msg" => $conn->error, "q" => $query]);
                disconnect($conn);
            }
			else echo json_encode(["result" => "success"]);
        break;
		
		case "DELETE":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null || $params[5]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[4]) && is_numeric($params[5])){
                    $idop = sprintf('%05d', $params[4]);
					$idherr = sprintf('%05d', $params[5]);
                }
                else{
                    $idop = $params[4];
					$idherr = $params[5];
                }
				$querys = "delete from operacion_has_herramienta 
						where operacion_numOperacion = $idop 
						and herramienta_numHerramienta = $idherr;";
				
				if ($conn->query($querys)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "herr" => $params[4], "op" => $params[5], "query" => $querys]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
			
        break;
		
        case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				
				$query = "select * from herramienta,operacion_has_herramienta 
						where operacion_has_herramienta.operacion_numoperacion = $term 
						and herramienta.numHerramienta = operacion_has_herramienta.Herramienta_numHerramienta;";
                $result = $conn->query($query);
				$response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
				echo json_encode($response);
            }
            else{
                $query = "select * FROM parte ; ";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 