<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
		
		case "POST":
            $request = json_decode(file_get_contents('php://input'));
			
			
			$fecha = $_POST['fecha'];
			$numpart = $_POST['numpart'];
			$numop = $_POST['numop'];
			$numline = $_POST['linea'];
			$hsels = $_POST['hsels'];
			$hsels = explode(",", $hsels);
			$cants = $_POST['cants'];
			$cants = explode(",", $cants);
			$razones = $_POST['razones'];
			$razones = explode(",", $razones);
			
			
			for($i=0; $i<count($hsels); $i++){
				$query="insert into cambio (idOp, idHerr, cant, razon, fecha, idLinea) 
						values($numop, $hsels[$i], $cants[$i], $razones[$i], '$fecha', $numline);";
				$conn->query($query);
			}
			echo json_encode(["result" => "success"]);
        break;

        case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
				$idparte = $_GET['idpart'];
				
				$query = "select herramienta.numHerramienta, herramienta.descripcion, 
				herramienta.shmmcode from herramienta, operacion_has_herramienta, operacion, 
				parte where parte.numParte = operacion.Parte_numParte and parte.numParte = $idparte 
				and operacion.numOperacion = $term 
				and operacion.numOperacion = operacion_has_herramienta.Operacion_numOperacion 
				and herramienta.numHerramienta = operacion_has_herramienta.Herramienta_numHerramienta;";
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				$result = $conn->query($query);
                $response = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
            else{
                $query = "select herramienta.numHerramienta, herramienta.descripcion from herramienta, operacion_has_herramienta, operacion, parte where parte.numParte = operacion.Parte_numParte and parte.numParte = $idparte and operacion.numOperacion = $term and operacion.numOperacion = operacion_has_herramienta.Operacion_numOperacion and herramienta.numHerramienta = operacion_has_herramienta.Herramienta_numHerramienta;";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 