<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

        
        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $desc = $_POST['desc'];
			$numpart = $_POST['numpart'];
			
			$query="insert into operacion (descripcion, parte_numparte) 
					values('$desc', $numpart);";
			
			if($conn->query($query) != TRUE){
                echo json_encode(["result" => "error", "msg" => $conn->error, "q" => $query]);
                disconnect($conn);
            }
			
			echo json_encode(["result" => "success"]);
        break;
		
		case "DELETE":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[4])){
                    $idop = sprintf('%05d', $params[4]);
                }
                else{
                    $idop = $params[4];
                }
				$querys="delete from operacion where numOperacion = $idop ;";
				if ($conn->query($querys)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4], "query" => $querys]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
			
        break;
		
		case "PUT":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[4])){
                    $id = sprintf('%05d', $params[4]);
                }
                else{
                    $id = $params[4];
                }
                $query = "update operacion 
						set descripcion = '$params[5]' 
						where numOperacion = $id ;";
                $query = str_replace("%20"," ",$query);
                echo $query;
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
            echo json_encode(["result" => "put", "uri" => $params[4]]);
        break;

        case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				
				$query = "select o.numOperacion, o.descripcion, p.numparte, p.nombre 
						from operacion o, parte p
						where p.numParte = o.Parte_numParte
						and p.numParte = $term
						order by o.descripcion;";
                $result = $conn->query($query);
				$response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
				echo json_encode($response);
            }
            else{
                $query = "select * FROM parte ; ";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 