<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

		case "DELETE":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
			$idChange = $params[4];
            $query="delete from cambio where idCambio = $idChange ;";
			if ($conn->query($query)) {
				echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
			} else {
				echo json_encode(["result" => "error", "msg" => $conn->error]);
			}
        break;
        
		case "PUT":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                /*if(is_numeric($params[4])){
                    $idcambio = sprintf('%05d', $params[4]);
                }
                else{
                    $idcambio = $params[4];
                }*/
				$idcambio = $params[4];
                $query="update cambio set 
						cant = $params[5], 
						razon = $params[6] 
						where idcambio = $idcambio ;";
                /*$query = str_replace("%20"," ",$query);
                echo $query;*/
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
            echo json_encode(["result" => "put", "uri" => $_SERVER["REQUEST_URI"]]);
        break;

		case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
				$idparte = $_GET['idpart'];
				$idline = $_GET['idline'];
				$fecha = $_GET['fecha'];
				
				$query = "select c.idCambio, c.cant, c.idOp, c.fecha, c.razon, h.numHerramienta, concat(h.descripcion, ' - ', h.shmmcode) descripcion,
						c.idLinea
						from cambio c, parte p, operacion o, herramienta h, operacion_has_herramienta oh, linea l
						where p.numParte = o.Parte_numParte 
						and p.numParte = $idparte
						and o.numOperacion = $term
						and l.idLinea = $idline
						and o.numOperacion = oh.Operacion_numOperacion 
						and h.numHerramienta = oh.Herramienta_numHerramienta
						and o.numOperacion =  c.idOp
						and h.numHerramienta = c.idHerr
						and c.idLinea = l.idLinea
						and c.fecha = '$fecha';";
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				$result = $conn->query($query);
                $response = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
            else{
                $query = "select herramienta.numHerramienta, herramienta.descripcion from herramienta, operacion_has_herramienta, operacion, parte where parte.numParte = operacion.Parte_numParte and parte.numParte = $idparte and operacion.numOperacion = $term and operacion.numOperacion = operacion_has_herramienta.Operacion_numOperacion and herramienta.numHerramienta = operacion_has_herramienta.Herramienta_numHerramienta;";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 