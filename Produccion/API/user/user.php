<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

        
        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $id = $_POST['idu'];
			$name = $_POST['nameu'];
			$last = $_POST['lastu'];
			$pass = $_POST['passu'];
			$job = $_POST['jobu'];
			
			$newPass = md5($pass); //Encriptamos la contraseña
			
			$query="INSERT into usuario(idUsuario, contrasena, nombre, apellido, puesto) 
					values ('$id', '$newPass', '$name', '$last', '$job');";
					
			if($conn->query($query) != TRUE){
                echo json_encode(["result" => "error", "msg" => $conn->error, "q" => $query]);
                disconnect($conn);
            }
			else echo json_encode(["result" => "success"]);
        break;
		
		case "DELETE":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                $id = $params[4];
                
                $query = "delete from usuario where idUsuario = '$id'"; 
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
			
        break;
		
		case "PUT":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                $id = $params[4];
				/*$name = html_entity_decode($params[5], ENT_QUOTES | ENT_HTML401, "UTF-8");
				$lastn = html_entity_decode($params[6], ENT_QUOTES | ENT_HTML401, "UTF-8");*/
				
				$name = utf8_decode($params[5]);
				$lastn = utf8_decode($params[6]);
                
				$query = "UPDATE usuario SET 
						nombre='$name', 
						apellido='$lastn', 
						puesto='$params[7]' 
						WHERE idUsuario = '$params[4]'";
                $query = str_replace("%20"," ",$query);
                echo $query;
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
            echo json_encode(["result" => "put", "uri" => $params[4]]);
        break;

        case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				
				$query = "select * from usuario;";
                $result = $conn->query($query);
				$response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
				echo json_encode($response);
            }
            else{
                $query = "select * FROM usuario ; ";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 