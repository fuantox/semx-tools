<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

        
        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $desc = $_POST['desc'];
			$time = $_POST['time'];
			$price = $_POST['price'];
			$binloc = $_POST['binloc'];
			$semx = $_POST['semx'];
			$shmm = $_POST['shmm'];
			
			$query="insert into herramienta (descripcion, tiempovida, precio, binloc, semxcode, shmmcode) 
			values('$desc', $time, $price, '$binloc', '$semx', '$shmm');";
			
			if($conn->query($query) != TRUE){
                echo json_encode(["result" => "error", "msg" => $conn->error, "q" => $query]);
                disconnect($conn);
            }
			
			echo json_encode(["result" => "success"]);
        break;
		
		case "DELETE":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[4])){
                    $id = sprintf('%05d', $params[4]);
                }
                else{
                    $id = $params[4];
                }
                $query = "delete from herramienta where numHerramienta = $id ;";
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
			
        break;
		
		case "PUT":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[4])){
                    $id = sprintf('%05d', $params[4]);
                }
                else{
                    $id = $params[4];
                }
                $query = "update herramienta set 
						descripcion = '$params[5]', 
						tiempoVida = $params[6], 
						precio = $params[7], 
						binloc = '$params[8]', 
						semxcode = '$params[9]', 
						shmmcode = '$params[10]' 
						where numHerramienta = $id ;";
                $query = str_replace("%20"," ",$query);
                echo $query;
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
            echo json_encode(["result" => "put", "uri" => $params[4]]);
        break;

        case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				
				$query = "select * from herramienta;";
                $result = $conn->query($query);
				$response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
				echo json_encode($response);
            }
            else{
                $query = "select * FROM herramienta ; ";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 