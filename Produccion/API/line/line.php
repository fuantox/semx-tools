<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

        
        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $numpart = $_POST['numpart'];
			$desc = $_POST['desc'];
			
			if(isset($_POST['start1']) && isset($_POST['end1']) || isset($_POST['start2']) && isset($_POST['end2']) || isset($_POST['start3']) && isset($_POST['end3'])){
				$query="insert into linea (nombre, parte_numparte) values('$desc', $numpart)";
				$conn->query($query); // ejecuta la consulta para traer al cliente 
			}
			
			$start1="";
			$end1="";
			if(isset($_POST['start1']) && isset($_POST['end1'])){
				$start1 = $_POST['start1'];
				$end1 = $_POST['end1'];
				$query = "select numoperacion from operacion 
						where descripcion between '$start1' and '$end1' 
						and Parte_numParte=$numpart order by descripcion;";
				$resultado = $conn->query($query);
				while ($fila = mysqli_fetch_array($resultado)) {
					$query2 = "insert into linea_has_operacion values ((select MAX(idlinea) from linea), $fila[0], 1);";
					$resultado2 = $conn->query($query2);
				}
			}
			$start2="";
			$end2="";
			if(isset($_POST['start2']) && isset($_POST['end2'])){
				$start2 = $_POST['start2'];
				$end2 = $_POST['end2'];
				$query = "select numoperacion from operacion where descripcion between '$start2' and '$end2' and Parte_numParte=$numpart order by descripcion;";
				$resultado = $conn->query($query);
				while ($fila = mysqli_fetch_array($resultado)) {
					$query2 = "insert into linea_has_operacion values ((select MAX(idlinea) from linea), $fila[0], 2);";
					$resultado2 = $conn->query($query2);
				}
			}
			$start3="";
			$end3="";
			if(isset($_POST['start3']) && isset($_POST['end3'])){
				$start3 = $_POST['start3'];
				$end3 = $_POST['end3'];
				$query = "select numoperacion from operacion where descripcion between '$start3' and '$end3' and Parte_numParte=$numpart order by descripcion;";
				$resultado = $conn->query($query);
				while ($fila = mysqli_fetch_array($resultado)) {
					$query2 = "insert into linea_has_operacion values ((select MAX(idlinea) from linea), $fila[0], 3);";
					$resultado2 = $conn->query($query2);
				}
			}
			if(!isset($_POST['start1']) && !isset($_POST['end1']) && !isset($_POST['start2']) && !isset($_POST['end2']) && !isset($_POST['start3']) && !isset($_POST['end3'])){
				echo json_encode(["result" => "error", "msg" => "No operations were assigned to the line.", "q" => $query]);
                disconnect($conn);
			}
			
			echo json_encode(["result" => "success"]);
        break;
		
		case "DELETE":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[4])){
                    $id = sprintf('%05d', $params[4]);
                }
                else{
                    $id = $params[4];
                }
                $querys="delete from linea where idLinea = $id ;";
				if ($conn->query($querys)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error, "uri" => $uri]);
                }
            }
			
        break;
		
		case "PUT":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[4])){
                    $id = sprintf('%05d', $params[4]);
                }
                else{
                    $id = $params[4];
                }
				
				$a = $params[5];
				$pos = strpos($a, "%23");
				if($pos !== false) {
					$a = split("%23", $params[5]);
					$desc = $a[0]."#".$a[1];
				}
				else $desc = $params[5];
				
                $query = "update linea 
						set nombre = '$desc' 
						where idlinea = $id ;";
						
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
            echo json_encode(["result" => "put", "uri" => $params[4]]);
        break;

        case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				
				$query = "select l.idlinea, l.nombre, 
						GROUP_CONCAT(case when lo.seccion=1 then o.descripcion else null end order by o.descripcion SEPARATOR ' - ') ops1, 
						GROUP_CONCAT(case when lo.seccion=2 then o.descripcion else null end order by o.descripcion SEPARATOR ' - ') ops2, 
						GROUP_CONCAT(case when lo.seccion=3 then o.descripcion else null end order by o.descripcion SEPARATOR ' - ') ops3 
						from linea l, operacion o, linea_has_operacion lo, parte p 
						where p.numParte = $term 
						and o.Parte_numParte = p.numParte 
						and l.Parte_numParte = p.numParte 
						and o.Parte_numParte = l.Parte_numParte 
						and o.numOperacion = lo.Operacion_numOperacion 
						and l.idLinea = lo.Linea_idLinea 
						group by l.idlinea 
						order by idLinea;";
                $result = $conn->query($query);
				$response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
				echo json_encode($response);
            }
            else{
                $query = "select * FROM herramienta ; ";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 