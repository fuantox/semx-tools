<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {

        case "PUT":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[5]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                if(is_numeric($params[5])){
                    $id = sprintf('%05d', $params[5]);
                }
                else{
                    $id = $params[5];
                }
                $query = "UPDATE `shokudo`.`empleado`
                            SET
                            `Nombre` = '$params[6]',
                            `Paterno` = '$params[7]',
                            `Materno` = '$params[8]',
                            `Empresa` = '$params[9]',
                            `Area` = '$params[10]',
                            `Nomina` = '$params[11]',
                            `Ingreso` = '$params[12]'
                            WHERE `idEmpleado` = '$id';";
                $query = str_replace("%20"," ",$query);
                echo $query;
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
            echo json_encode(["result" => "put", "uri" => $params[4]]);
        break;

        case "POST":
            $request = json_decode(file_get_contents('php://input'));
            $piezas = $_POST['cants'];
            $fechas = mysql_escape_mimic($_POST['fecha']);
            $idop = $_POST['ops'];
			$idpart = $_POST['parts'];
            $query = "INSERT INTO produccion_diaria
                (cantPiezas, fecha, idOperacion)
                VALUES ('$piezas', '$fechas', '$idop');";        
            if($conn->query($query) != TRUE){
                echo json_encode(["result" => "error", "msg" => $conn->error, "q" => $query, $request->{'id'}]);
                disconnect($conn);
            }
            echo json_encode(["result" => "success"]);
        break;

        case 'GET':
			if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
				$idparte = $_GET['idpart'];
				$query = "select o.numOperacion, o.descripcion FROM Operacion as o, Linea_has_Operacion as lo, Linea as l 
						where o.Parte_numParte = $idparte 
						and o.numOperacion = lo.Operacion_numOperacion 
						and lo.Linea_idLinea = l.idLinea 
						and l.idLinea = $term 
						order by o.descripcion ; ";
				
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
				$result = $conn->query($query);
                $response = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
            else{
                $query = "select herramienta.numHerramienta, herramienta.descripcion from herramienta, operacion_has_herramienta, operacion, parte where parte.numParte = operacion.Parte_numParte and parte.numParte = $term and operacion.numOperacion = $idop and operacion.numOperacion = operacion_has_herramienta.Operacion_numOperacion and herramienta.numHerramienta = operacion_has_herramienta.Herramienta_numHerramienta;";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}

function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 