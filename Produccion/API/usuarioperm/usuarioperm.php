<?php
session_start();
header("Content-Type: application/json;charset=utf-8");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Credentials: true");
require("../connection.php");

if(!isset($_SESSION["username"])){
    echo json_encode(["result" => "error", "msg" => "Not logged in"]);
}
else{
    $conn = connect();
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
		
		
		
		
		
		case "PUT":
            $uri = $_SERVER["REQUEST_URI"];
            $params = explode('/', $uri);
            if($params[4]==null || $params[5]==null){
                echo json_encode(["Error" => 'error']); 
            }
            else{
                $id = $params[4];
				$permisos = explode(",", $params[5]);
				$valores = array(0, 0, 0, 0, 0, 0);
				for ($i=0;$i<count($permisos);$i++){
					
					switch($permisos[$i]){
						case 'tool':
							$valores[0] = 1;
							break;
							
						case 'part':
							$valores[1] = 1;
							break;
							
						case 'change':
							$valores[2] = 1;
							break;
							
						case 'daily':
							$valores[3] = 1;
							break;
							
						case 'chart':
							$valores[4] = 1;
							break;
							
						case 'user':
							$valores[5] = 1;
							break;
							
						
					}
				}
				echo json_encode(["val" => $valores]);
				$query = "Update usuario set 
						ABCHerr=$valores[0], 
						ABCPart=$valores[1], 
						CambioHerr=$valores[2], 
						PrdoDiaria=$valores[3], 
						charts=$valores[4], 
						users=$valores[5] 
						where idusuario='$id';";
                $query = str_replace("%20"," ",$query);
                echo $query;
                if ($conn->query($query)) {
                    echo json_encode(["result" => "ok", "affected_rows" => $conn->affected_rows, "uri" => $uri, "id" => $params[4]]);
                } else {
                    echo json_encode(["result" => "error", "msg" => $conn->error]);
                }
            }
            echo json_encode(["result" => "put", "uri" => $params[4]]);
        break;
		
		
        case 'GET':
            if (isset($_GET['PATH_INFO'])){
                $term = $_GET['PATH_INFO'];
                $pos = strpos($term, " ");
                if ($pos!=false) {
                    $term = str_replace(' ', '|', $term);
                }
                $query = "select idUsuario, nombre, apellido, ABCHerr, ABCPart, CambioHerr, PrdoDiaria, charts, users 
                        FROM usuario
                        WHERE
                        idUsuario rlike '$term'
                        or nombre rlike '$term'
                        or apellido rlike '$term'
						or CONCAT(nombre, ' ', apellido) = '$term';";
                $result = $conn->query($query);
                $response = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
            else{
                $query = "select idUsuario, nombre, apellido, ABCHerr, ABCPart, CambioHerr, PrdoDiaria, charts, users FROM usuario;";
                $result = $conn->query($query);
                $response = [];
                
                while ($row = mysqli_fetch_assoc($result)) {
                    $response[] = $row;
                }
                echo json_encode($response);
            }
        break;
    }
}