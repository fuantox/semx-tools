<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['part'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">';  
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<div class="container main-content">
			<div class="row">
				<h1> Parts</h1>
			</div>
			
			<div class="col-md-10"><input type="hidden" class="form-control"></div>
			<div class="col-md-2"><button type="button" onclick="mostrar()" id="balta" class="btn btn-primary btn-block">
				<i class="glyphicon glyphicon-plus"></i>&nbsp;Add Part</button>
			</div>

	<!-- DAR DE ALTA NUEVAS PARTES -->
		<form action="Acciones.php" method="POST">	
			<div class="row" id="alta" style="display:none;">
				<input type="text" id="opcion" hidden="true" name="opcion" value="alta">
				<table class="table">
					<tbody><tr class="thead-inverse" style="text-align:center; border-style:ridge; border-width: 1px; border-color:#fff;"><td>
						<div class="row" style="padding-left: 20%;">
						  <div class="col-md-6" style="padding-left: 3%;"><input type="text" id="PartName" name="PartName" class="form-control" placeholder="Part Name"></div>
						  <div class="col-md-2" style="padding-left: 10%;"><button type="button" onclick="add()" class="btn btn-success btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Add</button></div>
						  <div class="col-md-2" style="padding-left: 0%;"><button type="button" onclick="ocultar()" id="bocultar" style="display:none;" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Close</button></div>
						</div>
					</td></tr></tbody>
				</table>
			</div> 
		</form>	

		<!--TABLA-->
			<br><br>
			<div id="partes">
				
			</div>
		</div>
        
		
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<script type="text/javascript">
		//MUESTRA LOS ELEMENTOS EN DONDE PODEMOS AGREGAR UNA NUEVA PARTE
		function mostrar(){
			document.getElementById('alta').style.display = 'block';
			document.getElementById('balta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'block';
		}
		//OCULTA LOS ELEMENTOS PARA YA NO DAR DE ALTA MAS PARTES
		function ocultar(){
			document.getElementById('alta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'none';
			document.getElementById('balta').style.display = 'block';
		}
	</script>
	
	<script type="text/javascript" src="js/part.js"></script>
</html>