<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['part'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
	else if (!isset($_GET['num']) || !isset($_GET['name'])) { //validar que exista algún valor en 'part' y 'name'
		print '<script language="JavaScript">'; 
		//print "alert('Select a Part to show Operations.');"; 
		print "window.location='Parte.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">'; 
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

require("API/connection.php");
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
			$conn = connect();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<?php 
		$numpart = $_GET['num']; //Obtenemos el numero de parte
		$namepart = $_GET['name']; //Obtenemos el nombre de parte
		?>
		
		<div class="container main-content">
			<div class="row">
				<h1> <?php echo $namepart." - Lines"; ?> </h1>
			</div>
			
			<!--FORM-->
			<div class="col-md-10"><input type="hidden" class="form-control"></div>
			<div class="col-md-2"><button type="button" onclick="mostrar()" id="balta" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i>&nbsp; Add Line</button></div><br><br>
			<div class="col-md-10"><input type="hidden" class="form-control"></div>
			<div class="col-md-2"><a id="boperaciones" class="btn btn-primary btn-block" href="Operacion.php?num=<?=$numpart?>&name=<?=$namepart?>"> Operations</a></div>		
			
			<div class="row" id="alta" style="display:none;">
				<table class="table">
					<tbody><tr class="thead-inverse" style="text-align:center; border-style:ridge; border-width: 1px; border-color:#fff;"><td>
						<div class="row">
							<div class="col-md-2"><input type="text" id="descs" class="form-control" placeholder="Line"></div>
							
							
						  
							<div class="col-md-2">
								<input type="checkbox" id="section1" value="s1" onchange="shows(1);" />&nbsp;Section 1
								<div id="cnt1" style="display:none;">
									<table>
										<tr>
										<td>From:&nbsp;</td>
										<td>
										<select id="from1"><?php
											$query = "select * from operacion where parte_numparte=".$numpart." order by descripcion;";
											$resultado = $conn->query($query);
											for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) {
												echo "<option id='desc".$fila[0]."'>".$fila[1]."</option>";
											}
											?>
										</select>
										</td>
										</tr>
										
										<tr>
										<td>To:&nbsp;</td>
										<td>
										<select id="to1"><?php
											$query = "select * from operacion where parte_numparte=".$numpart." order by descripcion;";
											$resultado = $conn->query($query);
											for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) {
												echo "<option id='desc".$fila[0]."'>".$fila[1]."</option>";
											}
											?>
										</td>
										</tr>
									</table>
								</div>
							</div>
						  
							<div class="col-md-2">
								<input type="checkbox" id="section2" value="s2" onchange="shows(2);">&nbsp;Section 2
								<div id="cnt2" style="display:none;">
									<table>
										<tr>
										<td>From:&nbsp;</td>
										<td>
										<select id="from2"><?php
											$query = "select * from operacion where parte_numparte=".$numpart." order by descripcion;";
											$resultado = $conn->query($query);
											for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) {
												echo "<option id='desc".$fila[0]."'>".$fila[1]."</option>";
											}?>
										</select>
										</td>
										</tr>
										
										<tr>
										<td>To:&nbsp;</td>
										<td>
										<select id="to2"><?php
											$query = "select * from operacion where parte_numparte=".$numpart." order by descripcion;";
											$resultado = $conn->query($query);
											for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) {
												echo "<option id='desc".$fila[0]."'>".$fila[1]."</option>";
											}?>
										</td>
										</tr>
									</table>
								</div>
							</div>
						  
							<div class="col-md-2">
								<input type="checkbox" id="section3" value="s3" onchange="shows(3);">&nbsp;Section 3
								<div id="cnt3" style="display:none;">
									<table>
										<tr>
										<td>From:&nbsp;</td>
										<td>
										<select id="from3"><?php
											$query = "select * from operacion where parte_numparte=".$numpart." order by descripcion;";
											$resultado = $conn->query($query);
											for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) {
												echo "<option id='desc".$fila[0]."'>".$fila[1]."</option>";
											}?>
										</select>
										</td>
										</tr>
										
										<tr>
										<td>To:&nbsp;</td>
										<td>
										<select id="to3"><?php
											$query = "select * from operacion where parte_numparte=".$numpart." order by descripcion;";
											$resultado = $conn->query($query);
											for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) {
												echo "<option id='desc".$fila[0]."'>".$fila[1]."</option>";
											}?>
										</td>
										</tr>
									</table>
								</div>
							</div>
						  <div>
						  
						  <div class="col-md-2" style="padding-left: 10%;"><button type="button" onclick="add(<?php echo $numpart; ?>)" class="btn btn-success btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Add</button></div>
						  <div class="col-md-2" style="padding-left: 0%;"><button type="button" onclick="ocultar()" id="bocultar" style="display:none;" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Close</button></div>
						  </div>
						</div>
					</td></tr>
					</tbody>
				</table>
			</div>
			
			
			<!--TABLA-->
			<br><br><input type="hidden" id="parteid" value="<?=$numpart;?>" />
			<div id="lineas">
			
			</div>
		</div>
		
		
        
        <?php
			disconnect($conn);
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<script type="text/javascript">
		function mostrar(){ //Mostrar el contenido para dar de alta
			document.getElementById('alta').style.display = 'block';
			document.getElementById('balta').style.display = 'none';
			document.getElementById('boperaciones').style.display = 'none';
			document.getElementById('bocultar').style.display = 'block';
		}
		function ocultar(){ //Ocultar el contenido para dar de alta
			document.getElementById('alta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'none';
			document.getElementById('balta').style.display = 'block';
			document.getElementById('boperaciones').style.display = 'block';
		}
		function shows(no){
			if(document.getElementById('section'+no).checked){
				document.getElementById('cnt'+no).style.display = 'block';
			}
			else{
				document.getElementById('cnt'+no).style.display = 'none';
			}
		}
	</script>
	
	<script type="text/javascript" src="js/line.js"></script>
	
</html>