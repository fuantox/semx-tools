<?php
    require 'template.php';
    /*session_start();
    $carrito = 0;
    if(isset($_SESSION["carrito"])){
        $carrito = $_SESSION["carrito"];
    }
    $logged = false;
    if(isset($_SESSION["id_usuario"])){
        $id_usuario = $_SESSION["id_usuario"];
        $logged = true;
    }*/
	session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if (!isset($_SESSION['tool']) && !isset($_SESSION['part']) && !isset($_SESSION['change']) && !isset($_SESSION['daily']) && !isset($_SESSION['chart']) && !isset($_SESSION['user'])) { //validar que exista algún valor en los permisos
		print '<script language="JavaScript">'; 
		//print "alert('Please Login.');"; 
		print "window.location='logout.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">'; 
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
        ?>
        
        <div class="container main-content">
			<div class="row">
				<h1> Menu </h1>
			</div>

			<div class="row" id="alta" style="display:block;">
				<input type="text" id="opcion" hidden="true" name="opcion" value="login">
				<table class="table">
					<tbody><tr class="thead-inverse" style="text-align:center; border-style:ridge; border-width: 1px; border-color:#fff;"><td>
						<div class="col-md-2"></div>
						<div class="col-md-7"><div class="list-group">
							<?php /* Aquí mostraremos solo la opciones según los permisos que cada usuario tenga */
							
							if($_SESSION['tool'] == 1){
								?><a href="HerramientaABC.php" class="list-group-item">TOOLS</a><?php
							}
							if($_SESSION['part'] == 1){
								?><a href="Parte.php" class="list-group-item">PARTS</a><?php
							}
							if($_SESSION['change'] == 1){
								?><a href="CambioHerramienta.php" class="list-group-item">TOOL CHANGE</a><?php
							}
							if($_SESSION['daily'] == 1){
								?><a href="ProduccionDiaria.php" class="list-group-item">DAILY PRODUCTION</a><?php
							}
							if($_SESSION['chart'] == 1){
								?><a href="MenuChart.php" class="list-group-item">RESULTS</a><?php
							}
							if($_SESSION['user'] == 1){
								?><a href="Usuario.php" class="list-group-item">USERS</a><?php
							}
							if($_SESSION['tool'] == 0 && $_SESSION['part'] == 0 && $_SESSION['change'] == 0 && $_SESSION['daily'] == 0 && $_SESSION['chart'] == 0 && $_SESSION['user'] == 0){
								?>
								<h1 class="text-center">You do not have any permission.</h1>
								
								<?php
							}?>
							
						</div></div>
					</td></tr></tbody>
				</table>
			</div> 
		</div>
        
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>

</html>