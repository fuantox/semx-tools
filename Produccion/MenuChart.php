<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['chart'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">';  
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

require("API/connection.php");
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>
	<link type="text/css" rel="stylesheet" href="css/bootstrap-datepicker.css"  media="screen,projection"/>

    <body>
        <?php 
            navbar();
			$conn = connect();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<div class="container main-content">
			<div class="row">
				<h1> Charts Menu </h1>
			</div>

			<div class="row" id="alta" style="display:block;">
				<input type="text" id="opcion" hidden="true" name="opcion" value="login">
				<table class="table">
					<tbody><tr class="thead-inverse" style="text-align:center; border-style:ridge; border-width: 1px; border-color:#fff;"><td>
						<div class="col-md-2"></div>
						<div class="col-md-7"><div class="list-group">
							
							<!-- Primera opción del menú. Al seleccionarlos, mostrará elementos para seleccionar fechas de inicio y final cada uno, además de poder seleccionar las parte -->
							<button type="button" class="list-group-item" onclick="showDate()"><center>TOOL COST PER PIECE CHART</center></button>
							<form id="resultadoschart" action="Resultados.php" method="POST">
							<table class="table" id="tabledate" style="display:none;">
								<tbody><tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;"></tr>
								
								<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">				
									<td class="col-md-6"><div class="row">
										<!-- Mostrar todas las partes existentes -->
										<div class="col-md-2"><label>Parts: </label></div>
										<select id="selPart1" name="selPart1" class="selectpicker" > <!-- Combobox donde se muestran todas las partes.-->
										  <option>Select an option...</option>
										  <?php 
											$query = "select * from parte;";
											$resultado = $conn->query($query);
											for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) { //Ciclo para mostrar todos los datos de la base de datos de PARTES
												echo "<option value='".$fila[0]."'> ".$fila[1]."</option>";
											}
										?>
										</select>
									</div></td>
								</tr>
								
								<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
									<td class="col-md-3"><div class="row">
										<div class="col-md-5">
											<input type="text" class="form-control" id="startd1" name="startd1" placeholder="Start Date" onkeypress="return false;"/>
										</div>
										<div class="col-md-5">
											<input type="text" id="finishd1" name="finishd1" class="form-control" placeholder="Finish Date" />
										</div>
									</div></td>
								</tr>
								
								<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
									<td class="col-md-3"><div class="row">
										<div class="col-md-1"><button type="button" id="buttonDate" onclick="sendDate();" form="" class="btn btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Send </button></div>
									</div></td>
								</tr>
							</table>
							</form>
							
							
							<!-- Segunda opción del menú. Al seleccionarlos, mostrará elementos para seleccionar fechas de inicio y final cada uno, además de poder seleccionar las parte -->
							<button type="button" class="list-group-item" onclick="showParts()"><center>TOOL COST CHART - COST PARETO CHART</center></button>
							<form id="costchart" action="CostChart.php" method="POST">
							<table class="table" id="tablepart" style="display:none;">
								<tbody><tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;"></tr>
								
								<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">				
									<td class="col-md-6"><div class="row">
										<!-- Mostrar todas las partes existentes -->
										<div class="col-md-2"><label>Parts: </label></div>
										<select id="selPart" name="selPart" class="selectpicker" > <!-- Combobox donde se muestran todas las partes.-->
										  <option>Select an option...</option>
										  <?php 
											$query = "select * from parte;";
											$resultado = $conn->query($query);
											for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) { //Ciclo para mostrar todos los datos de la base de datos de PARTES
												echo "<option value='".$fila[0]."'> ".$fila[1]."</option>";
											}
										?>
										</select>
									</div></td>
								</tr>
								
								<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
									<td class="col-md-3"><div class="row">
										<div class="col-md-5">
											<input type="text" class="form-control" id="startd" name="startd" placeholder="Start Date" onkeypress="return false;"/>
										</div>
										<div class="col-md-5">
											<input type="text" id="finishd" name="finishd" class="form-control" placeholder="Finish Date" />
										</div>
									</div></td>
								</tr>
								
								<tr class="thead-inverse" style="border-style:ridge; border-width: 1px; border-color:#FFF;">
									<td class="col-md-3"><div class="row">
										<div class="col-md-1"><button type="button" id="buttonSend" onclick="send()" form="" class="btn btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Send </button></div>
									</div></td>
								</tr>
							</table>
							</form>
							
						</div></div>
					</td></tr></tbody>
				</table>
			</div> 
		</div>
		
        <?php
			disconnect($conn);
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
	<script>
		$(document).ready(function(){
  
			$("#startd1").datepicker({
				todayBtn:  1,
				autoclose: true,
				format: 'yyyy-mm-dd',
			}).on('changeDate', function (selected) {
				var minDate = new Date(selected.date.valueOf());
				$('#finishd1').datepicker('setStartDate', minDate);
			});
			
			$("#finishd1").datepicker({
				autoclose: true,
				format: 'yyyy-mm-dd',
			}).on('changeDate', function (selected) {
					var minDate = new Date(selected.date.valueOf());
					$('#startd1').datepicker('setEndDate', minDate);
				});

		});
	</script>
	
	<script>
		$(document).ready(function(){
  
			$("#startd").datepicker({
				todayBtn:  1,
				autoclose: true,
				format: 'yyyy-mm-dd',
			}).on('changeDate', function (selected) {
				var minDate = new Date(selected.date.valueOf());
				$('#finishd').datepicker('setStartDate', minDate);
			});
			
			$("#finishd").datepicker({
				autoclose: true,
				format: 'yyyy-mm-dd',
			}).on('changeDate', function (selected) {
					var minDate = new Date(selected.date.valueOf());
					$('#startd').datepicker('setEndDate', minDate);
				});

		});
	</script>
	
	<script type="text/javascript">
        //FUNCIONES PARA TOOL COST PER PIECE CHART
		function showDate() {
			if(document.getElementById('tabledate').style.display == 'none'){
				document.getElementById('tabledate').style.display = 'block'; 
			}
			else if(document.getElementById('tabledate').style.display == 'block'){
				document.getElementById('tabledate').style.display = 'none'; 
			}
			//document.getElementById("buttonSend").disabled = false;
		}
		
		function sendDate(){
			var a = document.getElementById("selPart1");
			var part = a.options[a.selectedIndex].value; //PARTE
			var startd = document.getElementById("startd1").value;
			var finishd = document.getElementById("finishd1").value;
			
			if(part!="Select an option..."){
				if(startd!="" && finishd!=""){
					if(startd < finishd){
						document.getElementById("resultadoschart").submit();
					}
					else{
						swal("Error", "Start date must be less than Finish date", "error"); 
					}
				}
				else{
					swal("Error", "There are blank fields", "error"); 
				}
			}
			else{
				swal("Error", "Select a part", "error"); 
			}
		}
	
		//FUNCIONES PARA TOOL COST CHART - COST PARETO CHART
		function showParts() {
			if(document.getElementById('tablepart').style.display == 'none'){
				document.getElementById('tablepart').style.display = 'block'; //Mostrando el div de las HERRAMIENTAS
			}
			else if(document.getElementById('tablepart').style.display == 'block'){
				document.getElementById('tablepart').style.display = 'none'; //Mostrando el div de las HERRAMIENTAS
			}
		}
		
		function send(){
			var a = document.getElementById("selPart");
			var part = a.options[a.selectedIndex].value; //PARTE
			var startd = document.getElementById("startd").value;
			var finishd = document.getElementById("finishd").value;
			
			if(part!="Select an option..."){
				if(startd!="" && finishd!=""){
					if(startd < finishd){
						document.getElementById("costchart").submit();
						//window.location="CostChart.php?part=" + part + "&startd=" + startd + "&finishd=" + finishd;
					}
					else{
						swal("Error", "Start date must be less than Finish date", "error");
					}
				}
				else{
					swal("Error", "There are blank fields", "error"); 
				}
			}
			else{
				swal("Error", "Select a part", "error"); 
			}
		}
	</script>
</html>