<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['chart'] == 0){ //Verificar que otros usuarios no accedan a esta p疊ina
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
	else if (!isset($_POST['selPart1']) || !isset($_POST['startd1']) || !isset($_POST['finishd1'])) { //validar que exista alg佖 valor en 'part'
		print '<script language="JavaScript">'; 
		//print "alert('Select a Part to show.');"; 
		print "window.location='MenuChart.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">'; 
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

require("API/connection.php");
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
			$conn = connect();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<?php 
		$startd = $_POST["startd1"]; //Fecha de inicio
		$finishd = $_POST["finishd1"]; //Fecha de finishd
		$parte = $_POST["selPart1"]; //parte
		$nombre = "";
		?>
        
		<div id="principal" class="container main-content">
			<div class="row">
				<h1> TOOL COST PER PIECE </h1>
			</div>
			
			<div id="datos">
				<?php 
				$consulta = "select c.cant, h.precio, 
				pd.cantPiezas, h.tiempovida, 
				pd.fecha, h.descripcion, pd.idOperacion, o.descripcion 
				from operacion o, herramienta h, parte p, operacion_has_herramienta oh, 
				cambio c, linea l, linea_has_operacion lho, produccion_diaria pd
				where p.numParte = ".$parte."
				and p.numParte = o.Parte_numParte
				and o.numOperacion = c.idOp
				and o.numOperacion = pd.idOperacion
				and o.numOperacion = oh.Operacion_numOperacion
				and h.numHerramienta = oh.Herramienta_numHerramienta
				and h.numHerramienta = c.idHerr
				and l.Parte_numParte = p.numParte
				and l.idLinea = lho.Linea_idLinea
				and lho.Operacion_numOperacion = o.numOperacion
				and l.idLinea = c.idLinea
				and l.idLinea = pd.idLinea
				and pd.fecha = c.fecha
				and pd.fecha>='".$startd."'
				and pd.fecha<='".$finishd."'  
				order by pd.fecha, o.descripcion, l.idLinea;";
				
				$resultado = $conn->query($consulta);
				$results = mysqli_num_rows($resultado);
				
				for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) { //Ciclo para mostrar todos los datos de la base de datos de la consulta
					echo "<input type='hidden' id='cant$i' value='$fila[0]'>";
					echo "<input type='hidden' id='precio$i' value='$fila[1]'>";
					echo "<input type='hidden' id='piezas$i' value='$fila[2]'>";
					echo "<input type='hidden' id='vida$i' value='$fila[3]'>";
					echo "<input type='hidden' id='fecha$i' value='$fila[4]'>";
					echo "<input type='hidden' id='desc$i' value='$fila[5]'>";
					echo "<input type='hidden' id='idop$i' value='$fila[6]'>";
					echo "<input type='hidden' id='descop$i' value='$fila[7]'><br>";
					//echo "TOTAL: <input type='hidden' value='".$fila[0]*$fila[1]/$fila[2]."'>";
				}
				echo "<input type='hidden' id='results' value='$results'><br>";
				
				$consulta = "select operacion.descripcion 
				from produccion_diaria, cambio, operacion, parte 
				where produccion_diaria.idOperacion = cambio.idOp 
				and operacion.numOperacion = cambio.idOp 
				and parte.numParte = operacion.Parte_numParte 
				and parte.numParte = ".$parte." 
				group by(produccion_diaria.idOperacion);";
				
				$resultado = $conn->query($consulta);
				$results = mysqli_num_rows($resultado);
				
				for($i=0; $fila = mysqli_fetch_row($resultado); $i++){
					echo "<input type='hidden' id='operaciones$i' value='$fila[0]'>"; //Guardamos cada nombre de cada operacion
				}
				echo "<input type='hidden' id='nops' value='$results'><br>"; //Cantidad de operaciones encontradas
				
				$consulta = "select nombre from parte where numParte=".$parte.";";
				$resultado = $conn->query($consulta);
				$fila = mysqli_fetch_row($resultado);
				?>
			</div>
			
			<?php $nombre = $fila[0]; ?>
			<h3 id="namepart"> <?=$fila[0]." ".$startd." - ".$finishd;?> </h3>
			<h4>Total cost per piece: $<input type="text" id="costototal" value="0" style="border:none; border-color: transparent; outline:none;" readonly></h4>
			<center>
				<h5 style="color:black; display:inline;">Ideal Cost</h5>
				<h4 style="color:black; display:inline;">&#9644; &nbsp; &nbsp;</h4>
				<h5 style="color:black; display:inline;">Real Cost </h5>
				<p style="color:orange; display:inline; font-size:200%;">&#9632;</p>
				
				<table>
					<tr><td valign="middle" align="center">
						<!-- <h2 style=" -moz-transform: rotate(-90deg);">USD</h2> -->
						<h2 style="width:1px; word-wrap:break-word; white-space: pre-wrap;">USD</h2>
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td> <!--style="display:none;"-->
						<canvas id="myChart" height="400" width="1000"></canvas> <!-- Aquí es donde se mostrará la gráfica -->
					</td></tr>
				</table>
				<center><b><h2>Date</h2></b></center>
			</center>
		</div>
		
		<div class="col-md-2">
			<button class="btn btn-info btn-block" id="create" onclick="capture()">
				<i class="fa fa-arrow-down" aria-hidden="true"></i>
				&nbsp;Download Report
			</button>
		</div>
		<form method="POST" enctype="multipart/form-data" action="save.php" id="myForm">
			<input type="hidden" name="img_val" id="img_val" value="" />
			<input type="hidden" name="chartno" id="chartno" value="" />
			<?php echo "<input type='hidden' name='pagina' id='pagina' value='Resultados.php?part=$parte&startd=$startd&finishd=$finishd' />"; ?>
			<?php echo "<input type='hidden' name='nombre' id='nombre' value='Tool_Cost_Per_Piece_$nombre"."_$startd"."_$finishd' />"; ?>
		</form>
		
		
		<?php disconnect($conn); ?>
		
		
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<script src="jquery.min.js"></script> <!--Para las gráficas -->
	<script src="Chart.min.js"></script>
	
	<script type="text/javascript" src="js/report.js"></script>
	<script src="chart1.js"></script>
	
	<script type="text/javascript" src="jquery.min.17.js"></script> <!-- Para las capturas del div -->
	<script type="text/javascript" src="html2canvas.js"></script>
	<script type="text/javascript" src="jquery.plugin.html2canvas.js"></script>
	<script type="text/javascript">
		function capture() {
			document.getElementById("chartno").value= 'r';
			$('#principal').html2canvas({
				onrendered: function (canvas) {
					//Set hidden field's value to image data (base-64 string)
					$('#img_val').val(canvas.toDataURL("image/png"));
					//Submit the form manually
					document.getElementById("myForm").submit();
				}
			});
		}
	</script>

</html>