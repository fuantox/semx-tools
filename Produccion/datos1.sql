-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tools
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cambio`
--

DROP TABLE IF EXISTS `cambio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cambio` (
  `idCambio` int(11) NOT NULL AUTO_INCREMENT,
  `idOp` int(11) DEFAULT NULL,
  `idHerr` int(11) NOT NULL,
  `cant` int(11) NOT NULL,
  `razon` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `idLinea` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCambio`),
  UNIQUE KEY `idCambio_UNIQUE` (`idCambio`)
) ENGINE=InnoDB AUTO_INCREMENT=318 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cambio`
--

LOCK TABLES `cambio` WRITE;
/*!40000 ALTER TABLE `cambio` DISABLE KEYS */;
INSERT INTO `cambio` VALUES (251,1,4,1,1,'2017-07-12',1),(252,2,6,1,1,'2017-07-12',1),(253,6,85,1,1,'2017-07-12',1),(254,3,17,1,1,'2017-07-12',1),(257,17,80,1,3,'2017-07-12',1),(258,17,81,2,3,'2017-07-12',1),(259,17,81,2,1,'2017-07-12',1),(260,3,80,1,1,'2017-07-12',2),(261,3,81,2,3,'2017-07-12',2),(262,17,81,2,1,'2017-07-12',2),(263,4,14,1,1,'2017-07-12',2),(264,4,18,1,1,'2017-07-12',2),(265,4,82,1,1,'2017-07-12',2),(266,4,83,1,1,'2017-07-12',2),(271,17,81,4,1,'2017-07-12',4),(272,2,6,1,1,'2017-07-12',3),(273,6,85,1,1,'2017-07-12',3),(274,3,81,4,1,'2017-07-12',3),(276,17,81,4,1,'2017-07-12',3),(277,1,4,1,1,'2017-07-12',4),(279,17,79,1,1,'2017-07-12',4),(280,17,81,4,1,'2017-07-12',4),(281,3,81,2,1,'2017-07-12',4),(282,1,3,2,1,'2017-07-18',1),(283,1,4,1,1,'2017-07-18',1),(284,1,3,2,1,'2017-07-18',2),(285,1,4,1,1,'2017-07-18',2),(286,1,3,2,1,'2017-07-18',3),(287,1,4,1,1,'2017-07-18',3),(288,2,6,1,1,'2017-07-18',2),(289,2,6,1,1,'2017-07-18',3),(290,6,85,1,1,'2017-07-18',1),(291,6,85,1,1,'2017-07-18',2),(292,3,79,1,1,'2017-07-18',1),(293,3,80,1,1,'2017-07-18',1),(294,3,81,2,1,'2017-07-18',1),(295,17,81,3,1,'2017-07-18',1),(296,3,81,1,1,'2017-07-18',1),(297,3,80,1,1,'2017-07-18',2),(298,3,81,3,1,'2017-07-18',2),(299,17,81,3,1,'2017-07-18',2),(300,3,80,1,1,'2017-07-18',3),(301,3,81,3,1,'2017-07-18',3),(302,17,81,3,1,'2017-07-18',3),(303,4,13,1,1,'2017-07-18',1),(304,4,13,1,1,'2017-07-18',2),(305,4,18,1,1,'2017-07-18',1),(306,4,82,1,1,'2017-07-18',1),(307,4,18,1,1,'2017-07-18',3),(308,4,18,1,1,'2017-07-18',2),(309,4,82,1,1,'2017-07-18',2),(310,4,83,1,1,'2017-07-18',2),(311,7,27,2,1,'2017-07-18',1),(312,7,73,1,1,'2017-07-18',1),(313,7,27,1,1,'2017-07-18',2),(314,7,74,2,1,'2017-07-18',2),(315,2,6,2,1,'2017-07-18',1),(316,2,6,1,1,'2017-07-18',2),(317,1,3,2,1,'2017-07-18',1);
/*!40000 ALTER TABLE `cambio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herramienta`
--

DROP TABLE IF EXISTS `herramienta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herramienta` (
  `numHerramienta` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(80) NOT NULL,
  `tiempoVida` int(11) DEFAULT NULL,
  `precio` float NOT NULL,
  `binloc` varchar(15) DEFAULT NULL,
  `semxcode` varchar(20) DEFAULT NULL,
  `shmmcode` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`numHerramienta`),
  UNIQUE KEY `numHerramienta_UNIQUE` (`numHerramienta`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herramienta`
--

LOCK TABLES `herramienta` WRITE;
/*!40000 ALTER TABLE `herramienta` DISABLE KEYS */;
INSERT INTO `herramienta` VALUES (3,'CBN INSERT',400,31.47,'A-3-1','AV-CA-001','2NUDCGW070204-BN7500'),(4,'INSERT',200,4.43,'A-3-2','AV-CA-002','TPGT110304LFY-T2000Z'),(5,'INSERT',300,4.24,'A-3-3','AV-CA-003','TNPR331M-T1000A'),(6,'CBN INSERT',600,36.59,'A-3-4','AV-CA-004','3NUTNGA331-BN7500'),(7,'DRILL',4000,90.72,'A-3-5','AV-CA-005','G18-213 D12X60DEGXL100'),(8,'CHAMFERING TOOL',1500,110.82,'A-3-6','AV-CA-006','D8X45DEGX64LXD8'),(9,'ENDMILL',220,72.39,'A-3-7','AV-CA-007','G18-251 D9.9X90LXD10-8N'),(10,'DEBURRING TOOL',24000,237.76,'A-3-8','AV-CA-008','PAL12000'),(11,'REAMER',55,119.09,'A-3-9','AV-CA-009','ZO-S512139C'),(12,'DRILL',700,30.15,'A-3-10','AV-CA-010','MDW0300GS2'),(13,'DRILL',700,17.04,'A-3-11','AV-CA-011','G18-095 D3X90DEGX55'),(14,'CHAMFERING TOOL',2100,35.04,'A-3-12','AV-CA-012','G18-203 D2.5X20'),(15,'ENDMILL',2400,44.23,'A-3-13','AV-CA-013','GSX41000S-2D'),(16,'DRILL',390,57,'A-3-14','AV-CA-014','MDW0900GS2'),(17,'BRUSH',700,31.58,'A-4-1','AV-CA-015','D55X15X73.5'),(18,'BRUSH',1400,60.15,'A-4-2','AV-CA-016','D10X10X50XD3 SG-A-1000'),(19,'BRUSH',2400,17.91,'A-4-3','AV-CA-017','D23X90LXD6 TYPE FLAT'),(20,'QUIL',0,298.67,'A-5-5','AV-CA-018','H4548810'),(21,'DRESSER',0,344,'A-5-6','AV-CA-019','ドレッサー(DRESSER) -> under investigation'),(22,' HOLDER',0,54.46,'A-5-1','AV-CA-020','S10M-SDQCL07 (S10MSDQCL070213)'),(23,' HOLDER',0,54.46,'A-5-2','AV-CA-021','S10M-SDQCR07 (S10MSDQCR070213)'),(24,' HOLDER',0,64.67,'A-5-3','AV-CA-022','BBPT220R (S20SSTUPR110322)'),(25,' HOLDER',0,31.28,'A-5-4','AV-CA-023','ETENN2525M33W (ETENN2525M1604W)'),(26,'GRINDING WHEEL',22000,137.82,'A-5-7','AV-CA-024','SD21700130MBA'),(27,'GRINDING WHEEL',650,8.56,'A-4-4','AV-CA-025','D25X8X30 SA80J6VB'),(28,'MANDREL',0,1722.81,'A-5-8','AV-CA-026','CML-293L1K-4182A'),(29,'WEDGE',0,620.21,'A-5-9','AV-CA-027','CWL-293L1K-3550A'),(30,'ADAPTOR',0,482.39,'A-5-10','AV-CA-028','CTL-192000-0012A'),(31,'WRENCH',0,142.63,'A-5-11','AV-CA-029','2852V1.0-5.0'),(32,'BLADE',0,5.6,'A-5-12','AV-CA-030','2859T8'),(33,'BLADE',0,5.6,'A-5-13','AV-CA-031','2859T10'),(34,'TOOL HOLDER',0,377.29,'A-2-1','AV-CA-032','BBT40-HDC12-90'),(35,'TOOL HOLDER',0,377.29,'A-2-2','AV-CA-033','BBT40-HDC8-90'),(36,'TOOL HOLDER',0,377.29,'A-2-3','AV-CA-034','BBT40-HDC10-90'),(37,'TOOL HOLDER',0,290.48,'A-2-4','AV-CA-035','BBT40-MEGA6N-60'),(38,'COLLET',0,65.01,'A-5-14','AV-CA-036','NBC6-2.40AA'),(39,'SCREW',0,7.55,'A-5-16','AV-CA-037','NBA6B'),(40,'WRENCH',0,42.81,'A-5-17','AV-CA-038','MGR20'),(41,'SCREW',0,1.15,'A-5-15','AV-CA-039','BT0306'),(42,'WRENCH',0,1.75,'A-5-18','AV-CA-087','LH015'),(43,'BLADE',0,7.52,'A-5-20','AV-CA-088','2859H1.5'),(44,'TOOL HOLDER',0,295.4,'A-2-5','AV-CA-041','BBT40-MEGA8N-60'),(45,'COLLET',0,50.79,'A-5-19','AV-CA-042','NBC-8AA'),(46,'SCREW',0,8.51,'A-6-1','AV-CA-043','NBA8B'),(47,'WRENCH',0,52.74,'A-6-2','AV-CA-044','MGR25'),(48,'DIGITAL TORQUE WRENCH',0,74.45,'','AV-CA-045','MGR25A-N'),(49,'WRENCH ADAPTOR',0,1697.68,'','AV-CA-046','MGR-TL/P'),(50,'WRENCH ADAPTOR',0,66.48,'A-6-3','AV-CA-047','MGR20A-N'),(51,'PULL  BOLT',0,22.61,'A-6-4','AV-CA-048','40PMG'),(52,'TOOL HOLDER',0,256.58,'A-2-6','AV-CA-049','BBT30-MEGA 6N-75'),(53,'COLLET',0,49.99,'A-6-5','AV-CA-050','NBC6-3.0AA'),(54,'TOOL HOLDER',0,228,'A-2-7','AV-CA-051','BT30-NBS6-45'),(55,'WRENCH',0,8.26,'A-6-6-','AV-CA-052','NBK6'),(56,'WRENCH',0,248.44,'A-6-7','AV-CA-053','NBK6TL'),(57,'TOOL HOLDER',0,274.53,'A-2-8','AV-CA-054','BBT30-MEGA10E-50'),(58,'COLLET',0,67.81,'A-6-8','AV-CA-055','MEC10-10AA'),(59,'SCREW',0,8.51,'A-6-10','AV-CA-056','NBA10B'),(60,'WRENCH',0,67.27,'A-6-11','AV-CA-057','MGR35'),(61,'WRENCH ADAPTOR',0,91.73,'A-6-12','AV-CA-058','MGR35A-N'),(62,'TOOL HOLDER',0,238.63,'A-2-9','AV-CA-059','BT30-NBS10-45'),(63,'COLLET',0,50.79,'A-6-13','AV-CA-060','NBC10-9.0　AA'),(64,'WRENCH',0,8.72,'A-6-14','AV-CA-061','NBK10'),(65,'WRENCH',0,268.32,'A-6-15','AV-CA-062','NBK10TL'),(66,'SCREW',0,2.61,'A-6-16','AV-CA-063','BFTX02506N'),(67,'SPANNER',0,4.72,'A-6-17','AV-CA-064','TRX08'),(68,'SCREW',0,1.94,'A-6-18','AV-CA-065','BFTX0307A'),(69,'SPANNER',0,4.72,'A-6-19','AV-CA-066','TRX10'),(70,'PIN',0,6.03,'A-6-20','AV-CA-067','P334WS'),(71,'SHIM',0,6.67,'A-7-1','AV-CA-068','EST32'),(72,'SPANNER',0,2.48,'A-7-2','AV-CA-069','KY40'),(73,'CBN INSERT',1300,8.57,'A-4-5','AV-CA-076','R-2NUDCGW070204-BN7500 (2 FILOS)'),(74,'CBN INSERT',1300,5.64,'A-4-5','AV-CA-095','R-2NUDCGW070204-BN7500 (1 FILO)'),(75,'CBN INSERT',600,10.57,'A-4-6','AV-CA-077','R-3NUTNGA331-BN7500 (3 FILOS)'),(76,'CBN INSERT',600,6.5,'A-4-6','AV-CA-096','R-3NUTNGA331-BN7500 (1 FILO)'),(77,'CBN INSERT',600,9.21,'A-4-6','AV-CA-097','R-3NUTNGA331-BN7500 (2 FILOS)'),(78,'DRILL REGRIND',4000,27.96,'A-4-7','AV-CA-078','R-G18-213 D12X60DEGXL100'),(79,'CHAMFERING TOOL',1500,35.28,'A-4-8','AV-CA-079','R-D8X45DEGX64LXD8'),(80,'ENDMILL REGRIND',220,44.14,'A-4-9','AV-CA-080','R-G18-251 D9.9X90LXD10-8N'),(81,'REAMER REGRIND',60,18.5,'A-4-10','AV-CA-081','R-ZO-S512139C'),(82,'DRILL REGRIND',700,25.94,'A-4-11','AV-CA-082','R-MDW0300GS2'),(83,'DRILL REGRIND',700,9.74,'A-4-12','AV-CA-083','R-G18-095 D3X90DEGX55'),(84,'ENDMILL',2400,27.98,'A-4-13','AV-CA-085','R-GSX41000S-2D'),(85,'DRILL REGRIND',390,39.44,'A-4-14','AV-CA-086','R-MDW0900GS2'),(86,'CLEANER',0,10.27,'A-7-3','AV-CA-070','AWC10'),(87,'CLEANER',0,10.27,'A-7-4','AV-CA-071','AWC06'),(88,'CLEANER',0,10.27,'A-7-5','AV-CA-072','AWC08'),(89,'CLEANER',0,10.27,'A-7-6','AV-CA-073','AWC12'),(90,'HOLDERLOCK',0,460.89,'A-1-7','AV-CA-074','HL-BT40'),(91,'HOLDERLOCK',0,372.82,'A-1-8','AV-CA-075','HL-BT30'),(92,'TORNILLO ',0,4.17,'A-7-11','A-7-11','BFX0511R'),(93,'O-RING',0,0.1,'A-7-12','A-7-12','MN70-1.5-17.5'),(94,'LIMPIADOR',0,34.64,'','','SC30'),(95,'LIMPIADOR',0,34.46,'','','SC40'),(96,'LIMPIADOR',0,33.4,'','','SCE30'),(97,'LIMPIADOR',0,33.18,'','','SCE40'),(98,'GRINDING WHEEL',300000,2317.95,'B-2-1','HV-IN-001','T107420-01'),(99,'BRUSH',3500,68.56,'B-1-2','HV-IN-002','D100XH30XT6XD10X40LXTH78'),(100,'CHAMFERING TOOL',4000,101.57,'B-3-3','HV-IN-003','KSV23060'),(101,'DRILL',1000,20.89,'B-3-15','HV-IN-004','90361-1100H'),(102,'REAMER',1200,47.53,'B-3-2','HV-IN-005','D4.010X75LXS4.00'),(103,'BRUSH',5000,29.52,'B-3-18','HV-IN-006','A11-CB06M'),(104,'ENDMILL',1100,19.84,'B-3-8','HV-IN-007','GSX40500S-3D'),(105,'CHAMFERING TOOL',800,69.9,'B-3-1','HV-IN-008','HSCT-N D1X45XD6'),(106,'BRUSH',0,9.65,'B-3-10','HV-IN-009','D-171 D10XL50XD6'),(107,'FLAT DRILL',1100,42.99,'B-3-9','HV-IN-010','MDF0700S2D'),(108,'TOOL HOLDER',0,231.55,'B-2-3','HV-IN-011','25T-ECH7SR-60'),(109,'COLLET',0,66.16,'B-3-6','HV-IN-012','AR11-6'),(110,'ADJUSTMENT BOLT',0,5.79,'B-3-19','HV-IN-013','ECH7-05'),(111,'TOOL HOLDER',0,251.54,'B-2-4','HV-IN-014','25T-ECH10SR-60'),(112,'COLLET',0,77.89,'B-3-4','HV-IN-015','AR16-4'),(113,'ADJUSTMENT BOLT',0,17.52,'B-3-5','HV-IN-016','ECH10-05'),(114,'SPANNER',0,38.73,'B-3-7','HV-IN-017','SP-25'),(115,'COLLET',0,66.16,'B-3-13','HV-IN-018','AR11-4'),(116,'SLEEVE',0,26.36,'B-3-14','HV-IN-019','S06M'),(117,'SPANNER',0,24.12,'B-4-2','HV-IN-020','SP-17A'),(118,'TOOL HOLDER',0,290.22,'B-4-1','HV-IN-021','BBT30-MEGA6E-50'),(119,'COLLET',0,71.04,'B-3-11','HV-IN-022','MEC6-6.0AA'),(120,'WRENCH',0,52.61,'A-6-2','HV-IN-023','MGR25'),(121,'TOOL HOLDER',0,227.99,'A-2-7','HV-IN-024','BT30-NBS6-45'),(122,'COLLET',0,55.93,'B-3-12','HV-IN-025','NBC6-6.0AA'),(123,'SCREW',0,7.55,'A-5-16','HV-IN-026','NBA6B'),(124,'WRENCH',0,8.24,'A-6-6-','HV-IN-027','NBK6'),(125,'PULL  BOLT',0,20.61,'B-3-16','HV-IN-028','P30T-2MG'),(126,'TOOL HOLDER',0,371.85,'B-2-5','HV-IN-029','BBT30-HDC8-45'),(127,'CHAMFERING TOOL REGRIND',4000,15.36,'B-4-3','HV-IN-030','R-KSV23060'),(128,'FLAT DRILL',1100,26.35,'B-4-5','HV-IN-032','R-MDF0700S2D'),(129,'TOOL HOLDER',0,371.85,'B-2-2','HV-IN','BBT30-HDC6-45'),(130,'COLLET',0,0,'B-6-1','','AC16-6'),(131,'COLLET',0,0,'B-6-2','','AC16-8'),(132,'COLLET',0,0,'B-6-3','','AC16-10'),(133,'COLLET',0,0,'B-6-4','','YCC 10-10'),(134,'CLEANER',0,34.64,'B-7-1','','SC30'),(135,'CLEANER',0,33.4,'B-7-2','','SC40'),(136,'CLEANER',0,34.46,'B-7-3','','SCE30'),(137,'CLEANER',0,33.18,'B-7-4','','SCE40'),(138,'CEPILLO',0,82.58,'B-1-1','HV-IN-035','D100XT15XH28XTH43-0.9 #240'),(139,'ADAPTOR P/CEPILLO',0,137.84,'B-3-15','HV-IN-036','ADAPTOR P/CEPILLO D100'),(140,'CBN INSERT',1800,36.59,'C-3-1','HV-EX-001','3NUTNGA331LF-BN7500'),(141,'CENTER DRILL',4000,95.88,'C-3-2','HV-EX-002','FX-LDS D6X90DEG'),(142,'DRILL',1000,20.89,'C-3-3','HV-EX-003','78361-1120H'),(143,'REAMER',1000,47.53,'C-3-4','HV-EX-004','D4.006X75LXS4.00'),(144,'BRUSH',3500,29.52,'B-3-18','HV-IN-006','A11-CB06M'),(145,'HOLDER',0,54.36,'C-3-7','HV-EX-006','DTGNR2020K16'),(146,'WRENCH',0,203.92,'C-3-13','HV-EX-015','2852V2.0-8.0'),(147,'BLADE',0,7.52,'C-3-14','HV-EX-016','2859H4.0'),(148,'CLAMP SET',0,25.45,'C-1-15','HV-EX-017','SCP-1'),(149,'SHIM',0,8.01,'C-1-16','HV-EX-018','TNS1604'),(150,'SPANNER',0,2.86,'C-1-17','HV-EX-021','LH040'),(151,'SCREW',0,2.61,'C-1-18','HV-EX-019','BFTX0307N'),(152,'SPANNER',0,2.86,'C-1-19','HV-EX-022','LH025'),(153,'CBN INSERT REGRIND',1800,0,'C-4-1','HV-EX-023','R-3NUTNGA331LF-BN7500'),(154,'CENTER DRILL REGRIND',4000,21.69,'C-4-2','HV-EX-024','R-FX-LDS D6X90DEG'),(155,'PORTAHTAS',0,231.55,'','HV-IN-011','25T-ECH7SR-60'),(156,'BOQUILLA',0,66.16,'','HV-IN-018','AR11-4'),(157,'GRINDING WHEEL',300000,2317.95,'B-2-1','HV-IN-001','T107420-01'),(158,'CEPILLO',3500,82.58,'B-1-1','HV-IN-035','D100XT15XH28XTH43-0.9 #240'),(159,'ADAPTOR P/CEPILLO',0,137.84,'B-3-15','HV-IN-036','ADAPTOR P/CEPILLO D100'),(160,'INSERTO CBN',280,35.56,'B-4-18','SPR-002','2NCVBGA221-BNC2020'),(161,'INSERTO CBN',840,40.23,'B-4-16','SPR-004','3NCTNGA331-BNC2020'),(163,'INSERTO CBN',12600,45.72,'B-4-14','SPR-006','3NCTPGA221-BNC2020');
/*!40000 ALTER TABLE `herramienta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linea`
--

DROP TABLE IF EXISTS `linea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linea` (
  `idLinea` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(10) NOT NULL,
  `Parte_numParte` int(11) NOT NULL,
  PRIMARY KEY (`idLinea`,`Parte_numParte`),
  UNIQUE KEY `idLinea_UNIQUE` (`idLinea`),
  KEY `fk_Linea_Parte1_idx` (`Parte_numParte`),
  CONSTRAINT `fk_Linea_Parte1` FOREIGN KEY (`Parte_numParte`) REFERENCES `parte` (`numParte`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linea`
--

LOCK TABLES `linea` WRITE;
/*!40000 ALTER TABLE `linea` DISABLE KEYS */;
INSERT INTO `linea` VALUES (1,'L#1',1),(2,'L#2',1),(3,'L#4',1),(4,'L#5',1),(5,'L#1',3),(6,'L#2',3),(7,'L#3',3),(11,'L#1',2),(12,'L#2',2),(13,'L#3',2),(14,'MX-0022',4),(15,'MX-0023',4);
/*!40000 ALTER TABLE `linea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linea_has_operacion`
--

DROP TABLE IF EXISTS `linea_has_operacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linea_has_operacion` (
  `Linea_idLinea` int(11) NOT NULL,
  `Operacion_numOperacion` int(11) NOT NULL,
  `seccion` int(11) NOT NULL,
  PRIMARY KEY (`Linea_idLinea`,`Operacion_numOperacion`),
  KEY `fk_Linea_has_Operacion_Operacion1_idx` (`Operacion_numOperacion`),
  KEY `fk_Linea_has_Operacion_Linea1_idx` (`Linea_idLinea`),
  CONSTRAINT `fk_Linea_has_Operacion_Linea1` FOREIGN KEY (`Linea_idLinea`) REFERENCES `linea` (`idLinea`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Linea_has_Operacion_Operacion1` FOREIGN KEY (`Operacion_numOperacion`) REFERENCES `operacion` (`numOperacion`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linea_has_operacion`
--

LOCK TABLES `linea_has_operacion` WRITE;
/*!40000 ALTER TABLE `linea_has_operacion` DISABLE KEYS */;
INSERT INTO `linea_has_operacion` VALUES (1,1,2),(1,2,2),(1,3,2),(1,4,2),(1,5,1),(1,6,2),(1,7,3),(1,8,1),(1,17,2),(2,1,2),(2,2,2),(2,3,2),(2,4,2),(2,5,1),(2,6,2),(2,7,3),(2,8,1),(2,17,2),(3,1,2),(3,2,2),(3,3,2),(3,4,2),(3,5,1),(3,6,2),(3,7,3),(3,8,1),(3,17,2),(4,1,2),(4,2,2),(4,3,2),(4,4,2),(4,5,1),(4,6,2),(4,7,3),(4,8,1),(4,17,2),(5,13,2),(5,14,2),(5,15,2),(5,16,1),(6,13,2),(6,14,2),(6,15,2),(6,16,1),(7,13,2),(7,14,2),(7,15,2),(11,9,1),(11,10,2),(11,11,2),(11,12,3),(11,18,3),(12,9,1),(12,10,2),(12,11,2),(12,12,3),(12,18,3),(13,10,2),(13,11,2),(14,19,1),(15,19,1);
/*!40000 ALTER TABLE `linea_has_operacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operacion`
--

DROP TABLE IF EXISTS `operacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operacion` (
  `numOperacion` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `Parte_numParte` int(11) NOT NULL,
  PRIMARY KEY (`numOperacion`,`Parte_numParte`),
  UNIQUE KEY `numOperacion_UNIQUE` (`numOperacion`),
  KEY `fk_Operacion_Parte1_idx` (`Parte_numParte`),
  CONSTRAINT `fk_Operacion_Parte1` FOREIGN KEY (`Parte_numParte`) REFERENCES `parte` (`numParte`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operacion`
--

LOCK TABLES `operacion` WRITE;
/*!40000 ALTER TABLE `operacion` DISABLE KEYS */;
INSERT INTO `operacion` VALUES (1,'OP40',1),(2,'OP50',1),(3,'OP60A',1),(4,'OP70',1),(5,'OP10',1),(6,'OP55',1),(7,'OP80',1),(8,'OP20',1),(9,'OP10',2),(10,'OP20',2),(11,'OP30',2),(12,'OP40A',2),(13,'OP40',3),(14,'OP30',3),(15,'OP20',3),(16,'OP10',3),(17,'OP60B',1),(18,'OP40B',2),(19,'OP10',4);
/*!40000 ALTER TABLE `operacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operacion_has_herramienta`
--

DROP TABLE IF EXISTS `operacion_has_herramienta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operacion_has_herramienta` (
  `Operacion_numOperacion` int(11) NOT NULL,
  `Herramienta_numHerramienta` int(11) NOT NULL,
  PRIMARY KEY (`Operacion_numOperacion`,`Herramienta_numHerramienta`),
  KEY `fk_Operacion_has_Herramienta_Herramienta1_idx` (`Herramienta_numHerramienta`),
  KEY `fk_Operacion_has_Herramienta_Operacion1_idx` (`Operacion_numOperacion`),
  CONSTRAINT `fk_Operacion_has_Herramienta_Herramienta1` FOREIGN KEY (`Herramienta_numHerramienta`) REFERENCES `herramienta` (`numHerramienta`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Operacion_has_Herramienta_Operacion1` FOREIGN KEY (`Operacion_numOperacion`) REFERENCES `operacion` (`numOperacion`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operacion_has_herramienta`
--

LOCK TABLES `operacion_has_herramienta` WRITE;
/*!40000 ALTER TABLE `operacion_has_herramienta` DISABLE KEYS */;
INSERT INTO `operacion_has_herramienta` VALUES (1,3),(1,4),(2,5),(2,6),(3,7),(17,7),(3,8),(17,8),(3,9),(17,9),(3,10),(17,10),(3,11),(17,11),(4,12),(4,13),(4,14),(5,15),(6,16),(3,17),(17,17),(4,18),(7,19),(8,26),(7,27),(7,73),(7,74),(2,75),(2,76),(2,77),(3,78),(3,79),(17,79),(3,80),(17,80),(3,81),(17,81),(4,82),(4,83),(5,84),(6,85),(9,98),(10,99),(11,100),(11,101),(11,102),(11,103),(14,103),(12,104),(18,104),(12,105),(18,105),(12,106),(18,106),(12,107),(18,107),(11,127),(12,128),(18,128),(11,138),(13,140),(14,141),(14,142),(14,143),(15,144),(13,153),(14,154),(16,157),(15,158),(15,159),(19,160),(19,161),(19,163);
/*!40000 ALTER TABLE `operacion_has_herramienta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parte`
--

DROP TABLE IF EXISTS `parte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parte` (
  `numParte` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`numParte`),
  UNIQUE KEY `numParte_UNIQUE` (`numParte`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parte`
--

LOCK TABLES `parte` WRITE;
/*!40000 ALTER TABLE `parte` DISABLE KEYS */;
INSERT INTO `parte` VALUES (1,'Carrier'),(2,'HVI'),(3,'HVE'),(4,'MAZDA');
/*!40000 ALTER TABLE `parte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produccion_diaria`
--

DROP TABLE IF EXISTS `produccion_diaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produccion_diaria` (
  `idProd` int(11) NOT NULL AUTO_INCREMENT,
  `cantPiezas` float NOT NULL,
  `fecha` date NOT NULL,
  `idOperacion` int(11) NOT NULL,
  `idLinea` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProd`),
  UNIQUE KEY `idOperacion_UNIQUE` (`idProd`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produccion_diaria`
--

LOCK TABLES `produccion_diaria` WRITE;
/*!40000 ALTER TABLE `produccion_diaria` DISABLE KEYS */;
INSERT INTO `produccion_diaria` VALUES (1,1113,'2017-07-12',5,1),(2,1113,'2017-07-12',8,1),(3,435,'2017-07-12',1,1),(4,435,'2017-07-12',2,1),(5,435,'2017-07-12',3,1),(6,435,'2017-07-12',4,1),(7,435,'2017-07-12',6,1),(8,435,'2017-07-12',17,1),(9,388,'2017-07-12',7,1),(10,0,'2017-07-12',5,2),(11,0,'2017-07-12',8,2),(12,380,'2017-07-12',1,2),(13,380,'2017-07-12',2,2),(14,380,'2017-07-12',3,2),(15,380,'2017-07-12',4,2),(16,380,'2017-07-12',6,2),(17,380,'2017-07-12',17,2),(18,7,'2017-07-12',7,2),(19,0,'2017-07-12',5,3),(20,0,'2017-07-12',8,3),(21,427,'2017-07-12',1,3),(22,427,'2017-07-12',2,3),(23,427,'2017-07-12',3,3),(24,427,'2017-07-12',4,3),(25,427,'2017-07-12',6,3),(26,427,'2017-07-12',17,3),(27,0,'2017-07-12',7,3),(28,0,'2017-07-12',5,4),(29,0,'2017-07-12',8,4),(30,225,'2017-07-12',1,4),(31,225,'2017-07-12',2,4),(32,225,'2017-07-12',3,4),(33,225,'2017-07-12',4,4),(34,225,'2017-07-12',6,4),(35,225,'2017-07-12',17,4),(36,0,'2017-07-12',7,4),(37,1821,'2017-07-12',9,11),(38,1961,'2017-07-12',10,11),(39,1961,'2017-07-12',11,11),(40,2385,'2017-07-12',12,11),(41,2385,'2017-07-12',18,11),(42,2584,'2017-07-12',9,12),(43,1935,'2017-07-12',10,12),(44,1935,'2017-07-12',11,12),(45,2823,'2017-07-12',12,12),(46,2823,'2017-07-12',18,12),(47,1970,'2017-07-12',10,13),(48,1970,'2017-07-12',11,13),(49,1440,'2017-07-12',16,5),(50,1418,'2017-07-12',13,5),(51,1418,'2017-07-12',14,5),(52,1418,'2017-07-12',15,5),(53,1500,'2017-07-12',16,6),(54,896,'2017-07-12',13,6),(55,896,'2017-07-12',14,6),(56,896,'2017-07-12',15,6),(57,1556,'2017-07-12',13,7),(58,1556,'2017-07-12',14,7),(59,1556,'2017-07-12',15,7),(60,1705,'2017-07-18',5,1),(61,1705,'2017-07-18',8,1),(62,407,'2017-07-18',1,1),(63,407,'2017-07-18',2,1),(64,407,'2017-07-18',3,1),(65,407,'2017-07-18',4,1),(66,407,'2017-07-18',6,1),(67,407,'2017-07-18',17,1),(68,875,'2017-07-18',7,1),(69,0,'2017-07-18',5,2),(70,0,'2017-07-18',8,2),(71,363,'2017-07-18',1,2),(72,363,'2017-07-18',2,2),(73,363,'2017-07-18',3,2),(74,363,'2017-07-18',4,2),(75,363,'2017-07-18',6,2),(76,363,'2017-07-18',17,2),(77,867,'2017-07-18',7,2),(78,0,'2017-07-18',5,3),(79,0,'2017-07-18',8,3),(80,387,'2017-07-18',1,3),(81,387,'2017-07-18',2,3),(82,387,'2017-07-18',3,3),(83,387,'2017-07-18',4,3),(84,387,'2017-07-18',6,3),(85,387,'2017-07-18',17,3),(86,0,'2017-07-18',7,3),(87,0,'2017-07-18',5,4),(88,0,'2017-07-18',8,4),(89,0,'2017-07-18',1,4),(90,0,'2017-07-18',2,4),(91,0,'2017-07-18',3,4),(92,0,'2017-07-18',4,4),(93,0,'2017-07-18',6,4),(94,0,'2017-07-18',17,4),(95,0,'2017-07-18',7,4);
/*!40000 ALTER TABLE `produccion_diaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idUsuario` varchar(10) NOT NULL,
  `contrasena` varchar(150) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `puesto` varchar(20) NOT NULL,
  `ABCHerr` bit(1) DEFAULT NULL,
  `ABCPart` bit(1) DEFAULT NULL,
  `CambioHerr` bit(1) DEFAULT NULL,
  `PrdoDiaria` bit(1) DEFAULT NULL,
  `charts` bit(1) DEFAULT NULL,
  `users` bit(1) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `idUsuario_UNIQUE` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('00019','e10adc3949ba59abbe56e057f20f883e','Luz Maria','Esparza Perez','Becario','','','','','','\0'),('100001','8af90b6db43404409c11755ab62eee01','Martin','Fuantos','Ingeniero','','','','','','');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-20 17:24:05
