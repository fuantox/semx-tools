<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['user'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">'; 
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<div class="container main-content">
			<div class="row">
				<h1> Users</h1>
			</div>
			

			<div class="col-md-8"><input type="hidden" class="form-control"></div>
			<div class="col-md-2"><button type="button" onclick="mostrar()" id="balta" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add User</button></div>
			<div class="col-md-2"><button type="button" id="perm_button" class="btn btn-success" onclick="window.location='Permisos.php'"><span class="glyphicon glyphicon-plus"></span>&nbsp;Permissions</button></div>

			<!-- DAR DE ALTA NUEVOS USUARIOS -->
			
			<div class="row" id="alta" style="display:none;">
				<table class="table">
					<tbody><tr class="thead-inverse" style="text-align:center; border-style:ridge; border-width: 1px; border-color:#FFF;"><td>
						<div class="row">
						  <div class="col-md-4"><input type="text" class="form-control" id="idu" name="idu" placeholder="ID"></div>
						  <div class="col-md-4"><input type="text" class="form-control" id="nameu" name="nameu" placeholder="Name"></div>
						  <div class="col-md-4"><input type="text" class="form-control" id="lastu" name="lastu" placeholder="Last Name"></div>
						  <div class="col-md-4"><input type="text" class="form-control" id="jobu" name="jobu"  placeholder="Job post"></div>
						  <div class="col-md-4"><input type="password" class="form-control" id="passu" name="passu" placeholder="Password"></div>
						  <div class="col-md-4"><input type="password" class="form-control" id="rpassu" name="rpassu"  placeholder="Repeat password"></div>
						  
						  <div class="col-md-4" style="padding-left:90%;"><button type="button" onclick="add()" class="btn btn-success btn-sm"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Add</button></div>
						  <div class="col-md-4" style="padding-left:90%;"><button type="button" onclick="ocultar()" id="bocultar" style="display:none;" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Close</button></div>
						  
						</div>
					</td></tr></tbody>
				</table>
			</div>
			
			
			<!--TABLA-->
			<br><br>
			<div id="usuarios">
				
			</div>
		</div>
	
        
		
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<!-- Dependencies -->
	<script src="jquery/jquery.js" type="text/javascript"></script>
	<script src="jquery/jquery.ui.draggable.js" type="text/javascript"></script>

	<!-- Core files -->
	<script src="jquery/jquery.alerts.js" type="text/javascript"></script>
	<link href="jquery/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script type="text/javascript">
		//MUESTRA LOS ELEMENTOS EN DONDE PODEMOS AGREGAR UN NUEVO USUARIO
		function mostrar(){
			document.getElementById('alta').style.display = 'block';
			document.getElementById('balta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'block';
			document.getElementById('perm_button').style.display = 'none';
		}
		//OCULTA LOS ELEMENTOS PARA YA NO DAR DE ALTA MAS USUARIOS
		function ocultar(){
			document.getElementById('alta').style.display = 'none';
			document.getElementById('bocultar').style.display = 'none';
			document.getElementById('balta').style.display = 'block';
			document.getElementById('perm_button').style.display = 'block';
		}
	</script>
	<script type="text/javascript" src="js/user.js"></script>

</html>