var cants = Number(document.getElementById("results").value); //Obtenemos la cantidad de resultados encontrados
//alert(cants);

var ops = Number(document.getElementById("nops").value); //cantidad de operaciones que apareceran
//alert(ops);
var operaciones = [];
for(i=0; i<ops;i++){ // Guarda un vector con las operaciones que son para la parte seleccionada y que serán mostradas en la gráfica
	operaciones[i] = document.getElementById("operaciones"+i).value;
}
operaciones.sort(); //vector ordenado alfabéticamentes

var fechas = [];
var band3=0;
var j3=0;
var x3=0;
for(i=0; i<cants; i++){ //Guardar las fechas encontradas en la base de datos que serán mostradas en la tabla
	var fechas1 = document.getElementById("fecha"+band3).value;
	var fechas2 = document.getElementById("fecha"+i).value;
	if(fechas1 == fechas2){
		fechas[j3] = document.getElementById("fecha"+i).value;
	}
	else{
		j3++;
		band3 = i;
		i--;
	}
}



var totalM = []; //Total de la multiplicación de cantidad * precio
var cantPiezasT = []; //Cantidad de piezas
var band = 0;
var band2 = 0;
var band3 = 0;
var x = 0;
var y = 0;
var aux = 0;
for(i=0; i<ops; i++){
	totalM[i] = [];
	cantPiezasT[i] = [];
	for(j=0; j<cants;j++){
		var fech1 = document.getElementById("fecha"+band).value;
		var fech2 = document.getElementById("fecha"+j).value;
		if(fech1 == fech2){ //Solo guardará los costos del mismo día, sino, pasará a un nuevo día, y guardará en una nueva posición del vector
			if(document.getElementById("descop"+j).value == operaciones[i]){
				if(Number(document.getElementById("piezas"+j).value) != 0){
					x += ((Number(document.getElementById("cant"+j).value)*Number(document.getElementById("precio"+j).value)));
					totalM[i][band2] = Number(x) + 0.0;
					
					var p = document.getElementById("piezas"+j).value;
					if(p != aux) {
						y += Number(p);
						cantPiezasT[i][band3] = Number(y) + 0.0;
						aux = p;
					}
				}
				else if(Number(document.getElementById("piezas"+j).value) == 0){
					x += 0.0;
					totalM[i][band2] = Number(x) + 0.0;
				}
			}
			else{
				x=0;
				y=0;
			}
		}
		else{
			x = 0;
			band = j;
			band2++;
			band3++;
			j--;
		}
	}
	band = 0;
	band2 = 0;
	band3 = 0
}

for(i=0; i<totalM.length; i++){
	for(j=0; j<totalM[i].length; j++){
		if(totalM[i][j] == null || totalM[i][j] == "") totalM[i][j] = 0;
	}
}
for(i=0; i<cantPiezasT.length; i++){
	for(j=0; j<cantPiezasT[i].length; j++){
		if(cantPiezasT[i][j] == null || cantPiezasT[i][j] == "") cantPiezasT[i][j] = 0;
	}
}

//alert(totalM);
//alert(cantPiezasT);

var total = [];
for(i=0; i<totalM.length; i++){
	total[i] = [];
	for(j=0; j<totalM[i].length;j++){
		if(cantPiezasT[i][j]>0)
			total[i][j] = (parseFloat(totalM[i][j])/parseFloat(cantPiezasT[i][j])).toFixed(2);
		else 
			total[i][j] = 0;
	}
}

var totalReal = [];
var z = 0;
var pos = 0;
for(i=0; i<fechas.length; i++){
	for(j=0; j<total.length;j++){
		if(total[j][i] != null && total[j][i] != ""){
			z += Number(total[j][i]) +0.0;
			totalReal[pos] = z.toFixed(2);
		}
		else{
			z += 0.0;
			totalReal[pos] = z.toFixed(2);
		}
	}
	pos++;
	z = 0;
}
//alert(totalReal);



var totalN = []; //Total de la multiplicación de cantidad * precio
var cantPiezasU = []; //Cantidad de piezas
var band = 0;
var band2 = 0;
var band3 = 0;
var x = 0;
var y = 0;
var aux = 0;
for(i=0; i<ops; i++){
	totalN[i] = [];
	cantPiezasU[i] = [];
	for(j=0; j<cants;j++){
		var fech1 = document.getElementById("fecha"+band).value;
		var fech2 = document.getElementById("fecha"+j).value;
		//alert(fech1+"    "+fech2);
		if(fech1 == fech2){ //Solo guardará los costos del mismo día, sino, pasará a un nuevo día, y guardará en una nueva posición del vector
			if(document.getElementById("descop"+j).value == operaciones[i]){
				if(Number(document.getElementById("piezas"+j).value) != 0){
					x += ((Number(document.getElementById("cant"+j).value)*Number(document.getElementById("precio"+j).value)));
					totalN[i][band2] = Number(x) + 0.0;
					
					var p = Number(document.getElementById("cant"+j).value)*Number(document.getElementById("vida"+j).value);
					if(p != aux) {
						y += Number(p);
						cantPiezasU[i][band3] = Number(y) + 0.0;
						aux = p;
					}
				}
				else if(Number(document.getElementById("piezas"+j).value) == 0){
					x += 0.0;
					totalN[i][band2] = Number(x) + 0.0;
				}
			}
			else{
				x=0;
				y=0;
			}
		}
		else{
			x = 0;
			band = j;
			band2++;
			band3++;
			j--;
		}
	}
	band = 0;
	band2 = 0;
	band3 = 0
}

for(i=0; i<totalN.length; i++){
	for(j=0; j<totalN[j].length; j++){
		if(totalN[i][j] == null || totalN[i][j] == "") totalN[i][j] = 0;
	}
}
for(i=0; i<cantPiezasU.length; i++){
	for(j=0; j<cantPiezasU[j].length; j++){
		if(cantPiezasU[i][j] == null || cantPiezasU[i][j] == "") cantPiezasU[i][j] = 0;
	}
}

//alert(totalN);
//alert(cantPiezasU);

var total2 = [];
for(i=0; i<totalN.length; i++){ //Guardará un vector con la suma de los costos de herramienta por pieza, para cada operación en X día
	total2[i] = [];
	for(j=0; j<totalN[i].length;j++){
		if(cantPiezasU[i][j]>0)
			total2[i][j] = (parseFloat(totalN[i][j])/parseFloat(cantPiezasU[i][j])).toFixed(2);
		else 
			total2[i][j] = 0;
	}
}

var totalIdeal = [];
var z = 0;
var pos = 0;
for(i=0; i<fechas.length; i++){
	for(j=0; j<total2.length;j++){
		if(total[j][i] != null && total[j][i] != ""){
			z += Number(total2[j][i]) +0.0;
			totalIdeal[pos] = z.toFixed(2);
		}
		else{
			z += 0.0;
			totalIdeal[pos] = z.toFixed(2);
		}
	}
	pos++;
	z = 0;
}
//alert(totalIdeal);


/*var totalIdeal = [];
var band2=0;
var j2=0;
var x2=0;
for(i=0; i<cants; i++){ //ciclo que funciona para guardar todos los costos ideales
	var fecha1 = document.getElementById("fecha"+band2).value;
	var fecha2 = document.getElementById("fecha"+i).value;
	if(fecha1 == fecha2){ //Sumaremos todos los costos que se hayan realizado en el mismo día (herramientas cambiadas)*(precio) / (herramientas cambiadas)*(tiempo de vida)
		if(Number(document.getElementById("cant"+i).value)!=0){
			x2 += (Number(document.getElementById("cant"+i).value)*Number(document.getElementById("precio"+i).value))/(Number(document.getElementById("cant"+i).value)*Number(document.getElementById("vida"+i).value));
			totalIdeal[j2] = Number(x2).toFixed(4) + 0.0;
		}
		else if(Number(document.getElementById("cant"+i).value)==0){
			x2 += 0.0;
			totalIdeal[j2] = Number(x2) + 0.0;
		}
	}
	else{
		j2++;
		band2 = i;
		i--;
		x2=0;
	}
}
//alert(totalIdeal);*/

var costos = 0;
for(i=0; i<cants; i++){
	costos = Number(costos) + Number(Number(document.getElementById("precio"+i).value));
}
var piezas = 0;
for(i=0; i<cants; i++){
	piezas = Number(piezas) + Number(Number(document.getElementById("piezas"+i).value));
}
costo = Number(costos) / Number(piezas);
document.getElementById('costototal').value= costo.toFixed(4);

 var data = {
  labels: fechas,
  datasets: [{
    label: "Ideal Cost",
    //new option, barline will default to bar as that what is used to create the scale
    type: "line",
	fillColor: "rgba(220,220,220,0.2)",
    strokeColor: "rgba(0,0,0,0.6)",
    pointColor: "rgba(0,0,0,0.6)",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "rgba(220,220,220,1)",
    datasetFill:false,
    data: totalIdeal
  }, {
    label: "Real Cost",
    //new option, barline will default to bar as that what is used to create the scale
    type: "bar",
    fillColor: "rgba(255, 159, 64, 1)",
    strokeColor: "rgba(255, 159, 64, 1)",
    pointColor: "rgba(255, 159, 64, 1)",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "rgba(255, 159, 64, 1)",
    data: totalReal
  }] 
};

var ctx = document.getElementById("myChart").getContext("2d");
var chart = new Chart(ctx).Overlay(data, {
  //responsive: false
	//El código siguiente es para mostrar los valores de cada día de las gráficas
	showTooltips: false,
    onAnimationComplete: function () {
		var ctx = this.chart.ctx;
		ctx.font = this.scale.font;
		ctx.fillStyle = this.scale.textColor;
		ctx.textAlign = "center";	
		aux = 0;
		this.datasets.forEach(function (dataset) {
			dataset.points.forEach(function (point) {
				ctx.fillStyle = 'black';
				ctx.fillText(Number(point.value).toFixed(2), point.x, point.y - 20);
			});
			dataset.bars.forEach(function (bar) {
				ctx.fillStyle = 'gray';
                ctx.fillText(Number(bar.value).toFixed(2), bar.x, 300);
            });
		});
    }
	
});

document.getElementById('datos').style.display = 'none';

function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}