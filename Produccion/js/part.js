function getParts(){
	xmlhttp2 = new XMLHttpRequest();
	
	urlPath = "API/part/1";
	console.log(urlPath);
	xmlhttp2.open("get", urlPath, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONS PARTS: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showList();
};

function showList(){
    console.log("Entro");
	taskBody = document.getElementById("partes");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log("Entro");
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
			if(myArr.length!=0){
				
				out += '<table class="table" id="tabla">'
				     + '<thead style="border-top:10px; background:black; color:white;">'
						 + '<tr  class="thead-inverse">'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-6">Part Name</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">Options</th>'
						 + '</tr>'
					 + '</thead>'
				 + '<tbody>'; 
						
				for(i = 0; i < myArr.length; i++) { //Ciclo para mostrar todos los datos de la base de datos de Herramientas
					out += '<td id="namep'+myArr[i].numParte+'" style="text-align:center;">' 
					+ '<a href="Linea.php?num='+myArr[i].numParte+'&name='+myArr[i].nombre+'">'+myArr[i].nombre+' <input type="hidden" id="nameps'+myArr[i].numParte+'" value="'+myArr[i].nombre+'">' 
					+ '</a></td>'
					
					+ '<td style="text-align:center;">'
					  + '<button type="button" id="edit_button'+myArr[i].numParte+'" class="btn btn-default" onclick="edit('+myArr[i].numParte+')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit</button>'
					  + '<button type="button" id="done_button'+myArr[i].numParte+'" class="btn btn-success" onclick="save('+myArr[i].numParte+')" style="display:none;"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Done</button>'
					  + '<button type="button" id="delete_button'+myArr[i].numParte+'" class="btn btn-danger" onclick="del('+myArr[i].numParte+')"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button>'
					+ '</td></tr>';
				}
				out +=  '</tbody>'
				+ '</table>';
            }
			else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Parts found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
	urlPath = "API/part/1";
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

function add(){ //Validar una alta a herramienta
	var name = document.getElementById("PartName").value;
	if(name!=""){ //Verificar que no exista ningún campo en blanco
		params = "name="+name;
		
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", "API/part/part.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlhttp.readyState == 4 && http.status == 200) {
				sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
			}
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				console.log(xmlhttp.responseText);
				swal("Successfully added", "Part registered successfully.", "success"); 
				document.getElementById("PartName").value="";
				ocultar();
				showList();  
			}
		}
		xmlhttp.send(params); 
		showList();  
		showList();
	}
	else{
		sweetAlert("Error", "There are blank fields", "error");
	}
};

function edit(no){
	document.getElementById("edit_button"+no).style.display='none';
	document.getElementById("delete_button"+no).style.display='none';
	document.getElementById("done_button"+no).style.display='block';
	
	var nameps=document.getElementById("nameps"+no).value;
	
	var namep=document.getElementById("namep"+no);
	
	var namep_data=namep.innerHTML;
		
	namep.innerHTML="<input type='text' id='namep_text"+no+"' class='form-control' value='"+nameps+"'>";
}

function save(no) {
	var namep=document.getElementById("namep_text"+no).value;
	
	if(namep!=""){
		document.getElementById("namep"+no).innerHTML=namep;

		document.getElementById("edit_button"+no).disabled = false; //habilitar el boton
		
		params = "/"+no+"/"+namep;
			
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		xmlhttp.open("PUT", "API/part"+params, true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlhttp.readyState == 4 && http.status == 200) {
				sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
			}
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				console.log(xmlhttp.responseText);
				swal("Changes Saved", "Part saved successfully.", "success"); 
				showList(); 
			}
		}
		xmlhttp.send(params); 
		showList();  
		showList(); 
	}
	else{
		sweetAlert("Error", "There are blank fields", "error");
	}
}

function del(no){ //Función para realizar una baja
	swal({   
        title: "Are you sure?",   
        text: "Part data will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/part/"+no, true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                if(JSON.parse(xmlhttp.responseText).affected_rows != 1){
                    //swal("Error", "Employee "+id+" could not be deleted. \n Error 0x0001: Existent consumption registries.", "error"); 
                }
                else{
                    swal("Deleted!", "Part has been deleted.", "success"); 
					showList();
					//alert("Successfully deleted");
                }
            }
        }
        xmlhttp.send();        
        showList();
    });
    showList();	
};

function isNumber(n){ //Función para validar que una cadena sea un número
  return !isNaN(parseFloat(n)) && isFinite(n);
};

getParts();