function getTools(){
	var x = document.getElementById("selOp");
	var o = x.options[x.selectedIndex].value;
	var op = o;
	
	var x2 = document.getElementById("selPart");
	var part = x2.options[x2.selectedIndex].value;
	var parte = part;
	
	xmlhttp2 = new XMLHttpRequest();
	
	urlPath = "API/cambioHerr/"+op;
	console.log(urlPath);
	xmlhttp2.open("get", urlPath+"?idpart="+parte, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONSHERR: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showList2();
};

function showList2(){
	taskBody2 = document.getElementById("alltool");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
		//if (this.readyState == XMLHttpRequest.DONE) {		
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
            if(myArr.length!=0){
				console.log('JSON SHOW LIST 2: ' + xmlhttp.responseText);
				out = '<div class="col-md-2"><label>Tools: </label></div>'
					+ '<form id="selTool" action="">'
					+ '<table>';
				for(i = 0; i < myArr.length; i++) {
					out += '<tr>'
					+ '<td><input type="checkbox" id="herr'+myArr[i].numHerramienta+'" value="'+myArr[i].numHerramienta+'"> <label>'+myArr[i].descripcion+' - '+myArr[i].shmmcode+'&nbsp;</label></td>'
					+ '<td><input type="hidden" id="hname'+myArr[i].numHerramienta+'" value="'+myArr[i].descripcion+'"></td>'
					+ '<td><input type="number" id="cant'+myArr[i].numHerramienta+'" value="0" min="0" max="10000"></td>'
					+ '<td><label>&nbsp;&nbsp;Why?&nbsp;</label></td>'
					+ '<td><select id="razon'+myArr[i].numHerramienta+'" class="selectpicker">'
						+ '<option >Select an option...</option>'
						+ '<option>End of useful life.</option>'
						+ '<option>Broken tool.</option>'
						+ '<option>Quality defect.</option>'
						+ '<option>Change of model.</option>'
						+ '<option>Engineering test.</option>'
						+ '<option>Cleaning.</option>'
					+ '</select><br></td>'
					+ '</tr>';
                }
				out += '</table>'
					+ '</form>'
					+ '<input type="hidden" id="total" name="total" value='+i+' >';
            }
            else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Tools found.</h1>'
                +     '</div>';   
            }
            taskBody2.innerHTML = out;
        }
    };
	var x = document.getElementById("selOp");
	var o = x.options[x.selectedIndex].value;
	var op = o;
	
	var x2 = document.getElementById("selPart");
	var part = x2.options[x2.selectedIndex].value;
	var parte = part;
	
	urlPath = "API/cambioHerr/" + op;
    xmlhttp.open("get", urlPath+"?idpart="+parte, true);
    xmlhttp.send();
};

function changeRegister() { 
	var a = document.getElementById("selPart");
	var part = a.options[a.selectedIndex].value;
	
	var b = document.getElementById("selOp");
	var op = b.options[b.selectedIndex].value;
	
	var c = document.getElementById("selLine");
	var line = c.options[c.selectedIndex].value;
	
	divCont = document.getElementById('selTool'); 
	checks  = divCont.getElementsByTagName('input');
	
	var hsel = new Array();  //herramientas seleccionadas (valores checkbox)
	var cant = new Array();  //cantidades para las herramientas seleccionadas
	var razones = new Array();  //razones de cambio para las herramientas seleccionadas
	var hname = new Array();  //herramientas seleccionadas (textos checkbox)
	
	var band1 = 0;
	var band2 = 0;
	var pos = 0;
	
	var fecha = document.getElementById("startd").value;
	if(fecha == "" || fecha == null){
		swal("Error", "Select a day", "error"); 
	}
	else{
		for(i=0;i<checks.length; i++){ //Ciclo con el que iremos tomando los checkbox seleccionados
			if(checks[i].checked == true){
				band1 = band1 + 1;
				//alert("BAND1: " + band1);
				hsel[pos] = checks[i].value; 
				cant[pos] = document.getElementById("cant" + checks[i].value).value;
				hname[pos] = document.getElementById("hname" + checks[i].value).value;
				
				switch(document.getElementById("razon" + checks[i].value).value){
					case 'End of useful life.': razones[pos] = 1; break;
					case 'Broken tool.': razones[pos] = 2; break;
					case 'Quality defect.': razones[pos] = 3; break;
					case 'Change of model.': razones[pos] = 4; break;
					case 'Engineering test.': razones[pos] = 5; break;
					case 'Cleaning.': razones[pos] = 6; break;
					default: razones[pos] = 0; break;
				}
				
				if(razones[pos] != 0){
					band2 = band2 + 1;
					pos += 1;
				}
				else {
					swal("Error", "Select an option in Tool " + hname[pos], "error"); 
					break;
				}
			}
		}
		
		if(band1 == band2){ //Se procede a la alta del registro, ya todo fue validado
			var hsels = hsel.join(','); //juntamos todos los valores en una cadena
			var cants = cant.join(','); //juntamos todos los valores en una cadena
			var razon = razones.join(','); //juntamos todos los valores en una cadena
			
			params = "fecha="+fecha+"&numpart="+part+"&numop="+op+"&hsels="+hsels
					+"&cants="+cants+"&razones="+razon+"&linea="+line;
					
			var xmlhttp;
			xmlhttp = new XMLHttpRequest();
			xmlhttp.open("POST", "API/cambioHerr/cambioHerr.php", true);
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlhttp.readyState == 4 && http.status == 200) {
					sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
				}
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == XMLHttpRequest.DONE) {
					console.log(xmlhttp.responseText);
					swal("Successfully added", "Change registered successfully.", "success");
					showList2();  
				}
			}
			xmlhttp.send(params); 
			showList2();  
			showList2();
		}
	}
	showList2();
};