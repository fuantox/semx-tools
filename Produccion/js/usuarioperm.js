function getUser(id){
	xmlhttp2 = new XMLHttpRequest();
    urlPath = "API/usuarioperm/" + id;
    console.log(urlPath);
    xmlhttp2.open("get", urlPath, true);

    xmlhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            if(myArr.length!=0){
                console.log(xmlhttp2.responseText);
            }
            else{
                console.log(xmlhttp2.responseText);
            }
        }
    }
    
    xmlhttp2.send();
    showList();
};


function showList(){
    taskBody = document.getElementById("permisos");

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            var out = "";
            var i;
            if(myArr.length!=0){

                out += '<table class="table table-sm table-hover">'
                    +       '<thead class="thead-inverse">'
                    +           '<tr>'
                    +               '<th class="table-bordered text-center idColumn">ID</th>'
                    +               '<th class="table-bordered text-center nameColumn">Name</th>'
					+               '<th class="table-bordered text-center nameColumn" colspan="4">Assign Permissions</th>'
                    +           '</tr>'
                    +       '</thead>'
                    +   '<tbody id="taskTable">';
				
				for(i = 0; i < myArr.length; i++) {
					if(myArr[i].idUsuario != '100001'){
					out += '<tr id="tr' + myArr[i].idUsuario + '">'
						+ '<td id="title' + myArr[i].idUsuario + '" class="text-center">' + myArr[i].idUsuario + '</td>'
						+ '<td id="name' + myArr[i].idUsuario + '" class="text-center">' + myArr[i].nombre + ' ' + myArr[i].apellido 
						+ '</td>'
						+ '<td id="perm1' + myArr[i].idUsuario + '" style="display:none;">'; 
						
						if(myArr[i].ABCHerr == 1){ 
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="tool" checked>Add/Delete/Edit Tools.<br>';
						}
						else if(myArr[i].ABCHerr == 0 || myArr[i].ABCHerr == null){
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="tool">Add/Delete/Edit Tools.<br>';
						}
						
						if(myArr[i].ABCPart == 1){ 
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="part" checked>Add/Delete/Edit Parts.<br>';
						}
						else if(myArr[i].ABCPart == 0 || myArr[i].ABCPart == null){
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="part">Add/Delete/Edit Parts.<br>';
						}
						
						out += '</td>'
						+ '<td id="perm2' + myArr[i].idUsuario + '" style="display:none;">'; 
						
						if(myArr[i].CambioHerr == 1){ 
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="change" checked>Tool Change.<br>';
						}
						else if(myArr[i].CambioHerr == 0 || myArr[i].CambioHerr == null){
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="change">Tool Change.<br>';
						}
						
						if(myArr[i].PrdoDiaria == 1){ 
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="daily" checked>Daily Production.<br>';
						}
						else if(myArr[i].PrdoDiaria == 0 || myArr[i].PrdoDiaria == null){
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="daily">Daily Production.<br>';
						}
						
						out += '</td>'
						+ '<td id="perm3' + myArr[i].idUsuario + '" style="display:none;">'; 
						
						if(myArr[i].charts == 1){ 
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="chart" checked>Charts.<br>';
						}
						else if(myArr[i].charts == 0 || myArr[i].charts == null){
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="chart">Charts.<br>';
						}
						
						if(myArr[i].users == 1){ 
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="user" checked>Users.<br>';
						}
						else if(myArr[i].users == 0 || myArr[i].users == null){
							out += '<input type="checkbox" name="perm'+myArr[i].idUsuario+'[]" value="user">Users.<br>';
						}
						
						out += '</td>'
						
							+ '<td id="perm4' + myArr[i].idUsuario + '">' 
							+ '<input type="hidden" id="selUser'+myArr[i].idUsuario+'" value="' + myArr[i].idUsuario + '">'
							+ '<input type="hidden" id="opcion" value="altaperm">'
							+ '<button id="b_send'+myArr[i].idUsuario+'" type="button" onclick="enviar(\''+myArr[i].idUsuario+'\')" class="btn btn-success" style="display:none;"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Assign</button>'
							+ '<button id="b_del'+myArr[i].idUsuario+'" type="button" onclick="ocultar(\''+myArr[i].idUsuario+'\')" class="btn btn-danger" style="display:none;"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Close</button>'
							+ '<button id="b_show'+myArr[i].idUsuario+'" type="button" onclick="show(\''+myArr[i].idUsuario+'\')" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit</button>'
						
						+	 '</td></tr>';
					}
                }

				out += '</tbody>'
						+'</table>';

            }
            else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No users found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
    urlPath = "API/usuarioperm/" + document.getElementById("searchBarInput").value;
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

function enviar(no) {
	var user=document.getElementById("selUser"+no).value;
	var control=document.getElementById("opcion").value;
	
	var a=0;
	var checkedValue = new Array();
	var inputElements = document.getElementsByName('perm'+no+'[]');
	var len = inputElements.length;
	for (var i=0; i<len; i++) {
		if(inputElements[i].checked){
			checkedValue[a] = inputElements[i].value;
			a++;
		}
	}
	
	var permisos = checkedValue.join(','); //juntamos todos los valores en una cadena
	if(permisos == null || permisos == "") permisos = "a";
	
	params = "/"+user+"/"+permisos;
	
	var xmlhttp;
	xmlhttp = new XMLHttpRequest();
	xmlhttp.open("PUT", "API/usuarioperm"+params, true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
		if(xmlhttp.readyState == 4 && http.status == 200) {
			sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
		}
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == XMLHttpRequest.DONE) {
			console.log(xmlhttp.responseText);
			swal("Changes Saved", "Permissions saved successfully.", "success"); 
			showList(); 
		}
	}
	xmlhttp.send(params); 
	showList();  
	showList(); 
};

showList();
searchBar = document.getElementById("searchBarInput");
searchBar.addEventListener('input', showList);