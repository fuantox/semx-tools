function getTools(){
	xmlhttp2 = new XMLHttpRequest();
	
	urlPath = "API/herrabc/1";
	console.log(urlPath);
	xmlhttp2.open("get", urlPath, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONS PARTS: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showList();
};

function showList(){
    console.log("Entro");
	taskBody = document.getElementById("herramientas");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log("Entro");
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
			if(myArr.length!=0){
				
				out += '<table class="table" id="tabla">'
				     + '<thead style="border-top:10px; background:black; color:white;">'
						 + '<tr  class="thead-inverse">'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">Description</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-1">Time of Life</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-1">Price</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-1">Binloc</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">SEMX code</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">SHMM code</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-4">Options</th>'
						 + '</tr>'
					 + '</thead>'
				 + '<tbody>'; 
						
				for(i = 0; i < myArr.length; i++) { //Ciclo para mostrar todos los datos de la base de datos de Herramientas
					out += '<td id="desc'+myArr[i].numHerramienta+'" name="desc'+myArr[i].numHerramienta+'">'+myArr[i].descripcion+'</td>'
					+ '<td id="time'+myArr[i].numHerramienta+'" name="time'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].tiempoVida+'</td>'
					+ '<td id="price'+myArr[i].numHerramienta+'" name="price'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].precio+'</td>'
					+ '<td id="binloc'+myArr[i].numHerramienta+'" name="binloc'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].binloc+'</td>'
					+ '<td id="semx'+myArr[i].numHerramienta+'" name="semx'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].semxcode+'</td>'
					+ '<td id="shmm'+myArr[i].numHerramienta+'" name="shmm'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].shmmcode+'</td>'
					
					+ '<td style="text-align:center;">'
					  + '<button type="button" id="edit_button'+myArr[i].numHerramienta+'" class="btn btn-default" onclick="edit('+myArr[i].numHerramienta+')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit</button>'
					  + '<button type="button" id="done_button'+myArr[i].numHerramienta+'" class="btn btn-success" onclick="save('+myArr[i].numHerramienta+')" style="display:none;"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Done</button>'
					  + '<button type="button" id="delete_button'+myArr[i].numHerramienta+'" class="btn btn-danger" onclick="del('+myArr[i].numHerramienta+')"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button>'
					+ '</td></tr>';
				}
				out +=  '</tbody>'
				+ '</table>';
            }
			else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Tools found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
	urlPath = "API/herrabc/1";
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

function add(){ //Validar una alta a herramienta
	var desc=document.getElementById("descs").value;
	var time=document.getElementById("times").value;
	var price=document.getElementById("prices").value;
	var binloc=document.getElementById("binlocs").value;
	var semx=document.getElementById("semxs").value;
	var shmm=document.getElementById("shmms").value;
	
	if(desc!="" && time!="" && price!=""){ //Verificar que los campos no estén vacíos
		if(isNumber(time) == true){ //verificar que el tiempo de vida sea un número
			if(isNumber(price) == true){ //verificar que el precio sea un número
				params = "desc="+desc+"&time="+time+"&price="+price+"&binloc="+binloc
						+"&semx="+semx+"&shmm="+shmm;
						
				var xmlhttp;
				xmlhttp = new XMLHttpRequest();
				xmlhttp.open("POST", "API/herrabc/herrabc.php", true);
				xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
					if(xmlhttp.readyState == 4 && http.status == 200) {
						sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
					}
				}
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == XMLHttpRequest.DONE) {
						console.log(xmlhttp.responseText);
						swal("Successfully added", "Tool registered successfully.", "success"); 
						document.getElementById("descs").value="";
						document.getElementById("times").value="";
						document.getElementById("prices").value="";
						document.getElementById("binlocs").value="";
						document.getElementById("semxs").value="";
						document.getElementById("shmms").value="";
						ocultar();
						showList();  
					}
				}
				xmlhttp.send(params); 
				showList();  
				showList();
			}
			else{
				swal("Error", "Price must be a number", "error"); 
			}
		}
		else{
			swal("Error", "Time of life must be a number", "error"); 
		}
	}
	else{
		swal("Error", "There are blank fields", "error"); 
	}
	showList();
};

function edit(no){ //Editar los campos de la tabla
	document.getElementById("done_button"+no).style.display = 'block'; //Mostramos el botón "Done"
	document.getElementById("edit_button"+no).style.display = 'none'; //Ocultamos el botón "Edit"
	document.getElementById("delete_button"+no).style.display = 'none'; //Ocultamos el botón "Delete"
		
	var desc=document.getElementById("desc"+no);
	var time=document.getElementById("time"+no);
	var price=document.getElementById("price"+no);
	var binloc=document.getElementById("binloc"+no);
	var semx=document.getElementById("semx"+no);
	var shmm=document.getElementById("shmm"+no);
		
	var desc_data=desc.innerHTML;
	var time_data=time.innerHTML;
	var price_data=price.innerHTML;
	var binloc_data=binloc.innerHTML;
	var semx_data=semx.innerHTML;
	var shmm_data=shmm.innerHTML;
		
	desc.innerHTML="<input type='text' id='desc_text"+no+"' class='form-control' value='"+desc_data+"'>";
	time.innerHTML="<input type='text' id='time_text"+no+"' class='form-control' value='"+time_data+"'>";
	price.innerHTML="<input type='text' id='price_text"+no+"' class='form-control' value='"+price_data+"'>";
	binloc.innerHTML="<input type='text' id='binloc_text"+no+"' class='form-control' value='"+binloc_data+"'>";
	semx.innerHTML="<input type='text' id='semx_text"+no+"' class='form-control' value='"+semx_data+"'>";
	shmm.innerHTML="<input type='text' id='shmm_text"+no+"' class='form-control' value='"+shmm_data+"'>";
};

function save(no){  //Guardar los datos de la tabla que fueron modificados con el método edit() y validar para realizar un cambio
	var desc=document.getElementById("desc_text"+no).value;
	var time=document.getElementById("time_text"+no).value;
	var price=document.getElementById("price_text"+no).value;
	var binloc=document.getElementById("binloc_text"+no).value;
	var semx=document.getElementById("semx_text"+no).value;
	var shmm=document.getElementById("shmm_text"+no).value;
	
	if(desc!="" && time!="" && price!="" && binloc!="" && semx!="" && shmm!=""){ //Verficar que los campos NO estén vacíos
		if(isNumber(time) == true){ //verificar que el tiempo de vida sea un número
			if(isNumber(price) == true){ //verificar que el precio sea un número
				document.getElementById("desc"+no).innerHTML=desc;
				document.getElementById("time"+no).innerHTML=time;
				document.getElementById("price"+no).innerHTML=price;
				document.getElementById("binloc"+no).innerHTML=binloc;
				document.getElementById("semx"+no).innerHTML=semx;
				document.getElementById("shmm"+no).innerHTML=shmm;

				document.getElementById("edit_button"+no).disabled = false; //habilitar el boton
				
				params = "/"+no+"/"+desc+"/"+time+"/"+price+"/"+binloc+"/"+semx+"/"+shmm;
			
				var xmlhttp;
				xmlhttp = new XMLHttpRequest();
				xmlhttp.open("PUT", "API/herrabc"+params, true);
				xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
					if(xmlhttp.readyState == 4 && http.status == 200) {
						sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
					}
				}
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == XMLHttpRequest.DONE) {
						console.log(xmlhttp.responseText);
						sweetAlert("Changes Saved", "Tool saved successfully.", "success"); 
						showList(); 
					}
				}
				xmlhttp.send(params); 
				showList();  
				showList(); 
			}
			else{
				swal("Error", "Price must be a number", "error");
			}
		}
		else{
			swal("Error", "Time of life must be a number", "error");
		}
	}
	else{
		swal("Error", "There are blank fields", "error");
	}
};

function del(no){ //Función para realizar una baja
	swal({   
        title: "Are you sure?",   
        text: "Tool data will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/herrabc/"+no, true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                if(JSON.parse(xmlhttp.responseText).affected_rows != 1){
                    //swal("Error", "Employee "+id+" could not be deleted. \n Error 0x0001: Existent consumption registries.", "error"); 
                }
                else{
                    swal("Deleted!", "Tool has been deleted.", "success"); 
					showList();
					//alert("Successfully deleted");
                }
            }
        }
        xmlhttp.send();        
        showList();
    });
    showList();	
};

function isNumber(n){ //Función para validar que una cadena sea un número
  return !isNaN(parseFloat(n)) && isFinite(n);
};

getTools();