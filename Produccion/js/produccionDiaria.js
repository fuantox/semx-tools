function getOperations(){
	var x = document.getElementById("selPart");
	var part = x.options[x.selectedIndex].value;
	var parte = part;
	xmlhttp2 = new XMLHttpRequest();
	
	var fecha = "";
	if(document.getElementById("startd").value!="" && document.getElementById("startd").value!=null){
		fecha = document.getElementById("startd").value;
	}
	
	urlPath = "API/prodDiaria/" + parte;
	console.log(urlPath);
	xmlhttp2.open("get", urlPath+"?fecha="+fecha, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONS getOp: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showList();
};

function showList(){
    console.log("Entro");
	taskBody = document.getElementById("allop");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log("Entro");
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
			if (typeof myArr.length != "undefined" && myArr.length != null && myArr.length!=0){
            //if(myArr.length!=0){
				console.log('JSONSssss: ' + xmlhttp.responseText);
				out += '<div class="col-md-2"><label>Lines: </label></div><br><br>'
					+ '<table>'
					+ '<thead style="border-top:10px; background:black; color:white;">'
					+	'<tr  class="thead-inverse">'
					+	  '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2"># Line</th>'
					+	  '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2" colspan="2">Section 1</th>'
					+	  '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-3" colspan="2">Section 2</th>'
					+	  '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2" colspan="2">Section 3</th>'
					+	'</tr>'
					+  '</thead>'
					+  '<tbody>'
				for(i = 0; i < myArr.length; i++) {
					out += '<tr>'
					+  	'<td id="line'+i+'" name="'+myArr[i].idLinea+'" style="text-align:center;">'+myArr[i].nombre+'</td>';
					if(myArr[i].ops1!=null){
						out +=	'<td id="sec1'+myArr[i].idLinea+'" style="text-align:center;width:7%;">'+myArr[i].ops1+'</td>'
						+ '<td><input style="text-align:center;width:60px;" type="number" id="cant1'+i+'" name="cant1'+(i+1)+'" value="0" min="0" max="100000"> &nbsp;pieces </td>';
					}
					else if(myArr[i].ops1==null){
						out += '<td id="sec1'+myArr[i].idLinea+'" style="text-align:center;width:7%;"></td>'
						+ '<td><input style="text-align:center;width:60px;" type="hidden" id="cant1'+i+'" name="cant1'+(i+1)+'" value="-1">   </td>';
					}
					
					if(myArr[i].ops2!=null){
						out += '<td id="sec2'+myArr[i].idLinea+'" style="text-align:center;width:15%;">'+myArr[i].ops2+'</td>'
						+ '<td><input style="text-align:center;width:60px;" type="number" id="cant2'+i+'" name="cant2'+(i+1)+'" value="0" min="0" max="100000">  pieces </td>';
					}
					else if(myArr[i].ops2==null){
						out +=	'<td id="sec2'+myArr[i].idLinea+'" style="text-align:center;width:7%;"></td>'
						+ '<td><input style="text-align:center;width:60px;" type="hidden" id="cant2'+i+'" name="cant2'+(i+1)+'" value="-1">   </td>';
					}
					
					if(myArr[i].ops3!=null){
						out +=	'<td id="sec3'+myArr[i].idLinea+'" style="text-align:center;width:7%;">'+myArr[i].ops3+'</td>'
						+ '<td><input style="text-align:center;width:60px;" type="number" id="cant3'+i+'" name="cant3'+(i+1)+'" value="0" min="0" max="100000">  pieces </td>';
					}
					else if(myArr[i].ops3==null){
						out +=	'<td id="sec3'+myArr[i].idLinea+'" style="text-align:center;width:7%;"></td>'
						+ '<td><input style="text-align:center;width:60px;" type="hidden" id="cant3'+i+'" name="cant3'+(i+1)+'" value="-1">   </td>';
					}
					out += '</tr>';
                }
				out += '</tbody></table>';
				out += '<input type="hidden" id="total" name="total" value='+i+' >';
            }
			else if (typeof myArr.result != "undefined" && myArr.result != null){
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">Production for this date has been already registered.</h1>'
				+       '<h1 class="text-center">If changes are required, please go to <a id="operaciones" href="#" onclick="goToEdit();">edit</a>.</h1>'
                +     '</div>';   
            }
            else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Operations found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
	var x = document.getElementById("selPart");
	var part = x.options[x.selectedIndex].value;
	var parte = part;
	
	var fecha = "";
	if(document.getElementById("startd").value!="" && document.getElementById("startd").value!=null){
		fecha = document.getElementById("startd").value;
	}
	
	urlPath = "API/prodDiaria/" + parte;
    xmlhttp.open("get", urlPath+"?fecha="+fecha, true);
    xmlhttp.send();
};

function dailyRegister() { 
	var a = document.getElementById("selPart");
	var part = a.options[a.selectedIndex].value;
	
	var cant1 = new Array();  //cantidades para la produccion de la seccion1 seleccionadas
	var cant2 = new Array();  //cantidades para la produccion de la seccion1 seleccionadas
	var cant3 = new Array();  //cantidades para la produccion de la seccion1 seleccionadas
	
	var linea = new Array();  //cantidades de operaciones  de la aprte seleccionada
	
	var band1 = 0;
	var band2 = 0;
	var pos = 0;
	
	var fecha = document.getElementById("startd").value;
	if(fecha == "" || fecha == null){
		sweetAlert("Error", "Select a day", "error");
	}
	else {
		total = document.getElementById("total").value; //Total de operaciones para cada parte
		for(i=0;i<total; i++){
			band1 = band1 + 1;
			cant1[i] = document.getElementById("cant1" + i).value;
			cant2[i] = document.getElementById("cant2" + i).value;
			cant3[i] = document.getElementById("cant3" + i).value;
			linea[i] = document.getElementById("line" + i).getAttribute("name");
		}
		
		var cants1 = cant1.join(','); //juntamos todos los valores en una cadena
		var cants2 = cant2.join(','); //juntamos todos los valores en una cadena
		var cants3 = cant3.join(','); //juntamos todos los valores en una cadena
		var lineas = linea.join(','); //juntamos todos los valores en una cadena
		
		params = "fecha="+fecha+"&numpart="+part+"&cants1="+cants1+"&cants2="+cants2
				+"&cants3="+cants3+"&lineas="+lineas;
				console.log(params);
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", "API/prodDiaria/prodDiaria.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlhttp.readyState == 4 && http.status == 200) {
				sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
			}
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				console.log(xmlhttp.responseText);
				swal("Successfully added", "Production registered successfully.", "success"); 
				showList();  
			}
		}
		xmlhttp.send(params); 
		showList();  
		showList();
	}
};