function viewTools(){
	var x1 = document.getElementById("selLine1");
	var l = x1.options[x1.selectedIndex].value;
	var line = l;
	
	var x = document.getElementById("selOp1");
	var o = x.options[x.selectedIndex].value;
	var op = o;
	
	var x2 = document.getElementById("selPart1");
	var part = x2.options[x2.selectedIndex].value;
	var parte = part;
	
	var fecha = document.getElementById("startd1").value;
	
	xmlhttp2 = new XMLHttpRequest();
	
	urlPath = "API/viewHerr/"+op;
	console.log(urlPath+"?idpart="+parte+"&idline="+line+"&fecha="+fecha);
	xmlhttp2.open("get", urlPath+"?idpart="+parte+"&idline="+line+"&fecha="+fecha, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONSHERR: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showEdit();
};

function showEdit(){
	taskBody2 = document.getElementById("alltool1");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
		//if (this.readyState == XMLHttpRequest.DONE) {		
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
            if(myArr.length!=0){
				console.log('JSON SHOW LIST 2: ' + xmlhttp.responseText);
				out = '<div class="col-md-2"><label>Tools: </label></div>'
					+ '<form id="selTool" action="">'
					+ '<table>';
				for(i = 0; i < myArr.length; i++) {
					out += '<tr>'
					+ '<td><b>'+myArr[i].descripcion+':&nbsp;&nbsp;</b></td>'
					+ '<td><input type="hidden" id="hname'+myArr[i].numHerramienta+'" value="'+myArr[i].descripcion+'"></td>'
					+ '<td id="cant'+myArr[i].idCambio+'">'+myArr[i].cant+' pieces.</td>'
					+ '<td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reason:&nbsp;</b></td>';
					if(myArr[i].razon == 0) out += '<td id="reason'+myArr[i].idCambio+'">No reasons.</td>';
					if(myArr[i].razon == 1) out += '<td id="reason'+myArr[i].idCambio+'">End of useful life.</td>';
					if(myArr[i].razon == 2) out += '<td id="reason'+myArr[i].idCambio+'">Broken tool.</td>';
					if(myArr[i].razon == 3) out += '<td id="reason'+myArr[i].idCambio+'">Quality defect.</td>';
					if(myArr[i].razon == 4) out += '<td id="reason'+myArr[i].idCambio+'">Change of model.</td>';
					if(myArr[i].razon == 5) out += '<td id="reason'+myArr[i].idCambio+'">Engineering test.</td>';
					if(myArr[i].razon == 6) out += '<td id="reason'+myArr[i].idCambio+'">Cleaning.</td>';
					out += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="edit_button'+myArr[i].idCambio+'" class="btn btn-default" onclick="editChange('+myArr[i].idCambio+')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit</button>'
					     + '<button type="button" id="done_button'+myArr[i].idCambio+'" style="display: none;" class="btn btn-success" onclick="saveChange('+myArr[i].idCambio+')"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Done</button>'
						 + '<button type="button" id="delete_button'+myArr[i].idCambio+'" class="btn btn-danger" onclick="deleteChange('+myArr[i].idCambio+')"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button></td>';
					
                }
				out += '</table>'
					+ '</form>'
					+ '<input type="hidden" id="total" name="total" value='+i+' >';
            }
            else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Tools found.</h1>'
                +     '</div>';   
            }
            taskBody2.innerHTML = out;
        }
    };
	
	var x1 = document.getElementById("selLine1");
	var l = x1.options[x1.selectedIndex].value;
	var line = l;
	
	var x = document.getElementById("selOp1");
	var o = x.options[x.selectedIndex].value;
	var op = o;
	
	var x2 = document.getElementById("selPart1");
	var part = x2.options[x2.selectedIndex].value;
	var parte = part;
	
	var fecha = document.getElementById("startd1").value;
	
	urlPath = "API/viewHerr/" + op;
    xmlhttp.open("get", urlPath+"?idpart="+parte+"&idline="+line+"&fecha="+fecha, true);
    xmlhttp.send();
};

function deleteChange(id){
	swal({   
        title: "Are you sure?",   
        text: "Changes data will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/viewHerr/"+id, true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                if(JSON.parse(xmlhttp.responseText).affected_rows != 1){
                    //swal("Error", "Employee "+id+" could not be deleted. \n Error 0x0001: Existent consumption registries.", "error"); 
                }
                else{
                    swal("Deleted!", "Change has been deleted.", "success"); 
					//alert("Successfully deleted");
                }
            }
        }
        xmlhttp.send();        
        showEdit();
    });
    showEdit();
};

function editChange(no){
	document.getElementById("edit_button"+no).style.display='none';
	document.getElementById("done_button"+no).style.display='block';
	document.getElementById("delete_button"+no).style.display='none';

	var cant=document.getElementById("cant"+no);
	var reason=document.getElementById("reason"+no);

	var cant_data=cant.innerHTML;
	var reason_data=reason.innerHTML;

	cant_datas = cant_data.split(" ");

	cant.innerHTML="<input type='number' id='cant_text"+no+"' value='"+cant_datas[0]+"' min='0' max='10000'>";
	reason.innerHTML="<td><select id='razon_text"+no+"' class='selectpicker'>"
				+ "<option >Select an option...</option>"
				+ "<option>End of useful life.</option>"
				+ "<option>Broken tool.</option>"
				+ "<option>Quality defect.</option>"
				+ "<option>Change of model.</option>"
				+ "<option>Engineering test.</option>"
				+ "<option>Cleaning.</option>"
			+ "</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
};

function saveChange(no){
	var cant=document.getElementById("cant_text"+no).value;
	var x = document.getElementById("razon_text"+no);
	var reason = x.options[x.selectedIndex].value;
	
	if(cant!=""){
		if(reason!="Select an option..."){
			document.getElementById("cant"+no).innerHTML=cant;
			document.getElementById("reason"+no).innerHTML=reason;
			razon = 0;
			
			switch(reason){
				case 'End of useful life.': razon = 1; break;
				case 'Broken tool.': razon = 2; break;
				case 'Quality defect.': razon = 3; break;
				case 'Change of model.': razon = 4; break;
				case 'Engineering test.': razon = 5; break;
				case 'Cleaning.': razon = 6; break;
				default: razon = 0; break;
			}

			params = "/"+no+"/"+cant+"/"+razon;
			
			var xmlhttp;
            xmlhttp = new XMLHttpRequest();
            xmlhttp.open("PUT", "API/viewHerr"+params, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
                if(xmlhttp.readyState == 4 && http.status == 200) {
                    sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
                }
            }
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                    console.log(xmlhttp.responseText);
                    swal("Changes Saved", "Registry saved successfully.", "success"); 
                    $('#myModal').modal('toggle');
                    showEdit();  
                }
            }
            xmlhttp.send(params); 
            showEdit();  
            showEdit(); 
		}
		else{
			sweetAlert("Error", "Select an option", "error");
		}
	}
	else{
		sweetAlert("Error", "There are blank fields", "error");
	}
};