function getOperations(){
	idpart = document.getElementById("parteid").value;
	
	xmlhttp2 = new XMLHttpRequest();
	urlPath = "API/operation/"+idpart;
	console.log(urlPath);
	xmlhttp2.open("get", urlPath, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONS OPERATIONS: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showList();
};

function showList(){
    console.log("Entro");
	taskBody = document.getElementById("operaciones");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log("Entro");
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
			if(myArr.length!=0){
				
				out += '<table class="table" id="tabla">'
				     + '<thead style="border-top:10px; background:black; color:white;">'
						 + '<tr  class="thead-inverse">'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-6"># Operation</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">Options</th>'
						 + '</tr>'
					 + '</thead>'
				 + '<tbody>'; 
						
				for(i = 0; i < myArr.length; i++) { //Ciclo para mostrar todos los datos de la base de datos de Herramientas
					out += '<td id="desc'+myArr[i].numOperacion+'" style="text-align:center;"> <a href="Herramienta.php?numop='+myArr[i].numOperacion+'&desc='+myArr[i].descripcion+'&nump='+myArr[i].numparte+'&namep='+myArr[i].nombre+'">'+myArr[i].descripcion+' <input type="hidden" id="nameos'+myArr[i].numOperacion+'" value="'+myArr[i].descripcion+'"> </a></td>'
					
					+ '<td style="text-align:center;">'
					  + '<button type="button" id="edit_button'+myArr[i].numOperacion+'" class="btn btn-default" onclick="edit('+myArr[i].numOperacion+')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit</button>'
					  + '<button type="button" id="done_button'+myArr[i].numOperacion+'" class="btn btn-success" onclick="save('+myArr[i].numOperacion+')" style="display:none;"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Done</button>'
					  + '<button type="button" id="delete_button'+myArr[i].numOperacion+'" class="btn btn-danger" onclick="del('+myArr[i].numOperacion+', '+myArr[i].numparte+')"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button>'
					+ '</td></tr>';
				}
				out +=  '</tbody>'
				+ '</table>';
            }
			else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Operations found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
	idpart = document.getElementById("parteid").value;
	urlPath = "API/operation/"+idpart;
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

function add(numpart){ //Validar una alta a herramienta
	var desc=document.getElementById("descs").value;
	if(desc!=""){ //Verificar que no exista ningún campo en blanco
		params = "desc="+desc+"&numpart="+numpart;
		
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", "API/operation/operation.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlhttp.readyState == 4 && http.status == 200) {
				sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
			}
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				console.log(xmlhttp.responseText);
				swal("Successfully added", "Operation registered successfully.", "success"); 
				document.getElementById("descs").value="";
				ocultar();
				showList();  
			}
		}
		xmlhttp.send(params); 
		showList();  
		showList();
	}
	else{
		sweetAlert("Error", "There are blank fields", "error");
	}
};

function edit(no){
	document.getElementById("done_button"+no).style.display = 'block'; //Mostramos el botón "Done"
	document.getElementById("edit_button"+no).style.display = 'none'; //Ocultamos el botón "Edit"
	document.getElementById("delete_button"+no).style.display = 'none'; //Ocultamos el botón "Delete"
	
	var nameos = document.getElementById("nameos"+no).value;
	var desc=document.getElementById("desc"+no);
		
	var desc_data=desc.innerHTML;
		
	desc.innerHTML="<input type='text' id='desc_text"+no+"' class='form-control' value='"+nameos+"'>";
}

function save(no) {
	var desc=document.getElementById("desc_text"+no).value;

	if(desc!=""){ //Verificar que no existan campos en blanco
		document.getElementById("desc"+no).innerHTML=desc;

		document.getElementById("edit_button"+no).disabled = false; //habilitar el boton
		
		params = "/"+no+"/"+desc;
			
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		xmlhttp.open("PUT", "API/operation"+params, true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlhttp.readyState == 4 && http.status == 200) {
				sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
			}
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				console.log(xmlhttp.responseText);
				swal("Changes Saved", "Operation saved successfully.", "success"); 
				showList(); 
			}
		}
		xmlhttp.send(params); 
		showList();  
		showList(); 
	}
	else{
		sweetAlert("Error", "There are blank fields", "error");
	}
}

function del(no){ //Función para realizar una baja
	swal({   
        title: "Are you sure?",   
        text: "Operation data will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
		var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/operation/"+no, true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                if(JSON.parse(xmlhttp.responseText).affected_rows != 1){
                    //swal("Error", "Employee "+id+" could not be deleted. \n Error 0x0001: Existent consumption registries.", "error"); 
                }
                else{
                    swal("Deleted!", "Operation has been deleted.", "success"); 
					showList();
					//alert("Successfully deleted");
                }
            }
        }
        xmlhttp.send();        
        showList();
    });
    showList();	
};

function isNumber(n){ //Función para validar que una cadena sea un número
  return !isNaN(parseFloat(n)) && isFinite(n);
};

getOperations();