function getTools(){
	idop = document.getElementById("opid").value;
	
	xmlhttp2 = new XMLHttpRequest();
	urlPath = "API/herr/"+idop;
	console.log(urlPath);
	xmlhttp2.open("get", urlPath, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONS TOOL: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showList();
};

function showList(){
    console.log("Entro");
	taskBody = document.getElementById("herramientas");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log("Entro");
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
			if(myArr.length!=0){	
				out += '<table class="table" id="tabla">'
				     + '<thead style="border-top:10px; background:black; color:white;">'
						 + '<tr  class="thead-inverse">'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">Description</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-1">Time of Life</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-1">Price</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-1">Binloc</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">SEMX code</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">SHMM code</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-4">Options</th>'
						 + '</tr>'
					 + '</thead>'
				 + '<tbody>'; 
						
				for(i = 0; i < myArr.length; i++) { //Ciclo para mostrar todos los datos de la base de datos de Herramientas
					out += '<input type="hidden" id="herrs'+myArr[i].numHerramienta+'" value="'+myArr[i].numHerramienta+'">'
					+ '<td id="desc'+myArr[i].numHerramienta+'" name="desc'+myArr[i].numHerramienta+'">'+myArr[i].descripcion+'</td>'
					+ '<td id="time'+myArr[i].numHerramienta+'" name="time'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].tiempoVida+'</td>'
					+ '<td id="price'+myArr[i].numHerramienta+'" name="price'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].precio+'</td>'
					+ '<td id="binloc'+myArr[i].numHerramienta+'" name="binloc'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].binloc+'</td>'
					+ '<td id="semx'+myArr[i].numHerramienta+'" name="semx'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].semxcode+'</td>'
					+ '<td id="shmm'+myArr[i].numHerramienta+'" name="shmm'+myArr[i].numHerramienta+'" style="text-align:center;">'+myArr[i].shmmcode+'</td>'
					
					+ '<td style="text-align:center;">'
					  + '<button type="button" id="delete_button'+myArr[i].numHerramienta+'" class="btn btn-danger" onclick="del('+myArr[i].numHerramienta+')"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button>'
					+ '</td></tr>';
				}
				out +=  '</tbody>'
				+ '</table>';
            }
			else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Tools found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
	idop = document.getElementById("opid").value;
	urlPath = "API/herr/"+idop;
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

function asignar(idop){ //Validar una alta a herramienta
	var a = document.getElementById("seleccion");
	var seleccion = a.options[a.selectedIndex].value;
	if(seleccion == "Select an option..."){ //Si seleccionó la opción: "Select an option".
		swal("Error", "Select a Tool", "error"); 
	}
	else{
		params = "idh="+seleccion+"&idop="+idop;
		
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", "API/herr/herr.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlhttp.readyState == 4 && http.status == 200) {
				sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
			}
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				console.log("RESULTADO"+xmlhttp.responseText);
				
				myArr = JSON.parse(xmlhttp.responseText);
				if(myArr.result == "success"){
					swal("Successfully assigned", "Tool assigned successfully", "success"); 
					document.getElementById("seleccion").value= "Select an option...";
					ocultar();
				}
				if(myArr.result == "error"){
					swal("Error", "This tool has already been assigned", "error"); 
				}
				showList();  
			}
		}
		xmlhttp.send(params); 
		showList();  
		showList();
	}
	showList();
};

function del(numtool){ //Función para realizar una baja
	numop = document.getElementById("opid").value;
	swal({   
        title: "Are you sure?",   
        text: "This tool will be deleted from this operation.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
		var xmlhttp;
        xmlhttp = new XMLHttpRequest();
		params = numop+"/"+numtool
		xmlhttp.open("DELETE", "API/herr/"+params, true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                if(JSON.parse(xmlhttp.responseText).affected_rows != 1){
                    swal("Error", "Tool could not be deleted. \n Error 0x0001: Existent consumption registries.", "error"); 
                }
                else if(JSON.parse(xmlhttp.responseText).affected_rows == 1){
                    swal("Deleted!", "Tool has been deleted.", "success"); 
					showList();
                }
            }
        }
        xmlhttp.send();        
        showList();
    });
    showList();	
};

getTools();