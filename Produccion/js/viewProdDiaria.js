function getOperations1(){
	var x = document.getElementById("selPart1");
	var part = x.options[x.selectedIndex].value;
	var parte = part;
	
	var fecha = document.getElementById("startd1").value;
	
	xmlhttp2 = new XMLHttpRequest();
	
	urlPath = "API/viewProdDiaria/" + parte;
	console.log(urlPath);
	xmlhttp2.open("get", urlPath+"?fecha="+fecha, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONS: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showListProd();
};

function showListProd(){
    taskBody = document.getElementById("allop1");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
			if(myArr.length!=0){
				console.log('JSONSssss: ' + xmlhttp.responseText);
				out += '<div class="col-md-2"><label>Lines: </label></div><br><br>'
					+ '<table>'
					+ '<thead style="border-top:10px; background:black; color:white;">'
					+	'<tr  class="thead-inverse">'
					+	  '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2"># Line</th>'
					+	  '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2" colspan="2">Section 1</th>'
					+	  '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-3" colspan="2">Section 2</th>'
					+	  '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2" colspan="2">Section 3</th>'
					+	'</tr>'
					+  '</thead>'
					+  '<tbody>';
					
				var seccion = 0;	
				var inc = 0;
				var idLines = new Array();
				var secciones = new Array();
				var idProds = new Array();
				var aux=0;
				
				for(i = 0; i < myArr.length; ) {
					out += '<tr>'
					+  	'<td id="linea'+i+'" name="l'+myArr[i].idLinea+'" style="text-align:center;">'+myArr[i].nombre+'</td>';
					
					var idLine = myArr[i].idLinea;
					while(i < myArr.length && myArr[i].idLinea == idLine){
						seccion = 0;
						ops1 = "";
						while(i<myArr.length && myArr[i].idLinea == idLine && myArr[i].seccion==1){
							if(ops1=="") {ops1 = myArr[i].descripcion;}
							else if(ops1!="") {ops1 = ops1+" - "+myArr[i].descripcion;}
							idLines[aux] = myArr[i].idLinea;
							secciones[aux] = myArr[i].seccion;
							idProds[aux] = myArr[i].idProd; aux++;
							i++;
							if(i==myArr.length || myArr[i].idLinea != idLine || myArr[i].seccion!=1){
								out +=	'<td id="secs1'+myArr[i-1].idLinea+'" style="text-align:center;width:12%;">'+ops1+'</td>'
									+ '<td id="cants1'+inc+'" name="cants1'+(inc+1)+'">'+myArr[i-1].cantPiezas+' pieces.</td>';
									seccion = 1;
									inc++;
							}
						}
							
						ops2 = "";
						while(i<myArr.length && myArr[i].idLinea == idLine && myArr[i].seccion==2){
							if(ops2=="") {ops2 = myArr[i].descripcion;}
							else if(ops2!="") {ops2 = ops2+" - "+myArr[i].descripcion;}
							idLines[aux] = myArr[i].idLinea; 
							secciones[aux] = myArr[i].seccion;
							idProds[aux] = myArr[i].idProd; aux++;
							i++;
							if(i==myArr.length || myArr[i].idLinea != idLine || myArr[i].seccion!=2){
								if(seccion==1){
									out +=	'<td id="secs2'+myArr[i-1].idLinea+'" style="text-align:center;width:20%;">'+ops2+'</td>'
									+ '<td id="cants2'+inc+'" name="cants2'+(inc+1)+'">'+myArr[i-1].cantPiezas+' pieces.</td>';
									inc++;
								}
								else if(seccion==0){
									out +=	'<td id="secs1'+myArr[i-2].idLinea+'" style="text-align:center;width:12%;"></td>'
									+ '<td id="cants1'+inc+'" name="cants1'+(inc+1)+'"></td>'
									+ '<td id="secs2'+myArr[i-1].idLinea+'" style="text-align:center;width:20%;">'+ops2+'</td>'
									+ '<td id="cants2'+(inc+1)+'" name="cants2'+(inc+2)+'">'+myArr[i-1].cantPiezas+' pieces.</td>';
									inc++;inc++;
								}
								seccion = 2;
							}
						}
							
						ops3 = "";
						while(i<myArr.length && myArr[i].idLinea == idLine && myArr[i].seccion==3){
							if(ops3=="") {ops3 = myArr[i].descripcion;}
							else if(ops3!="") {ops3 = ops3+" - "+myArr[i].descripcion;}
							idLines[aux] = myArr[i].idLinea;
							secciones[aux] = myArr[i].seccion;
							idProds[aux] = myArr[i].idProd; aux++;
							i++;
							if(i==myArr.length || myArr[i].idLinea != idLine || myArr[i].seccion!=3){
								if(seccion==2){
									out +=	'<td id="secs3'+myArr[i-1].idLinea+'" style="text-align:center;width:12%;">'+ops3+'</td>'
									+ '<td id="cants3'+inc+'" name="cants3'+(inc+1)+'">'+myArr[i-1].cantPiezas+' pieces.</td>';
									inc++;
								}
								else if(seccion==1){
									out +=	'<td id="secs2'+myArr[i-2].idLinea+'" style="text-align:center;width:20%;"></td>'
									+ '<td id="cants2'+inc+'" name="cants2'+(inc+1)+'"></td>'
									+ '<td id="secs3'+myArr[i-1].idLinea+'" style="text-align:center;width:12%;">'+ops3+'</td>'
									+ '<td id="cants3'+(inc+1)+'" name="cants3'+(inc+2)+'">'+myArr[i-1].cantPiezas+' pieces.</td>';
									inc++;inc++;
								}
								else if(seccion==0){
									out +=	'<td id="secs1'+myArr[i-3].idLinea+'" style="text-align:center;width:12%;"></td>'
									+ '<td id="cants1'+inc+'" name="cants1'+(inc+1)+'"></td>'
									+ '<td id="secs2'+myArr[i-2].idLinea+'" style="text-align:center;width:20%;"></td>'
									+ '<td id="cants2'+(inc+1)+'" name="cants2'+(inc+2)+'"></td>'
									+ '<td id="secs3'+myArr[i-1].idLinea+'" style="text-align:center;width:12%;">'+ops3+'</td>'
									+ '<td id="cants3'+(inc+2)+'" name="cants3'+(inc+3)+'">'+myArr[i-1].cantPiezas+' pieces.</td>';
									inc++;inc++;inc++;
								}
								seccion = 3;
							}
						}
						if(seccion == 1){
							out +=	'<td id="secs2'+myArr[i-2].idLinea+'" style="text-align:center;width:20%;"></td>'
								+ '<td id="cants2'+inc+'" name="cants2'+(inc+1)+'"></td>'
								+ '<td id="secs3'+myArr[i-1].idLinea+'" style="text-align:center;width:12%;"></td>'
								+ '<td id="cants3'+(inc+1)+'" name="cants3'+(inc+2)+'"></td>';
							inc++;inc++;
						}
						else if(seccion == 2){
							out +=	'<td id="secs3'+myArr[i-1].idLinea+'" style="text-align:center;width:12%;"></td>'
								+ '<td id="cants3'+inc+'" name="cants3'+(inc+1)+'"></td>';
							inc++;
						}
					}
					out += '</tr>';
                }
				var lines = idLines.join(',');
				var sec = secciones.join(',');
				var ids = idProds.join(',');
				/*alert("LINEAS: "+lines);
				alert("SECCIONES: "+sec);
				alert("IDPROD: "+ids);*/
				out += '</tbody></table>';
				out += '<input type="hidden" id="totals" name="totals" value="'+i+'" />';
				out += '<button type="button" id="edit_buttonP" class="btn btn-default" onclick="editProd('+inc+')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit</button>'
					+ '<button type="button" id="done_buttonP" style="display: none;" class="btn btn-success" onclick="saveProd('+inc+', \''+lines+'\', \''+sec+'\', \''+ids+'\')"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Done</button>'
					+ '<button type="button" id="delete_buttonP" class="btn btn-danger" onclick="deleteProd(\''+ids+'\')"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button>';
            }
            else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Operations found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
	var x = document.getElementById("selPart1");
	var part = x.options[x.selectedIndex].value;
	var parte = part;
	
	var fecha = document.getElementById("startd1").value;
	
	urlPath = "API/viewProdDiaria/" + parte;
    xmlhttp.open("get", urlPath+"?fecha="+fecha, true);
    xmlhttp.send();
};

function deleteProd(idp){
	swal({   
        title: "Are you sure?",   
        text: "Production data will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/viewProdDiaria/"+idp, true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                if(JSON.parse(xmlhttp.responseText).affected_rows != 1){
                    //swal("Error", "Employee "+id+" could not be deleted. \n Error 0x0001: Existent consumption registries.", "error"); 
                }
                else{
                    swal("Deleted!", "Production has been deleted.", "success"); 
					//alert("Successfully deleted");
                }
            }
        }
        xmlhttp.send();        
        showListProd();
    });
    showListProd();
};

function editProd(tam){
	document.getElementById("edit_buttonP").style.display='none';
	document.getElementById("delete_buttonP").style.display='none';
	document.getElementById("done_buttonP").style.display='block';
	
	var cants = new Array();
	var cants_data = new Array();
	
	for(i=0; i<tam; ){
		cants[i] = document.getElementById("cants1"+i); 
		cants_data[i]=cants[i].innerHTML;
		valor1 = cants_data[i].split(" ");
		if(valor1 != "" && valor1 != null){
			cants[i].innerHTML="<input type='number' id='cants_text1"+i+"' value='"+valor1[0]+"' min='0' max='10000'>";
		}
		i++;
		
		cants[i] = document.getElementById("cants2"+i); 
		cants_data[i]=cants[i].innerHTML;
		valor2 = cants_data[i].split(" ");
		if(valor2 != "" && valor2 != null){
			cants[i].innerHTML="<input type='number' id='cants_text2"+i+"' value='"+valor2[0]+"' min='0' max='10000'>";
		}
		i++;				
		
		cants[i] = document.getElementById("cants3"+i); 
		cants_data[i]=cants[i].innerHTML;
		valor3 = cants_data[i].split(" ");
		if(valor3 != "" && valor3 != null){
			cants[i].innerHTML="<input type='number' id='cants_text3"+i+"' value='"+valor3[0]+"' min='0' max='10000'>";	
		}
		i++;
	}
};

function saveProd(tam, idl, sec, idp){ //idl son los id de las Líneas, sec las secciones en donde hay registro, idp los id de produccion
	var cants = new Array();
	var x=0;
	var err=0;
	
	for(i=0; i<tam; ){
		if (typeof document.getElementById("cants_text1"+i) != "undefined" 
		&& document.getElementById("cants_text1"+i) != null){
			if(isNumber(document.getElementById("cants_text1"+i).value) == true
			&& document.getElementById("cants_text1"+i).value>=0){ 
				cants[x] = document.getElementById("cants_text1"+i).value;
				document.getElementById("cants1"+i).innerHTML=cants[x];
				x++;
			}
			else err++;
		}
		i++;
		
		if (typeof document.getElementById("cants_text2"+i) != "undefined" 
		&& document.getElementById("cants_text2"+i) != null){
			if(isNumber(document.getElementById("cants_text2"+i).value) == true
			&& document.getElementById("cants_text2"+i).value>=0){ 
				cants[x] = document.getElementById("cants_text2"+i).value;
				document.getElementById("cants2"+i).innerHTML=cants[x];
				x++;
			}
			else err++;
		}
		i++;
		
		if (typeof document.getElementById("cants_text3"+i) != "undefined" 
		&& document.getElementById("cants_text3"+i) != null){
			if(isNumber(document.getElementById("cants_text3"+i).value) == true
			&& document.getElementById("cants_text3"+i).value>=0){ 
				cants[x] = document.getElementById("cants_text3"+i).value;
				document.getElementById("cants3"+i).innerHTML=cants[x];
				x++;
			}
			else err++;
		}
		i++;
	}
	if(err==0){
		document.getElementById("edit_buttonP").style.display='block';
		document.getElementById("delete_buttonP").style.display='block';
		document.getElementById("done_buttonP").style.display='none';
		var cantsTotal = cants.join(',');
		/*alert(cantsTotal);
		alert(idl);
		alert(sec);
		alert(idp);*/
		params = "/"+cantsTotal+"/"+idl+"/"+idp+"/"+sec;
			
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		xmlhttp.open("PUT", "API/viewProdDiaria"+params, true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlhttp.readyState == 4 && http.status == 200) {
				sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
			}
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				console.log(xmlhttp.responseText);
				swal("Changes Saved", "Registry saved successfully.", "success"); 
				showListProd();
			}
		}
		xmlhttp.send(params); 
		showListProd();  
		showListProd(); 
	}
	else if(err>0){
		sweetAlert("Error", "There are "+err+" errors", "error");
		editProd(tam);
	}
	//showListProd();
};

function isNumber(n){ //Función para validar que una cadena sea un número
  return !isNaN(parseFloat(n)) && isFinite(n);
};

function goToEdit(){ //Mandar a la parte de editar, en caso de que se quiera registrar algo cuando ya exista
	var a = document.getElementById("selPart");
	var part = a.options[a.selectedIndex].value;
	
	var fecha = document.getElementById("startd").value;
	
	document.getElementById('selPart1').value = part;
	document.getElementById('startd1').value = fecha;
	
	document.getElementById("register").className = "";
	document.getElementById("edit").className = "active";
	document.getElementById("alta").className = "tab-pane fade";
	document.getElementById("baja").className = "tab-pane fade in active";
	
	getOperations1();
};