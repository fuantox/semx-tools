function getLines(){
	console.log("ENtro");
	idpart = document.getElementById("parteid").value;
	
	xmlhttp2 = new XMLHttpRequest();
	urlPath = "API/line/"+idpart;
	console.log(urlPath);
	xmlhttp2.open("get", urlPath, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONS LINES: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showList();
};

function showList(){
    console.log("Entro");
	taskBody = document.getElementById("lineas");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log("Entro");
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
			if(myArr.length!=0){
				
				out += '<table class="table" id="tabla">'
				     + '<thead style="border-top:10px; background:black; color:white;">'
						 + '<tr  class="thead-inverse">'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2"># Line</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">Section 1</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-4">Section 2</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">Section 3</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">Options</th>'
						 + '</tr>'
					 + '</thead>'
				 + '<tbody>'; 
						
				for(i = 0; i < myArr.length; i++) { //Ciclo para mostrar todos los datos de la base de datos de Herramientas
					out += '<input type="hidden" id="lines'+myArr[i].idlinea+'" value="'+myArr[i].idlinea+'" />'
					+ '<input type="hidden" id="nameli'+myArr[i].idlinea+'" value="'+myArr[i].nombre+'" />'
					+ '<td id="desc'+myArr[i].idlinea+'" style="text-align:center;">'+myArr[i].nombre+'</td>';
					
					if(myArr[i].ops1!=null)
						out += '<td id="sec1'+myArr[i].idlinea+'" style="text-align:center;">'+myArr[i].ops1+'</td>';
					else
						out += '<td id="sec1'+myArr[i].idlinea+'" style="text-align:center;"></td>';
					
					if(myArr[i].ops2!=null)
						out += '<td id="sec2'+myArr[i].idlinea+'" style="text-align:center;">'+myArr[i].ops2+'</td>';
					else
						out += '<td id="sec2'+myArr[i].idlinea+'" style="text-align:center;"></td>';
				
					if(myArr[i].ops3!=null)
						out += '<td id="sec3'+myArr[i].idlinea+'" style="text-align:center;">'+myArr[i].ops3+'</td>';
					else
						out += '<td id="sec3'+myArr[i].idlinea+'" style="text-align:center;"></td>';
					
					out += '<td style="text-align:center;">'
					  + '<button type="button" id="edit_button'+myArr[i].idlinea+'" class="btn btn-default" onclick="edit('+myArr[i].idlinea+')"><span class="glyphicon glyphicon-pencil"></span>Edit</button>'
					  + '<button type="button" id="done_button'+myArr[i].idlinea+'" class="btn btn-success" onclick="save('+myArr[i].idlinea+')" style="display:none;"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Done</button>'
					  + '<button type="button" id="delete_button'+myArr[i].idlinea+'" class="btn btn-danger" onclick="del('+myArr[i].idlinea+')"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button>'
					+ '</td></tr>';
				}
				out +=  '</tbody>'
				+ '</table>';
            }
			else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Lines found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
	idpart = document.getElementById("parteid").value;
	
	urlPath = "API/line/"+idpart;
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

function add(numpart){ //Validar una alta a herramienta
	var from1 = document.getElementById("from1");
	var f1 = from1.options[from1.selectedIndex].value;
	var to1 = document.getElementById("to1");
	var t1 = to1.options[to1.selectedIndex].value;
	
	var from2 = document.getElementById("from2");
	var f2 = from2.options[from2.selectedIndex].value;
	var to2 = document.getElementById("to2");
	var t2 = to2.options[to2.selectedIndex].value;
	
	var from3 = document.getElementById("from3");
	var f3 = from3.options[from3.selectedIndex].value;
	var to3 = document.getElementById("to3");
	var t3 = to3.options[to3.selectedIndex].value;
	
	var start1=0;
	var start2=0;
	var start3=0;
	var end1=0;
	var end2=0;
	var end3=0;
	
	var params = "";
	
	var desc=document.getElementById("descs").value;
	
	if(desc!=""){ //Verificar que no exista ningún campo en blanco
		params = "numpart="+numpart+"&desc="+encodeURIComponent(desc);
		var aux = 0;
		if(document.getElementById('section1').checked){
			if(f1<t1 || f1==t1){
				start1 = f1;
				end1 = t1;
				params = params + "&start1="+start1+"&end1="+end1;
				aux++;
			}
		}
		if(document.getElementById('section2').checked){
			if(f2<t2 || f2==t2){
				start2 = f2;
				end2 = t2;
				params = params + "&start2="+start2+"&end2="+end2;
				aux++;
			}
		}
		if(document.getElementById('section3').checked){
			if(f3<t3 || f3==t3){
				start3 = f3;
				end3 = t3;
				params = params + "&start3="+start3+"&end3="+end3;
				aux++;
			}
		}
		
		if(f1<=t1 && f2<=t2 && f3<=f3) {
			var xmlhttp;
			xmlhttp = new XMLHttpRequest();
			xmlhttp.open("POST", "API/line/line.php", true);
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlhttp.readyState == 4 && http.status == 200) {
					sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
				}
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == XMLHttpRequest.DONE) {
					console.log(xmlhttp.responseText);
					swal("Successfully added", "Line registered successfully.", "success"); 
					document.getElementById("descs").value="";
					document.getElementById('section1').checked=false; 
					shows(1);
					document.getElementById('section2').checked=false;
					shows(2);
					document.getElementById('section3').checked=false;
					shows(3);
					ocultar();
					showList();  
				}
			}
			xmlhttp.send(params); 
			showList();  
			showList();
		}
		else if(f1>t1 || f2>t2 || f3>f3) {
			sweetAlert("Error", "There are errors in Operations assignment", "error");
		}
	}
	else{
		sweetAlert("Error", "There are blank fields", "error");
	}
};

function edit(no){ //Editar los campos de la tabla
	document.getElementById("done_button"+no).style.display = 'block'; //Mostramos el botón "Done"
	document.getElementById("edit_button"+no).style.display = 'none'; //Ocultamos el botón "Edit"
	document.getElementById("delete_button"+no).style.display = 'none'; //Ocultamos el botón "Delete"
	
	var nameli = document.getElementById("nameli"+no).value;
	
	var desc=document.getElementById("desc"+no);
		
	var desc_data=desc.innerHTML;
	
	desc.innerHTML="<input type='text' id='desc_text"+no+"' class='form-control' value='"+nameli+"'>";
};

function save(no){  //Guardar los datos de la tabla que fueron modificados con el método edit() y validar para realizar un cambio
	var desc=document.getElementById("desc_text"+no).value;
	
	if(desc!=""){ //Verificar que no existan campos en blanco
		document.getElementById("desc"+no).innerHTML=desc;

		document.getElementById("edit_button"+no).disabled = false; //habilitar el boton
		
		params = "/"+no+"/"+encodeURIComponent(desc)+"";
		
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		xmlhttp.open("PUT", "API/line"+params, true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlhttp.readyState == 4 && http.status == 200) {
				sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
			}
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				console.log(xmlhttp.responseText);
				swal("Changes Saved", "Line saved successfully.", "success"); 
				showList(); 
			}
		}
		xmlhttp.send(params); 
		showList();  
		showList(); 
	}
	else{
		sweetAlert("Error", "There are blank fields", "error");
	}
};

function del(no){ //Función para realizar una baja
	swal({   
        title: "Are you sure?",   
        text: "Line data will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/line/"+no, true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                if(JSON.parse(xmlhttp.responseText).affected_rows != 1){
                    //swal("Error", "Employee "+id+" could not be deleted. \n Error 0x0001: Existent consumption registries.", "error"); 
                }
                else{
                    swal("Deleted!", "Line has been deleted.", "success"); 
					showList();
					//alert("Successfully deleted");
                }
            }
        }
        xmlhttp.send();        
        showList();
    });
    showList();	
};

function isNumber(n){ //Función para validar que una cadena sea un número
  return !isNaN(parseFloat(n)) && isFinite(n);
};

getLines();