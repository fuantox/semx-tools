function getUsers(){
	xmlhttp2 = new XMLHttpRequest();
	
	urlPath = "API/user/1";
	console.log(urlPath);
	xmlhttp2.open("get", urlPath, true);
	xmlhttp2.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var myArr = JSON.parse(this.responseText);
			if(myArr.length!=0){
				console.log('JSONS USERS: ' + xmlhttp2.responseText);
				//$('#myModal').modal('toggle');
			}
			else{
				console.log(xmlhttp2.responseText);
			}
		}
	}
	xmlhttp2.send();
	showList();
};

function showList(){
    console.log("Entro");
	taskBody = document.getElementById("usuarios");

    xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			console.log("Entro");
			var myArr = JSON.parse(this.responseText);
            var out = "";
            var i = 0;
			if(myArr.length!=0){
				
				out += '<table class="table" id="tabla">'
				     + '<thead style="border-top:10px; background:black; color:white;">'
						 + '<tr  class="thead-inverse">'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-2">User</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-3" colspan="2">Name</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-3">Job Post</th>'
							 + '<th style="text-align:center; border:solid; border-color:#fff;" class="col-md-3">Options</th>'
						 + '</tr>'
					 + '</thead>'
				 + '<tbody>'; 
						
				for(i = 0; i < myArr.length; i++) { //Ciclo para mostrar todos los datos de la base de datos de Herramientas
					if(myArr[i].idUsuario != "100001"){
						out += '<td id="user'+myArr[i].idUsuario+'" style="text-align:center;">'+myArr[i].idUsuario+'</td>'
						+ '<td id="name'+myArr[i].idUsuario+'">'+myArr[i].nombre+'</td>'
						+ '<td id="lname'+myArr[i].idUsuario+'">'+myArr[i].apellido+'</td>'
						+ '<td id="post'+myArr[i].idUsuario+'">'+myArr[i].puesto+'</td>'
						
						+ '<td style="text-align:center;">'
						  + '<button type="button" id="edit_button'+myArr[i].idUsuario+'" class="btn btn-default" onclick="edit(\''+myArr[i].idUsuario+'\')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit</button>'
						  + '<button type="button" id="done_button'+myArr[i].idUsuario+'" class="btn btn-success" onclick="save(\''+myArr[i].idUsuario+'\')" style="display:none;"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Done</button>'
						  + '<button type="button" id="delete_button'+myArr[i].idUsuario+'" class="btn btn-danger" onclick="del(\''+myArr[i].idUsuario+'\')"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button>'
						+ '</td></tr>';
					}
				}
				out +=  '</tbody>'
				+ '</table>';
            }
			else{
                out = '<div class="container textContainer">'
                +       '<h1 class="text-center">No Users found.</h1>'
                +     '</div>';   
            }

            taskBody.innerHTML = out;
        }
    };
	urlPath = "API/user/1";
    xmlhttp.open("get", urlPath, true);
    xmlhttp.send();
};

function add(){ //Validar una alta a herramienta
	var idu=document.getElementById("idu").value;
	var name=document.getElementById("nameu").value;
	var lastn=document.getElementById("lastu").value;
	var pass=document.getElementById("passu").value;
	var rpass=document.getElementById("rpassu").value;
	var job=document.getElementById("jobu").value;
	
	if(idu!="" && name!="" && lastn!="" && job!=""  && pass!="" && rpass!=""){
		if(isNumber(idu)){
			if(pass == rpass){
				params = "idu="+idu+"&nameu="+name+"&lastu="+lastn+"&passu="+pass+"&jobu="+job;
						
				var xmlhttp;
				xmlhttp = new XMLHttpRequest();
				xmlhttp.open("POST", "API/user/user.php", true);
				xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
					if(xmlhttp.readyState == 4 && http.status == 200) {
						sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
					}
				}
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == XMLHttpRequest.DONE) {
						console.log(xmlhttp.responseText);
						
						myArr = JSON.parse(xmlhttp.responseText);
						if(myArr.result == "success"){
							swal("Successfully added", "User registered successfully.", "success"); 
							document.getElementById("idu").value="";
							document.getElementById("nameu").value="";
							document.getElementById("lastu").value="";
							document.getElementById("passu").value="";
							document.getElementById("rpassu").value="";
							document.getElementById("jobu").value="";
							ocultar();
						}
						if(myArr.result == "error"){
							swal("Error", "This User has already been registered", "error"); 
						}
						
						
						
						showList();  
					}
				}
				xmlhttp.send(params); 
				showList();  
				showList();
			}
			else{
				sweetAlert("Error", "Passwords does not match.", "error");
			}
		}
		else{
			sweetAlert("Error", "ID user must be number.", "error");
		}
	}
	else {
		sweetAlert("Error", "There are blank fields.", "error");
	}
};

function edit(no){ //Editar los campos de la tabla
	document.getElementById("edit_button"+no).style.display='none';
	document.getElementById("delete_button"+no).style.display='none';
	//document.getElementById("perm_button"+no).style.display='none';
	document.getElementById("done_button"+no).style.display = 'block';
	
	var user=document.getElementById("user"+no);
	var name=document.getElementById("name"+no);
	var lname=document.getElementById("lname"+no);
	var post=document.getElementById("post"+no);
		
	var user_data=user.innerHTML;
	var name_data=name.innerHTML;
	var lname_data=lname.innerHTML;
	var post_data=post.innerHTML;
		
	user.innerHTML="<input type='text' disabled='disabled' id='user_text"+no+"' class='form-control' value='"+user_data+"'>";
	name.innerHTML="<input type='text' id='name_text"+no+"' class='form-control' value='"+name_data+"'>";
	lname.innerHTML="<input type='text' id='lname_text"+no+"' class='form-control' value='"+lname_data+"'>";
	post.innerHTML="<input type='text' id='post_text"+no+"' class='form-control' value='"+post_data+"'>";
};

function save(no){  //Guardar los datos de la tabla que fueron modificados con el método edit() y validar para realizar un cambio
	var user=document.getElementById("user_text"+no).value;
	var name=document.getElementById("name_text"+no).value;
	var lname=document.getElementById("lname_text"+no).value;
	var post=document.getElementById("post_text"+no).value;
	
	if(user!="" && name!="" && lname!="" && post!=""){ //Verficar que los campos NO estén vacíos
		if(isNumber(user) == true){ //verificar que el tiempo de vida sea un número
			document.getElementById("user"+no).innerHTML=user;
			document.getElementById("name"+no).innerHTML=name;
			document.getElementById("lname"+no).innerHTML=lname;
			document.getElementById("post"+no).innerHTML=post;
			
			document.getElementById("edit_button"+no).disabled = true; //habilitar el boton
			
			params = "/"+user+"/"+name+"/"+lname+"/"+post;
		
			var xmlhttp;
			xmlhttp = new XMLHttpRequest();
			xmlhttp.open("PUT", "API/user"+params, true);
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlhttp.readyState == 4 && http.status == 200) {
					sweetAlert("Error", "Error in web service, please contact IT Staff.", "error");
				}
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == XMLHttpRequest.DONE) {
					console.log(xmlhttp.responseText);
					swal("Changes Saved", "User saved successfully.", "success"); 
					showList(); 
				}
			}
			xmlhttp.send(params); 
			showList();  
			showList(); 
		}
		else{
			sweetAlert("Error", "ID user must be number.", "error");
		}
	}
	else{
		sweetAlert("Error", "There are blank fields.", "error");
	}
};

function del(no){ //Función para realizar una baja
	swal({   
        title: "Are you sure?",   
        text: "User data will be deleted.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Delete",  
        closeOnConfirm: false 
    }, function(){  
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("DELETE", "API/user/"+no, true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                console.log(xmlhttp.responseText);
                if(JSON.parse(xmlhttp.responseText).affected_rows != 1){
                    //swal("Error", "Employee "+id+" could not be deleted. \n Error 0x0001: Existent consumption registries.", "error"); 
                }
                else{
                    swal("Deleted!", "User has been deleted.", "success"); 
					showList();
                }
            }
        }
        xmlhttp.send();        
        showList();
    });
    showList();	
};

function isNumber(n){ //Función para validar que una cadena sea un número
  return !isNaN(parseFloat(n)) && isFinite(n);
};

getUsers();