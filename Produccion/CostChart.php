<?php
    require 'template.php';
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	if($_SESSION['chart'] == 0){ //Verificar que otros usuarios no accedan a esta página
		print '<script language="JavaScript">'; 
		//print "alert('This page is only for Engineers.');"; 
		print "window.location='Menu.php';";
		print '</script>'; 
		exit;
	}
	else if (!isset($_POST['selPart']) || !isset($_POST['startd']) || !isset($_POST['finishd'])) { //validar que exista alg佖 valor en 'part'
		print '<script language="JavaScript">'; 
		//print "alert('Select a Part to show.');"; 
		print "window.location='MenuChart.php';";
		print '</script>'; 
		exit;
	}
} else {
	print '<script language="JavaScript">'; 
	print "window.location='login.php';";
	print '</script>'; 
	exit;
}
/*$now = time();
if($now > $_SESSION['expire']) {
	session_destroy();
	print '<script language="JavaScript">'; 
	print "alert('Session ends. Please log in again.');"; 
	print "window.location='login.php';";
	print '</script>';
	exit;
}*/

require("API/connection.php");
?>

<!DOCTYPE html>
<html>
    <?php 
        head();
    ?>

    <body>
        <?php 
            navbar();
			$conn = connect();
        ?>
        
        <!------------------------------------------------ CONTENIDO ---------------------------------------------------------->
		<?php 
		$parte = $_POST["selPart"]; //Variable que indica qu・se mostrar・
		$startd = $_POST["startd"]; //Fecha de inicio
		$finishd = $_POST["finishd"]; //Fecha de fin
		$nombre = "";
		?>
		
		<div id="principal" class="container main-content">
			<div class="row">
				<h1> TOOL COST CHART </h1>
			</div>
			
			<!-- CHART1 -->
			<div id="datos">	
				<?php 
				$query = "select c.cant, h.precio, pd.cantPiezas, 
				pd.fecha, h.descripcion, pd.idOperacion, 
				p.numParte, o.descripcion, l.nombre 
				from operacion o, herramienta h, parte p, operacion_has_herramienta oh, 
				cambio c, linea l, linea_has_operacion lho, produccion_diaria pd
				where p.numParte = $parte
				and p.numParte = o.Parte_numParte
				and o.numOperacion = c.idOp
				and o.numOperacion = pd.idOperacion
				and o.numOperacion = oh.Operacion_numOperacion
				and h.numHerramienta = oh.Herramienta_numHerramienta
				and h.numHerramienta = c.idHerr
				and l.Parte_numParte = p.numParte
				and l.idLinea = lho.Linea_idLinea
				and lho.Operacion_numOperacion = o.numOperacion
				and l.idLinea = c.idLinea
				and l.idLinea = pd.idLinea
				and pd.fecha = c.fecha
				and pd.fecha>='$startd'
				and pd.fecha<='$finishd'  
				order by pd.fecha, o.descripcion, l.idLinea;";
				
				$resultado = $conn->query($query);
				$results = mysqli_num_rows($resultado);
				
				for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) { //Ciclo para mostrar todos los datos de la base de datos de la consulta
					echo "<input type='hidden' id='cant$i' value='$fila[0]'>";
					echo "<input type='hidden' id='precio$i' value='$fila[1]'>";
					echo "<input type='hidden' id='piezas$i' value='$fila[2]'>";
					//echo "TOTAL: <input type='hidden' value='".$fila[0]*$fila[1]/$fila[2]."'>";
					echo "<input type='hidden' id='fecha$i' value='$fila[3]'>";
					echo "<input type='hidden' id='desc$i' value='$fila[4]'>";
					echo "<input type='hidden' id='idop$i' value='$fila[5]'>";
					echo "<input type='hidden' id='numpart$i' value='$fila[6]'>";
					echo "<input type='hidden' id='descop$i' value='$fila[7]'>";
					echo "<input type='hidden' id='li$i' value='$fila[8]'><br><br><br>";
				}
				echo "<input type='hidden' id='res' value='$results'><br>"; //Cantidad de filas de la consulta anterior
				
				$consulta = "select operacion.descripcion 
				from produccion_diaria, cambio, operacion, parte 
				where produccion_diaria.idOperacion = cambio.idOp 
				and operacion.numOperacion = cambio.idOp 
				and parte.numParte = operacion.Parte_numParte 
				and parte.numParte = $parte 
				group by(produccion_diaria.idOperacion);";
				
				$resultado = $conn->query($consulta);
				$results = mysqli_num_rows($resultado);
				
				for($i=0; $fila = mysqli_fetch_row($resultado); $i++){
					echo "<input type='hidden' id='operaciones$i' value='$fila[0]'>"; //Guardamos cada nombre de cada operacion
				}
				echo "<input type='hidden' id='nops' value='$results'><br>"; //Cantidad de operaciones encontradas
				
				$consulta = "select nombre from parte where numParte=".$parte.";";
				$resultado = $conn->query($consulta);
				$fila = mysqli_fetch_row($resultado);
				?>
			</div>
			<?php $nombre = $fila[0]; //Guardamos el nombre de la parte ?>
			<h2 id="namep1"> <?=$fila[0]." ".$startd." - ".$finishd;?> </h2>
			
			<div id="grafica" class="row"> 
				<canvas id="myChart" style="height:100px; width:100px;"></canvas> <!-- Aquí será mostrada la gráfica -->
			</div>
			<?php disconnect($conn); ?>
			
			
			<!-- CHART2 -->
			<div class="row">
				<table class="table table-borderless">
					<td> <h1> COST PARETO CHART </h1> </td>
				</table>
			</div>
			
			<div id="datos2">
				<?php 
				
				$conn = connect();
				
				$consulta = "select c.cant, h.precio, pd.cantPiezas, 
				pd.fecha, h.descripcion, pd.idOperacion, 
				p.numParte, o.descripcion, l.nombre 
				from operacion o, herramienta h, parte p, operacion_has_herramienta oh, 
				cambio c, linea l, linea_has_operacion lho, produccion_diaria pd
				where p.numParte = $parte
				and p.numParte = o.Parte_numParte
				and o.numOperacion = c.idOp
				and o.numOperacion = pd.idOperacion
				and o.numOperacion = oh.Operacion_numOperacion
				and h.numHerramienta = oh.Herramienta_numHerramienta
				and h.numHerramienta = c.idHerr
				and l.Parte_numParte = p.numParte
				and l.idLinea = lho.Linea_idLinea
				and lho.Operacion_numOperacion = o.numOperacion
				and l.idLinea = c.idLinea
				and l.idLinea = pd.idLinea
				and pd.fecha = c.fecha
				and pd.fecha>='$startd'
				and pd.fecha<='$finishd'  
				order by o.descripcion, h.descripcion;";
								
				$resultado = $conn->query($consulta);
				$results = mysqli_num_rows($resultado);
				
				for ($i=0; $fila = mysqli_fetch_row($resultado); $i++) { //Ciclo para mostrar todos los datos de la base de datos de la consulta
					echo "<input type='hidden' id='cants$i' value='$fila[0]'>";
					echo "<input type='hidden' id='precios$i' value='$fila[1]'>";
					echo "<input type='hidden' id='piezass$i' value='$fila[2]'>";
					//echo "TOTAL: <input type='hidden' value='".$fila[0]*$fila[1]/$fila[2]."'>";
					echo "<input type='hidden' id='fechas$i' value='$fila[3]'>";
					echo "<input type='hidden' id='descs$i' value='$fila[4]'>";
					echo "<input type='hidden' id='idops$i' value='$fila[5]'>";
					echo "<input type='hidden' id='numparts$i' value='$fila[6]'>";
					echo "<input type='hidden' id='descops$i' value='$fila[7]'><br><br>";
				}
				echo "<input type='hidden' id='ress' value='$results'><br>"; //Cantidad de filas de la consulta anterior
				
				$consulta2 = "select distinct h.descripcion, o.descripcion 
				from operacion o, herramienta h, parte p, operacion_has_herramienta oh, 
				cambio c, linea l, linea_has_operacion lho, produccion_diaria pd
				where p.numParte = $parte
				and p.numParte = o.Parte_numParte
				and o.numOperacion = c.idOp
				and o.numOperacion = pd.idOperacion
				and o.numOperacion = oh.Operacion_numOperacion
				and h.numHerramienta = oh.Herramienta_numHerramienta
				and h.numHerramienta = c.idHerr
				and l.Parte_numParte = p.numParte
				and l.idLinea = lho.Linea_idLinea
				and lho.Operacion_numOperacion = o.numOperacion
				and l.idLinea = c.idLinea
				and l.idLinea = pd.idLinea
				and pd.fecha = c.fecha
				and pd.fecha>='$startd'
				and pd.fecha<='$finishd'  
				order by o.descripcion, h.descripcion;";
				
				$resultado2 = $conn->query($consulta2);
				$results2 = mysqli_num_rows($resultado2);
				
				for($i=0; $fila = mysqli_fetch_row($resultado2); $i++){
					echo "<input type='hidden' id='opherrs$i' value='$fila[1] $fila[0]'>"; //Guardamos cada descripcion de cada herramienta y de operacion
				}
				echo "<input type='hidden' id='nopss' value='$results2'><br>"; //Cantidad de herramientas encontradas
				$consulta = "select nombre from parte where numParte=".$parte.";";
				$resultado = $conn->query($consulta);
				$fila = mysqli_fetch_row($resultado);
				?>
			</div>
			<?php $nombre = $fila[0]; //Guardamos el nombre de la parte ?>
			<h2 id="namep1"> <?=$fila[0]." ".$startd." - ".$finishd;?> </h2>
			<h4>Total Cost Pareto: $<input type="text" id="pareto" value="0" style="border:none; border-color: transparent; outline:none;" readonly></h4>
			<div id="grafica2" class="row"> 
				<canvas id="myChart2" style="height:100px; width:100px;"></canvas> <!-- Aquí será mostrada la segunda gráfica -->
			</div>
		</div>
		
		
		<div class="col-md-2">
			<button class="btn btn-info btn-block" id="create" onclick="capture()">
				<i class="fa fa-arrow-down" aria-hidden="true"></i>
				&nbsp;Download Report
			</button>
		</div>
		<form method="POST" enctype="multipart/form-data" action="save.php" id="myForm">
			<input type="hidden" name="img_val" id="img_val" value="" />
			<input type="hidden" name="chartno" id="chartno" value="" />
			<?php echo "<input type='hidden' name='pagina' id='pagina' value='CostChart.php?part=$parte&startd=$startd&finishd=$finishd' />"; ?>
			<?php echo "<input type='hidden' name='nombre' id='nombre' value='Tool_Cost_Pareto_$nombre"."_$startd"."_$finishd' />"; ?>
		</form>
		
		<?php disconnect($conn); ?>
        
        <?php
            stickyFooter();
        ?>
    </body> 

<?php
    scripts();
?>
	<script src="Chart.bundle.min.js"></script> <!-- Script que permitirá generar las gráficas -->
	<script src="chart2.js"></script> <!-- Script para realizar los cálculos y mostrarlos en la gráfica 1 -->
	<script src="chart3.js"></script> <!-- Script para realizar los cálculos y mostrarlos en la gráfica 2 -->
	
	<script type="text/javascript" src="jquery.min.17.js"></script>
	<script type="text/javascript" src="html2canvas.js"></script>
	<script type="text/javascript" src="jquery.plugin.html2canvas.js"></script>
	
	<script type="text/javascript">
		function capture() {
			document.getElementById("chartno").value= 'c';
			$('#principal').html2canvas({
				onrendered: function (canvas) {
					//Set hidden field's value to image data (base-64 string)
					$('#img_val').val(canvas.toDataURL("image/png"));
					//Submit the form manually
					document.getElementById("myForm").submit();
				}
			});
		}
	</script>

</html>