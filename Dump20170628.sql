-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tools
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cambio`
--

DROP TABLE IF EXISTS `cambio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cambio` (
  `idCambio` int(11) NOT NULL AUTO_INCREMENT,
  `idOp` int(11) DEFAULT NULL,
  `idHerr` int(11) NOT NULL,
  `cant` int(11) NOT NULL,
  `razon` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idCambio`),
  UNIQUE KEY `idCambio_UNIQUE` (`idCambio`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cambio`
--

LOCK TABLES `cambio` WRITE;
/*!40000 ALTER TABLE `cambio` DISABLE KEYS */;
INSERT INTO `cambio` VALUES (1,1,1,10,1,'2017-06-28'),(2,1,2,14,2,'2017-06-28');
/*!40000 ALTER TABLE `cambio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herramienta`
--

DROP TABLE IF EXISTS `herramienta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herramienta` (
  `numHerramienta` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(80) NOT NULL,
  `tiempoVida` int(11) DEFAULT NULL,
  `precio` float NOT NULL,
  `binloc` varchar(15) DEFAULT NULL,
  `semxcode` varchar(20) DEFAULT NULL,
  `shmmcode` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`numHerramienta`),
  UNIQUE KEY `numHerramienta_UNIQUE` (`numHerramienta`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herramienta`
--

LOCK TABLES `herramienta` WRITE;
/*!40000 ALTER TABLE `herramienta` DISABLE KEYS */;
INSERT INTO `herramienta` VALUES (1,'CBN INSERT',400,31.47,'A-3-1','AV-CA-001','2NUDCGW070204-BN7500'),(2,'INSERT',200,4.43,'A-3-2','AV-CA-002','TPGT110304LFY-T2000Z'),(3,'INSERT',300,4.24,'A-3-3','AV-CA-003','TNPR331M-T1000A'),(4,'CBN INSERT',600,36.59,'A-3-4','AV-CA-004','3NUTNGA331-BN7500'),(5,'DRILL',4000,90.72,'A-3-5','AV-CA-005','G18-213 D12X60DEGXL100'),(6,'CHAMFERING TOOL',1500,110.82,'A-3-6','AV-CA-006','D8X45DEGX64LXD8'),(7,'ENDMILL',220,72.39,'A-3-7','AV-CA-007','G18-251 D9.9X90LXD10-8N'),(8,'DEBURRING TOOL',24000,237.76,'A-3-8','AV-CA-008','PAL12000'),(9,'REAMER',55,119.09,'A-3-9','AV-CA-009','ZO-S512139C'),(10,'DRILL',700,30.15,'A-3-10','AV-CA-010','MDW0300GS2'),(11,'DRILL',700,17.04,'A-3-11','AV-CA-011','G18-095 D3X90DEGX55');
/*!40000 ALTER TABLE `herramienta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linea`
--

DROP TABLE IF EXISTS `linea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linea` (
  `idLinea` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(10) NOT NULL,
  `Parte_numParte` int(11) NOT NULL,
  PRIMARY KEY (`idLinea`,`Parte_numParte`),
  UNIQUE KEY `idLinea_UNIQUE` (`idLinea`),
  KEY `fk_Linea_Parte1_idx` (`Parte_numParte`),
  CONSTRAINT `fk_Linea_Parte1` FOREIGN KEY (`Parte_numParte`) REFERENCES `parte` (`numParte`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linea`
--

LOCK TABLES `linea` WRITE;
/*!40000 ALTER TABLE `linea` DISABLE KEYS */;
INSERT INTO `linea` VALUES (1,'L#1',1),(2,'L#2',1),(3,'L#4',1),(4,'L#5',1);
/*!40000 ALTER TABLE `linea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linea_has_operacion`
--

DROP TABLE IF EXISTS `linea_has_operacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linea_has_operacion` (
  `Linea_idLinea` int(11) NOT NULL,
  `Operacion_numOperacion` int(11) NOT NULL,
  `seccion` int(11) NOT NULL,
  PRIMARY KEY (`Linea_idLinea`,`Operacion_numOperacion`),
  KEY `fk_Linea_has_Operacion_Operacion1_idx` (`Operacion_numOperacion`),
  KEY `fk_Linea_has_Operacion_Linea1_idx` (`Linea_idLinea`),
  CONSTRAINT `fk_Linea_has_Operacion_Linea1` FOREIGN KEY (`Linea_idLinea`) REFERENCES `linea` (`idLinea`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Linea_has_Operacion_Operacion1` FOREIGN KEY (`Operacion_numOperacion`) REFERENCES `operacion` (`numOperacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linea_has_operacion`
--

LOCK TABLES `linea_has_operacion` WRITE;
/*!40000 ALTER TABLE `linea_has_operacion` DISABLE KEYS */;
INSERT INTO `linea_has_operacion` VALUES (1,1,1),(1,2,1),(1,3,2),(1,4,2),(1,5,2),(1,6,2),(1,7,2),(1,8,2),(1,9,3),(1,10,3),(2,3,2),(2,4,2),(2,5,2),(2,6,2),(2,7,2),(2,8,2),(2,9,3),(2,10,3),(3,3,2),(3,4,2),(3,5,2),(3,6,2),(3,7,2),(3,8,2),(4,3,2),(4,4,2),(4,5,2),(4,6,2),(4,7,2),(4,8,2);
/*!40000 ALTER TABLE `linea_has_operacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operacion`
--

DROP TABLE IF EXISTS `operacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operacion` (
  `numOperacion` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `Parte_numParte` int(11) NOT NULL,
  PRIMARY KEY (`numOperacion`,`Parte_numParte`),
  UNIQUE KEY `numOperacion_UNIQUE` (`numOperacion`),
  KEY `fk_Operacion_Parte1_idx` (`Parte_numParte`),
  CONSTRAINT `fk_Operacion_Parte1` FOREIGN KEY (`Parte_numParte`) REFERENCES `parte` (`numParte`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operacion`
--

LOCK TABLES `operacion` WRITE;
/*!40000 ALTER TABLE `operacion` DISABLE KEYS */;
INSERT INTO `operacion` VALUES (1,'OP10',1),(2,'OP20',1),(3,'OP40',1),(4,'OP50',1),(5,'OP55',1),(6,'OP60A',1),(7,'OP60B',1),(8,'OP70',1),(9,'OP80',1),(10,'OP90',1);
/*!40000 ALTER TABLE `operacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operacion_has_herramienta`
--

DROP TABLE IF EXISTS `operacion_has_herramienta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operacion_has_herramienta` (
  `Operacion_numOperacion` int(11) NOT NULL,
  `Herramienta_numHerramienta` int(11) NOT NULL,
  PRIMARY KEY (`Operacion_numOperacion`,`Herramienta_numHerramienta`),
  KEY `fk_Operacion_has_Herramienta_Herramienta1_idx` (`Herramienta_numHerramienta`),
  KEY `fk_Operacion_has_Herramienta_Operacion1_idx` (`Operacion_numOperacion`),
  CONSTRAINT `fk_Operacion_has_Herramienta_Herramienta1` FOREIGN KEY (`Herramienta_numHerramienta`) REFERENCES `herramienta` (`numHerramienta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Operacion_has_Herramienta_Operacion1` FOREIGN KEY (`Operacion_numOperacion`) REFERENCES `operacion` (`numOperacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operacion_has_herramienta`
--

LOCK TABLES `operacion_has_herramienta` WRITE;
/*!40000 ALTER TABLE `operacion_has_herramienta` DISABLE KEYS */;
INSERT INTO `operacion_has_herramienta` VALUES (1,1),(1,2),(2,3),(3,4),(4,5),(5,6),(6,7),(7,8),(8,9),(9,10),(10,11);
/*!40000 ALTER TABLE `operacion_has_herramienta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parte`
--

DROP TABLE IF EXISTS `parte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parte` (
  `numParte` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`numParte`),
  UNIQUE KEY `numParte_UNIQUE` (`numParte`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parte`
--

LOCK TABLES `parte` WRITE;
/*!40000 ALTER TABLE `parte` DISABLE KEYS */;
INSERT INTO `parte` VALUES (1,'Carrier');
/*!40000 ALTER TABLE `parte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produccion_diaria`
--

DROP TABLE IF EXISTS `produccion_diaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produccion_diaria` (
  `idProd` int(11) NOT NULL AUTO_INCREMENT,
  `cantPiezas` float NOT NULL,
  `fecha` date NOT NULL,
  `idOperacion` int(11) NOT NULL,
  PRIMARY KEY (`idProd`),
  UNIQUE KEY `idOperacion_UNIQUE` (`idProd`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produccion_diaria`
--

LOCK TABLES `produccion_diaria` WRITE;
/*!40000 ALTER TABLE `produccion_diaria` DISABLE KEYS */;
INSERT INTO `produccion_diaria` VALUES (1,1,'2017-06-28',1),(2,2,'2017-06-28',1),(3,3,'2017-06-28',1),(4,4,'2017-06-28',2),(5,5,'2017-06-28',2),(6,6,'2017-06-28',3),(7,7,'2017-06-28',4),(8,0,'2017-06-28',1),(9,4,'2017-06-28',1),(10,4,'2017-06-28',1),(11,4,'2017-06-28',2),(12,4,'2017-06-28',2),(13,4,'2017-06-28',3),(14,4,'2017-06-28',4),(15,10,'2017-06-28',1),(16,10,'2017-06-28',2),(17,20,'2017-06-28',3),(18,20,'2017-06-28',4),(19,20,'2017-06-28',5),(20,20,'2017-06-28',6),(21,20,'2017-06-28',7),(22,20,'2017-06-28',8),(23,30,'2017-06-28',9),(24,30,'2017-06-28',10),(25,40,'2017-06-28',3),(26,40,'2017-06-28',4),(27,40,'2017-06-28',5),(28,40,'2017-06-28',6),(29,40,'2017-06-28',7),(30,40,'2017-06-28',8),(31,50,'2017-06-28',9),(32,50,'2017-06-28',10),(33,60,'2017-06-28',3),(34,60,'2017-06-28',4),(35,60,'2017-06-28',5),(36,60,'2017-06-28',6),(37,60,'2017-06-28',7),(38,60,'2017-06-28',8),(39,70,'2017-06-28',3),(40,70,'2017-06-28',4),(41,70,'2017-06-28',5),(42,70,'2017-06-28',6),(43,70,'2017-06-28',7),(44,70,'2017-06-28',8),(45,33333,'2017-06-28',1),(46,33333,'2017-06-28',2),(47,0,'2017-06-28',3),(48,0,'2017-06-28',4),(49,0,'2017-06-28',5),(50,0,'2017-06-28',6),(51,0,'2017-06-28',7),(52,0,'2017-06-28',8),(53,0,'2017-06-28',9),(54,0,'2017-06-28',10),(55,0,'2017-06-28',3),(56,0,'2017-06-28',4),(57,0,'2017-06-28',5),(58,0,'2017-06-28',6),(59,0,'2017-06-28',7),(60,0,'2017-06-28',8),(61,0,'2017-06-28',9),(62,0,'2017-06-28',10),(63,0,'2017-06-28',3),(64,0,'2017-06-28',4),(65,0,'2017-06-28',5),(66,0,'2017-06-28',6),(67,0,'2017-06-28',7),(68,0,'2017-06-28',8),(69,0,'2017-06-28',3),(70,0,'2017-06-28',4),(71,0,'2017-06-28',5),(72,0,'2017-06-28',6),(73,0,'2017-06-28',7),(74,0,'2017-06-28',8);
/*!40000 ALTER TABLE `produccion_diaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `contrasena` varchar(150) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `puesto` varchar(20) NOT NULL,
  `ABCHerr` bit(1) DEFAULT NULL,
  `ABCPart` bit(1) DEFAULT NULL,
  `CambioHerr` bit(1) DEFAULT NULL,
  `PrdoDiaria` bit(1) DEFAULT NULL,
  `charts` bit(1) DEFAULT NULL,
  `users` bit(1) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `idUsuario_UNIQUE` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (100001,'202cb962ac59075b964b07152d234b70','Martin','Fuantos','E','','','','','','');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-28 14:22:52
